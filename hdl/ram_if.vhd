-------------------------------------------------------------------------------
--
-- Design unit:   Control block
--                 
--
-- File name:     ctrl.vhd
--
-- Description:   This block allows to write data to external RAM memory 
--                by simple DMA interface. It uses AHB bus to interface 
--                the RAM memory. AHB bus is implemented as a master device.
--
--                It works in write mode and read mode (depends on data_wr 
--                signal). Block is enabled by asserting of ram_strt signal 
--                during one clock period (it can not be busy before). When 
--                ram_strt is asserted, address, and bytes count are loaded.
--
--                During read mode, data are immediately shifted after 
--                receiving. When data are valid on data output, data_out_vld 
--                is asserted.
--
--                In write mode, data_in are inserted to block by asserting 
--                data_in_vld signal during one clock period. Inserted data are 
--                inserted to fifo to adjust speed of writting. It is necessary 
--                to control write process by monitoring wr_buf_fl signal.
--
-- System:        VHDL'93
--
-- Author:        Marek Laban, MICRONIC a.s., LabHC
--
-- Copyright:     MICRONIC a.s., LabHC
--
-- Created in frame of HECTOR project (No. 644052).
--
-- Revision:      Version 1.00, May 2016
-- History:       16/05/2016 - v1.00 Created (ML, MIC)
--
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

LIBRARY smartfusion2;
USE smartfusion2.ALL;

ENTITY ram_if IS
PORT (
  -- global reset and clock signals
    n_rst            : IN    STD_LOGIC;                  
    clk              : IN    STD_LOGIC;                  
  ---------------------------------------------------------------------------- 
  -- AHB bus signals ---------------------------------------------------------
  ----------------------------------------------------------------------------
    -- AHB-Lite transfer type for master. 
    -- Indicates the type of the current transfer
    htrans            : OUT   STD_LOGIC_VECTOR(1 DOWNTO 0);   
    -- AHB-Lite transfer size for master. Indicates the size of the transfer. 
    -- Only byte, half-word and word transactions are supported
    hsize             : OUT   STD_LOGIC_VECTOR(1 DOWNTO 0);
    -- AHB-Lite write indication for master 0. 
    -- High for a write, Low for a read.           
    hwrite            : OUT   STD_LOGIC;    
    -- AHB-Lite write data for master                   
    hwdata            : OUT   STD_LOGIC_VECTOR(31 DOWNTO 0);   
    -- AHB-Lite address
    haddr             : OUT   STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- AHB-Lite locked sequence indication for master 0      
    hmastlock         : OUT   STD_LOGIC;            
    -- AHB-Lite slave 0 select                     
    hsel              : OUT   STD_LOGIC;  
    -- AHB-Lite ready signal from slave 0                    
    hreadyout         : IN    STD_LOGIC; 
    -- AHB-Lite transfer response for master                     
    hresp             : IN    STD_LOGIC;       
    -- AHB-Lite read data for master              
    hrdata            : IN    STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- AHB-Lite burst register output
    hburst            : OUT   STD_LOGIC_VECTOR(2 DOWNTO 0);
    --
  ----------------------------------------------------------------------------
  -- Interface for R/W data to/from external RAM memmory ---------------------
  ----------------------------------------------------------------------------
    -- data input and valid input data signal
    data_in           : IN    STD_LOGIC_VECTOR(31 DOWNTO 0);
    data_in_vld       : IN    STD_LOGIC;  -- data are valid during asserting 1 
    -- data output and valid output data signal
    data_out          : OUT   STD_LOGIC_VECTOR(31 DOWNTO 0);
    data_out_vld      : OUT   STD_LOGIC;  -- data are valid during asserting 1
    --
    -- start address of DMA 
    addr_in           : IN    STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- bytes count (number of 8-bit words (rounded to multiple of 4))
    byte_cnt          : IN    STD_LOGIC_VECTOR(31 DOWNTO 0);
    --
    -- control signals  
    data_wr           : IN    STD_LOGIC;  -- data read(0) or data write(1) 
    ram_strt          : IN    STD_LOGIC;  -- ram start (strobe during 1 clk period)
    -- info signals  
    wr_buf_fl         : OUT   STD_LOGIC;  -- write buffer full (1 when full)
    buff_ofw          : OUT   STD_LOGIC;  -- buffer overflow (1 when overflow)
    ram_bsy           : OUT   STD_LOGIC;  -- ram busy (1 when busy, 0 when idle) 
    -- actual address of ram opertion
    ram_stg           : OUT   STD_LOGIC_VECTOR(31 DOWNTO 0) 
);
END ram_if;
ARCHITECTURE ram_if OF ram_if IS

  ATTRIBUTE syn_preserve : BOOLEAN;
  ATTRIBUTE syn_preserve OF ram_if : ARCHITECTURE IS true;
  ATTRIBUTE syn_hier     : STRING;
  ATTRIBUTE syn_hier     OF ram_if : ARCHITECTURE IS "hard" ;
 
  -- AHB HTRANS definitions 
  CONSTANT HTRANS_IDLE : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; 
  CONSTANT HTRANS_BUSY : STD_LOGIC_VECTOR(1 DOWNTO 0) := "01"; 
  CONSTANT HTRANS_NSEQ : STD_LOGIC_VECTOR(1 DOWNTO 0) := "10"; 
  CONSTANT HTRANS_SEQ  : STD_LOGIC_VECTOR(1 DOWNTO 0) := "11"; 

  -- ram interface state machine definitions
  TYPE ram_if_states_def IS ( IDLE,
                              ADDRESS_PHASE_WR,
                              DATA_PHASE_WR,
                              WAIT_PHASE_WR,
                              END_PHASE_WR,
                              ADDRESS_PHASE_RD,
                              DATA_PHASE_RD
                            );
  SIGNAL ram_if_state       :ram_if_states_def;

  -- AHB bus auxiliary signals and registers --------
  -- type of transfer
  SIGNAL htrans_t  : STD_LOGIC_VECTOR(1  DOWNTO 0);
  -- actual address of transfer
  SIGNAL haddr_t   : STD_LOGIC_VECTOR(31 DOWNTO 0);
  -- end address of transfer
  SIGNAL haddr_end : STD_LOGIC_VECTOR(31 DOWNTO 0);
  -- sampled hready signal
  SIGNAL hready_int       : STD_LOGIC;
  -- hready falling edge flag
  SIGNAL hready_flngedge  : STD_LOGIC;
  --
  -- FIFO auxiliary signals and registers ------
  -- reset of fifo register routines
  SIGNAL wbuf_rst  : STD_LOGIC;
  -- write buffer not empty signal
  SIGNAL wr_buf_ne : STD_LOGIC;
  -- data written to buffer register
  SIGNAL wr_data   : STD_LOGIC_VECTOR(31 DOWNTO 0);
  -- number of words inserted to fifo
  SIGNAL wr_cnti   : STD_LOGIC_VECTOR(3   DOWNTO 0);
  -- number of words loaded from fifo
  SIGNAL wr_cnto   : STD_LOGIC_VECTOR(3   DOWNTO 0);
  -- fifo register
  SIGNAL wbuf_data : STD_LOGIC_VECTOR(511 DOWNTO 0);
  -- actual input address of buffer
  SIGNAL wr_addr   : STD_LOGIC_VECTOR(3   DOWNTO 0);
  -- difference between inserted and loaded words
  SIGNAL wr_diff   : STD_LOGIC_VECTOR(3   DOWNTO 0);
  -- fifo overflow detected 
  SIGNAL ofw_detect: STD_LOGIC;
 
BEGIN
-- AHB bus protocol ----------------------------------------------------------- 
  PROCESS (n_rst,clk)
  BEGIN

    IF(n_rst = '0')THEN
      haddr_t      <= (OTHERS =>'0');    
      hwdata       <= (OTHERS =>'0');
      data_out     <= (OTHERS =>'0');
      data_out_vld <= '0';
      wr_cnto      <= (OTHERS =>'0');
      htrans_t     <= HTRANS_IDLE;
      hwrite       <= '0';
      wbuf_rst     <= '0';
      hsel         <= '0';
      hready_int   <= '1';
      ram_if_state <= IDLE;
    ELSE 
      IF(rising_edge(clk))THEN
        --
        hready_int  <= hreadyout;  -- sample hready out
        --
        CASE ram_if_state IS
          --
          WHEN IDLE => 
            IF(ram_strt = '1')THEN
              haddr_t    <= addr_in;
              haddr_end  <= addr_in + byte_cnt;
              IF(data_wr = '1')THEN
                wbuf_rst   <= '1';            -- disable buffer and owf reset
                wr_cnto    <= (OTHERS =>'0'); -- reset fifo read words counter
                hwrite     <= '1';            -- set write mode 
                ram_if_state <= ADDRESS_PHASE_WR;
              ELSE
                hwrite       <= '0';          -- set read mode
                hsel 	     <= '1';       
                htrans_t     <= HTRANS_NSEQ;
                ram_if_state <= ADDRESS_PHASE_RD;
              END IF;
            END IF;
            --
          WHEN ADDRESS_PHASE_WR =>
            -- wait while write buffer is not empty and AHB is ready
            IF(wr_buf_ne = '1' AND hreadyout = '1')THEN
              IF(htrans_t = HTRANS_IDLE)THEN
                hsel 	   <= '1';
                htrans_t   <= HTRANS_NSEQ; -- set non sequence transf at first
              ELSE
                htrans_t   <= HTRANS_SEQ;  -- next transfer are sequential
              END IF;
              ram_if_state <= DATA_PHASE_WR;
            END IF;
            --
          WHEN DATA_PHASE_WR =>
            -- when hready falls down, address is loaded
            IF(hready_flngedge = '1')THEN
              haddr_t      <= haddr_t + 4;  -- when address was loaded, set next address
              wr_cnto      <= wr_cnto + 1;  -- new word is read from fifo
              hwdata       <= wr_data;              
              ram_if_state <= WAIT_PHASE_WR;
            END IF;
          WHEN WAIT_PHASE_WR => 
            -- when end address was reacher or fifo overflowed -> end transfer
            IF((haddr_t >= haddr_end) OR (ofw_detect = '1')) THEN  
              ram_if_state <= END_PHASE_WR; 
              htrans_t     <= HTRANS_IDLE;
            -- when buffer is empty, set AHB busy flag           
            ELSIF(wr_buf_ne = '0') THEN  
              ram_if_state <= ADDRESS_PHASE_WR;
              htrans_t     <= HTRANS_BUSY;
            -- when buffer is not empty, continue in write process
            ELSIF(wr_buf_ne = '1' AND hreadyout = '1') THEN
              ram_if_state <= DATA_PHASE_WR;
              htrans_t     <= HTRANS_SEQ;
            END IF;
          WHEN END_PHASE_WR =>
            -- wait while operation is finished
            IF   (hreadyout = '1') THEN  
              wr_cnto      <= (OTHERS =>'0');
              wbuf_rst     <= '0';
              hsel 	       <= '0';
              ram_if_state <= IDLE;
            END IF;
          WHEN ADDRESS_PHASE_RD => 
            data_out_vld     <= '0';
            -- wait while address is read
            IF(hreadyout = '0')THEN
              -- if enough of bytes was read -> end operation
              IF(haddr_t >= haddr_end)THEN
                htrans_t     <= HTRANS_IDLE;
                hsel 	     <= '0';
                ram_if_state <= IDLE;
              -- else set new address and read new data
              ELSE
                haddr_t      <= haddr_t + 4;
                htrans_t     <= HTRANS_SEQ;
                ram_if_state <= DATA_PHASE_RD;          
              END IF;            
            END IF;            
          WHEN DATA_PHASE_RD =>
            -- wait while operation is finished
            IF(hreadyout = '1')THEN
              data_out       <= hrdata;   -- load new data
              data_out_vld   <= '1';      -- set valid data signal
              ram_if_state   <= ADDRESS_PHASE_RD;
            END IF;
          WHEN OTHERS => NULL;
        END CASE;
      END IF;
    END IF;
  END PROCESS;

  -- ram busy signal -> enabled immediately after ram_strt asserting
  ram_bsy    <= '0' WHEN ((ram_if_state = IDLE) AND (ram_strt = '0'))
                    ELSE '1';
  -- ram stage shows actual address
  ram_stg    <= haddr_t;

  -- AHB bus signals
  hburst     <= "001";    -- INCR burst size mode
  hmastlock  <= '0';      -- lock operation disabled
  hsize      <= "10";     -- 32-bit word are loaded
  haddr      <= haddr_t;  -- address of transfer
  htrans     <= htrans_t; -- type of transfer
  -- monitor hready faling edge
  hready_flngedge   <= (not hreadyout) and (hready_int);

-- overflow checker ------------------------------------------------------------
  PROCESS(clk, n_rst)
  BEGIN
    IF (n_rst = '0') THEN
      ofw_detect <= '0';
    ELSIF rising_edge(clk) THEN
      -- during start signal reset overflow flag
      IF(ram_strt = '1' AND ram_if_state = IDLE)THEN
        ofw_detect <= '0';
      -- if overflow occurs set overflow flag
      ELSIF(wr_diff = "1111" AND ram_if_state /= IDLE) THEN
        ofw_detect <= '1';
      END IF;
    END IF;
  END PROCESS; 

  buff_ofw <= ofw_detect;

-- fifo buffer register ------------------------------------------------------
  PROCESS(clk, wbuf_rst )
  BEGIN
    IF (wbuf_rst = '0') THEN                
      wbuf_data <= (OTHERS =>'0');
      wr_cnti   <= (OTHERS =>'0');
    ELSIF rising_edge(clk) THEN             
      -- when data are valid on data_in register
      IF(data_in_vld = '1')THEN
        -- save new word to fifo buffer
        wbuf_data  <= wbuf_data(479 DOWNTO 0) & data_in;
        -- set write pointer 
        wr_cnti    <= wr_cnti + 1;                                       
      END IF;
    END IF;
  END PROCESS;  

  -- difference between write and read pointer
  wr_diff   <= wr_cnti - wr_cnto;  
  -- buffer not empty signal
  wr_buf_ne <= '1' WHEN (wr_diff >= "0001") ELSE '0'; 

  -- set address of actual word (for reading) in buffer
  wr_addr   <= wr_diff - 1;                          
  wr_buf_fl <= '1' WHEN (wr_diff >= "1101") ELSE '0'; 

  -- 512 bit fifo with MUX output
  WITH wr_addr SELECT                             
  wr_data <=  wbuf_data (31  DOWNTO 0  ) WHEN "0000",
              wbuf_data (63  DOWNTO 32 ) WHEN "0001",
              wbuf_data (95  DOWNTO 64 ) WHEN "0010",
              wbuf_data (127 DOWNTO 96 ) WHEN "0011",
              wbuf_data (159 DOWNTO 128) WHEN "0100",
              wbuf_data (191 DOWNTO 160) WHEN "0101",
              wbuf_data (223 DOWNTO 192) WHEN "0110",
              wbuf_data (255 DOWNTO 224) WHEN "0111",
              wbuf_data (287 DOWNTO 256) WHEN "1000",
              wbuf_data (319 DOWNTO 288) WHEN "1001",
              wbuf_data (351 DOWNTO 320) WHEN "1010",
              wbuf_data (383 DOWNTO 352) WHEN "1011",
              wbuf_data (415 DOWNTO 384) WHEN "1100",
              wbuf_data (447 DOWNTO 416) WHEN "1101",
              wbuf_data (479 DOWNTO 448) WHEN "1110",
              wbuf_data (511 DOWNTO 480) WHEN "1111",
              x"00000000"                WHEN OTHERS;
 
END ram_if;
