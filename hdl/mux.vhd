-------------------------------------------------------------------------------
--
-- Design unit:   Multiplexer 
--                 
--
-- File name:     mux.vhd
--
-- Description:   Multiplex input signals for system between 
--                HDMI and SATA connector 
--
-- System:        VHDL'93
--
-- Author:        Marek Laban, MICRONIC a.s., LabHC
--
-- Copyright:     MICRONIC a.s., LabHC
--
-- Created in frame of HECTOR project (No. 644052).
--
-- Revision:      Version 1.00, May 2016
-- History:       16/05/2016 - v1.00 Created (ML, MIC)
--
-------------------------------------------------------------------------------

LIBRARY IEEE;

USE ieee.std_logic_unsigned.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;

ENTITY mux IS
PORT (
  -- TRNG status register
    db_ssi_rx_1  : IN  STD_LOGIC;
    db_ssi_rx_2  : IN  STD_LOGIC;
    db_ssi_rx    : OUT STD_LOGIC;
  -- multiplexor config
    set_mux   : IN  std_logic 

);
END mux;

ARCHITECTURE architecture_mux OF mux IS
BEGIN

  WITH set_mux SELECT                             
      db_ssi_rx <=  db_ssi_rx_1   WHEN '0',
                    db_ssi_rx_2   WHEN '1',
                   '0'  WHEN OTHERS;

END architecture_mux;
