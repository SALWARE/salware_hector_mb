---------------------------------------------------------------------
--
-- Design unit:   Package of constants for vhdl modules
--
-- File name:     lab_hc_mb_pkg.vhd
--
-- Description:   This package defines common types, subtypes and
--                constants required to implement building blocks
--                of Hubert Curien laboratory designs in VHDL.
--                Important constants:
--                   - version number
--
-- System:        VHDL'93
--
-- Autor:         Viktor Fischer, Nathalie Bochard
--                Hubert Curien Laboratory, France
--
-- Copyright:     Hubert Curien Laboratory
--
-- Revision:      Version 1.00, May 2013
--
-- Last changes:  - ...
--
---------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

PACKAGE lab_hc_mb_pkg IS         

  -- BEGIN COMMAND ************************************************************
  -- command definitions for the control block  
    -- Idle mode (nothing to do)
    CONSTANT CMD_OP_IDLE    : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"00000000";
    -- Operation start !! fixed for MSS !!
    CONSTANT CMD_OP_ST      : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"00000001";
    -- Set daughter board 1 active (HDMI) 
    CONSTANT CMD_SETDB1     : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"00000002";
    -- Set daughter board 2 active (SATA) 
    CONSTANT CMD_SETDB2     : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"00000003"; 
    -- Get TRNG status
    CONSTANT CMD_SSI_LOAD_SEND_LOW : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"00000006"; 
    -- User command 1 (not used)
    CONSTANT CMD_GET_TIME      : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"00000007";  
    -- Load high 32 bits of SSI word
    CONSTANT CMD_SSI_LOAD_HIGH : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"00000008";
    -- Read high 32 bits from SSI word
    CONSTANT CMD_READ_SSI_HIGH : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"00000009";
    -- Read low 32 bits from SSI word
    CONSTANT CMD_READ_SSI_LOW  : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"0000000A"; 
    -- start crypto operation
    CONSTANT CMD_CRYPTO_ENA    : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"0000000B"; 
  
    -- Start Address of available RAM memory restricted for FPGA
    CONSTANT RAM_ADR_START     : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"A0000000"; 
    -- End address of restricted memory for FPGA
    CONSTANT RAM_ADR_END       : STD_LOGIC_VECTOR(31 DOWNTO 0) := X"A2000000";
    --
--------------------------------------------------------------------------
-----   Type, sub-type and constant declarations for general use  --------
--------------------------------------------------------------------------
  SUBTYPE SLV_1_0      IS STD_LOGIC_VECTOR(1 DOWNTO 0);
  SUBTYPE SLV_2_0      IS STD_LOGIC_VECTOR(2 DOWNTO 0);
  SUBTYPE SLV_3_0      IS STD_LOGIC_VECTOR(3 DOWNTO 0);
  SUBTYPE SLV_4_0      IS STD_LOGIC_VECTOR(4 DOWNTO 0);
  SUBTYPE SLV_5_0      IS STD_LOGIC_VECTOR(5 DOWNTO 0);
  SUBTYPE SLV_6_0      IS STD_LOGIC_VECTOR(6 DOWNTO 0);
  SUBTYPE SLV_7_0      IS STD_LOGIC_VECTOR(7 DOWNTO 0);
  SUBTYPE SLV_8_0      IS STD_LOGIC_VECTOR(8 DOWNTO 0);
  SUBTYPE SLV_9_0      IS STD_LOGIC_VECTOR(9 DOWNTO 0);
  SUBTYPE SLV_11_0     IS STD_LOGIC_VECTOR(11 DOWNTO 0);
  SUBTYPE SLV_12_0     IS STD_LOGIC_VECTOR(12 DOWNTO 0);
  SUBTYPE SLV_13_0     IS STD_LOGIC_VECTOR(13 DOWNTO 0);
  SUBTYPE SLV_15_0     IS STD_LOGIC_VECTOR(15 DOWNTO 0);
  SUBTYPE SLV_16_0     IS STD_LOGIC_VECTOR(16 DOWNTO 0);
  SUBTYPE SLV_31_0     IS STD_LOGIC_VECTOR(31 DOWNTO 0);
  SUBTYPE SLV_127_0    IS STD_LOGIC_VECTOR(127 DOWNTO 0);



END lab_hc_mb_pkg;