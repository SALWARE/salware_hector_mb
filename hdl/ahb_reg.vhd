-------------------------------------------------------------------------------
--
-- Design unit:   Fabric registers available from MSS by AHB bus
--                 
--
-- File name:     ahb_reg.vhd
--
-- Description:   This block makes access to fabric registers from MSS. It uses
--                AHB slave interface for this purpose. New registers can 
--                be easily added to block, but MSS firmaware must be remade.
--
--                It consists of following registers:
--
--                cmd_reg - command register is directly accessible by Fab 
--                packet (0xFB prefix). If new value is written to cmd_reg, 
--                then new_cmd flag is asserted. Commands allows to create 
--                various operations in FPGA fabric.
--
--                state_reg - state register provides information about 
--                the FPGA for a MSS and subsequently to the host PC. 
--
--                stage_reg - stage register shows actual address of RAM 
--                processing for MSS system. 
--
--                fabin_reg, fabout_reg - both registers are directly available 
--                by Fab packet (0xFB prefix). fabin_reg is fabric input register
--                (from MSS to FPGA), it is 32-bit payload value of Fab packet
--                and fabout_reg or fabric output register(from FPGA to MSS) is 
--                sent as payload of fabric status packet. Fabric status packet 
--                (prefix 0xF5)is always sent as response to Fab packet.
-- 
--                fabst_reg - this register contains 32 status bit word, which 
--                can be used to monitor internal FPGA state. It is inserted
--                in a fabric status packet (prefix 0xF5).
--
--                wstart_reg, wsize_reg are parameters where will be saved data 
--                for user file (data in RAM for MSS file). wstart_reg is start 
--                address of data stream and wsize_reg is size of stream. Both 
--                registers are configurable only by MSS.
--
--                rstart_reg, rsize_reg are parameters where data of user file was 
--                written. Similarly, rstart_reg is start address of stream and 
--                rsize_reg is size of stream. 
--
--                mssio_out - gpio output registers available from MSS
--                mssio_in  - gpio input  registers available for MSS
--
-- System:        VHDL'93
--
-- Author:        Marek Laban, MICRONIC a.s., LabHC
--
-- Copyright:     MICRONIC a.s., LabHC
--
-- Created in frame of HECTOR project (No. 644052).
--
-- Revision:      Version 1.00, May 2016
-- History:       16/05/2016 - v1.00 Created (ML, MIC)
--
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

USE ieee.std_logic_textio.all;
USE std.textio.all;
LIBRARY work;
USE work.lab_hc_mb_pkg.ALL;
ENTITY ahb_reg IS
  PORT (
  -- global signals -----------------------------------------------------------
    n_rst      : IN    STD_LOGIC;                  
    clk        : IN    STD_LOGIC;

  -- AHB SLAVE bus (MSS)-------------------------------------------------------
    haddr_mss  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    hwdata_mss : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    hrdata_mss : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    hsel_mss   : IN  STD_LOGIC;   
    hready_mss : OUT STD_LOGIC;
    hwrite_mss : IN  STD_LOGIC;
    hrdy_mss   : IN  STD_LOGIC;
  -----------------------------------------------------------------------------
    
  -- registers available from MSS
    cmd_reg    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- command register
    state_reg  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);  -- state of operation
    stage_reg  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);  -- actual used address
    --
    fabin_reg  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- fabric input (from MSS)
    fabout_reg : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);  -- fabric output (to MSS)
    fabst_reg  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);  -- fabric status (to MSS)
    --
    -- parameters where will be saved data for user file generation (RAM to MSS
    -- file)
    wstart_reg : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- start address
    wsize_reg  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- size in bytes
    --
    -- parameters where will be loaded user file (MSS file to RAM)
    rstart_reg : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- start address
    rsize_reg  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- size in bytes
    -- registers used for GPIO input/output from MSS
    mssio_out_reg : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);   
    mssio_in_reg  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    --
    -- fifo used for communication using the SSI
    db2mb_in  : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
    db2mb_wr  : IN  STD_LOGIC;
    mb2db_rd  : IN  STD_LOGIC;
    mb2db_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    --
    -- PUF control word
    --  Number of bits  |  8   |  8   |     8     |   8       |
    --  Function:       | RSVD | MODE | mb2db_nmb | db2mb_nmb |
    -- 
    --  RSVD      - reserved space
    --  in_nmb    - mode of the puf function
    --  out_nmb   - number of 
    --  mb2db_nmb - number of words send from PC to motherboard
    --  db2mb_nmb - number of words send from motherboard to PC    
    puf_ctrl_wrd: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- puf status register 
    puf_stat_reg: IN  STD_LOGIC_VECTOR(31 DOWNTO 0);    
    -- new command received signal (assert high)
    new_cmd     : OUT STD_LOGIC
  );
END ahb_reg;

ARCHITECTURE ahb_reg OF ahb_reg IS

  COMPONENT MB2DB_FIFO IS
    -- Port list
    PORT(
      -- Inputs
      CLK   : IN  STD_LOGIC;
      DATA  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
      RE    : IN  STD_LOGIC;
      RESET : IN  STD_LOGIC;
      WE    : IN  STD_LOGIC;
      -- Outputs
      EMPTY : OUT STD_LOGIC;
      FULL  : OUT STD_LOGIC;
      Q     : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
    );
  END COMPONENT MB2DB_FIFO;

  COMPONENT DB2MB_FIFO IS
    -- Port list
    PORT(
      -- Inputs
      CLK   : IN  STD_LOGIC;
      DATA  : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
      RE    : IN  STD_LOGIC;
      RESET : IN  STD_LOGIC;
      WE    : IN  STD_LOGIC;
      -- Outputs
      EMPTY : OUT STD_LOGIC;
      FULL  : OUT STD_LOGIC;
      Q     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
    );
  END COMPONENT DB2MB_FIFO;


-- MSS address of fabric registers -------------------------------------------   
  -- command register address
  CONSTANT CMD_ADDR      : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000000"; 
  -- write to external RAM - first address
  CONSTANT STATE_ADDR    : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000004"; 
  -- stage of acquistion (ext. RAM addr.) register address
  CONSTANT STAGE_ADDR    : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000008";
  -- fabric input register address 
  CONSTANT FABIN_ADDR    : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"7000000C";
  -- fabric output regiter address
  CONSTANT FABOUT_ADDR   : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000010";
  -- fabric output status 
  CONSTANT FABST_ADDR    : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000014";
  --
  -- read from external RAM - start address
  CONSTANT WSTART_ADDR   : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000018"; 
  -- write to external RAM - size of stream
  CONSTANT WSIZE_ADDR    : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"7000001C"; 
  -- read from external RAM - start address
  CONSTANT RSTART_ADDR   : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000020"; 
  -- read from external RAM - size of stream
  CONSTANT RSIZE_ADDR    : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000024"; 
  --
  -- GPIO output registers from MSS
  CONSTANT MSSIO_OUT_ADDR: STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000028";   
  -- GPIO input registers to MSS
  CONSTANT MSSIO_IN_ADDR : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"7000002C";  
  --
  -- Crypto data input registers (128bit splitted to 4 words)
  CONSTANT CRYP_IN_ADDR  : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000030";
  CONSTANT CRYP_OUT_ADDR : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000034";
  -- Crypto control register
  CONSTANT CRYP_CTRL_ADDR: STD_LOGIC_VECTOR(31 DOWNTO 0) := x"70000038";
  -- PUF status register address
  CONSTANT PUF_STAT_ADDR : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"7000003C";  


-- states and registers of MSS AHB bus communication -------------------------
  -- ahb state type definition
  TYPE ahb_state_states IS (  AHB_ADDR,
                              AHB_DATA_WR,
                              AHB_DATA_RD,
                              AHB_WAIT
                           ); 
  -- ahb state definition
  SIGNAL ahb_state        : ahb_state_states;
  -- internal address of AHB implementation
  SIGNAL int_addr         : STD_LOGIC_VECTOR(31 DOWNTO 0);
  --    
-- internal registers of fabric ----------------------------------------------
  -- command register
  SIGNAL cmd_reg_i    : STD_LOGIC_VECTOR(31 DOWNTO 0);  -- command register
  --
  SIGNAL fabin_reg_i  : STD_LOGIC_VECTOR(31 DOWNTO 0);  -- fabric input
  -- registers of external RAM operations
  SIGNAL wstart_reg_i : STD_LOGIC_VECTOR(31 DOWNTO 0);  -- write start address
  SIGNAL wsize_reg_i  : STD_LOGIC_VECTOR(31 DOWNTO 0);  -- write size in bytes
  --
  SIGNAL rstart_reg_i : STD_LOGIC_VECTOR(31 DOWNTO 0);  -- read start address
  SIGNAL rsize_reg_i  : STD_LOGIC_VECTOR(31 DOWNTO 0);  -- read size in bytes
  --
  SIGNAL mssio_out_i  : STD_LOGIC_VECTOR(31 DOWNTO 0);  -- read size in bytes
  -- PUF control word
  --  Number of bits  |  8   |  8   |     8     |   8       |
  --  Function:       | RSVD | MODE | mb2db_nmb | db2mb_nmb |
  -- 
  --  RSVD      - reserved space
  --  in_nmb    - mode of the puf function
  --  out_nmb   - number of 
  --  mb2db_nmb - number of words send from PC to motherboard
  --  db2mb_nmb - number of words send from motherboard to PC
  SIGNAL puf_ctrl_wrd_i : STD_LOGIC_VECTOR(31 DOWNTO 0);
  
  -- fifo signals and registers for loading data from AHB register and sending
  -- it to the daughter board
  SIGNAL db2mb_rd  : STD_LOGIC;
  SIGNAL db2mb_out : STD_LOGIC_VECTOR(31 DOWNTO 0);

  -- fifo signals and registers for loading data from loading data
  -- from daughter board and sending them to the AHB register
  SIGNAL mb2db_in  : STD_LOGIC_VECTOR(31 DOWNTO 0);
  SIGNAL mb2db_wr  : STD_LOGIC;
  SIGNAL db_nrst   : STD_LOGIC;  
BEGIN

  db2mb_fifo_inst: DB2MB_FIFO 
  PORT MAP (
    -- Inputs
    CLK    => clk,
    DATA   => db2mb_in,
    RE     => db2mb_rd,
    RESET  => db_nrst,
    WE     => db2mb_wr, 
    -- Outputs
    EMPTY  => open,
    FULL   => open,
    Q      => db2mb_out
  );
 
  mb2db_fifo_inst: MB2DB_FIFO 
  PORT MAP (
    -- Inputs
    CLK    => clk,
    DATA   => mb2db_in,
    RE     => mb2db_rd,
    RESET  => db_nrst,
    WE     => mb2db_wr, 
    -- Outputs
    EMPTY  => open,
    FULL   => open,
    Q      => mb2db_out
  ); 
 
-- AHB bus communication -------------------------------------------------------
  PROCESS(clk, n_rst)
  BEGIN
    IF (n_rst = '0') THEN
      ahb_state    <= AHB_ADDR;         -- set default state of AHB bus
      cmd_reg_i    <= (OTHERS =>'0');
      fabin_reg_i  <= (OTHERS =>'0');
      wstart_reg_i <= x"A0000000";      -- set default DMA addresses
      wsize_reg_i  <= x"00000200";
      rstart_reg_i <= x"A0000000";    
      rsize_reg_i  <= x"00000200";    
      mssio_out_i  <= (OTHERS =>'0');
      hrdata_mss     <= (OTHERS =>'0');
      puf_ctrl_wrd_i <= (OTHERS =>'0');
      mb2db_wr       <= '0';
      mb2db_in       <= (OTHERS =>'0');
    ELSIF rising_edge(clk) THEN
      CASE ahb_state IS                 
        WHEN AHB_ADDR =>              -- address phase
          -- if hsel_mss active, save address
          IF    (hsel_mss = '1' AND hwrite_mss = '1') THEN  
            ahb_state <= AHB_DATA_WR;
          --
          -- if hsel_mss active, save address
          ELSIF (hsel_mss = '1' AND hwrite_mss = '0') THEN 
            ahb_state <= AHB_DATA_RD;
          END IF;
          --
          int_addr  <= haddr_mss;
        WHEN AHB_DATA_WR =>           -- data phase - write data from MSS
          CASE int_addr IS            -- choose register by address
            WHEN CMD_ADDR   =>             
              -- reset counter of read words  during every command execution
              cmd_reg_i      <= hwdata_mss;
            WHEN FABIN_ADDR =>
              fabin_reg_i    <= hwdata_mss;
            WHEN WSTART_ADDR =>              
              wstart_reg_i   <= hwdata_mss;
            WHEN WSIZE_ADDR  =>              
              wsize_reg_i    <= hwdata_mss;
            WHEN RSTART_ADDR =>              
              rstart_reg_i   <= hwdata_mss;
            WHEN RSIZE_ADDR  =>              
              rsize_reg_i    <= hwdata_mss;
            WHEN MSSIO_OUT_ADDR  =>              
              mssio_out_i    <= hwdata_mss;
            WHEN CRYP_IN_ADDR  =>
              mb2db_in       <= hwdata_mss;
              mb2db_wr       <= '1';
            WHEN CRYP_CTRL_ADDR  => 
              puf_ctrl_wrd_i <= hwdata_mss;
            WHEN OTHERS => NULL;
          END CASE;
          ahb_state <= AHB_WAIT; -- go back to address phase
          --
        WHEN AHB_DATA_RD =>      -- data phase - read data from MSS
          CASE int_addr IS       -- choose register by address
            WHEN CMD_ADDR    =>                 
              hrdata_mss     <= cmd_reg_i;
            WHEN STATE_ADDR  =>              
              hrdata_mss     <= state_reg;
            WHEN STAGE_ADDR  =>              
              hrdata_mss     <= stage_reg;
            WHEN FABIN_ADDR =>
              hrdata_mss     <= fabin_reg_i;
            WHEN FABOUT_ADDR =>
              hrdata_mss     <= fabout_reg;
            WHEN FABST_ADDR  =>
              hrdata_mss     <= fabst_reg;
            WHEN WSTART_ADDR =>              
              hrdata_mss     <= wstart_reg_i;
            WHEN WSIZE_ADDR  =>                   
              hrdata_mss     <= wsize_reg_i;
            WHEN RSTART_ADDR =>              
              hrdata_mss     <= rstart_reg_i;
            WHEN RSIZE_ADDR  =>                   
              hrdata_mss     <= rsize_reg_i;
            WHEN MSSIO_OUT_ADDR  =>                   
              hrdata_mss     <= mssio_out_i;
            WHEN MSSIO_IN_ADDR  =>                   
              hrdata_mss     <= mssio_in_reg;
            WHEN CRYP_OUT_ADDR =>
              -- MSS read this register 2 times - therefore it is incremented by 2
              hrdata_mss     <= db2mb_out;
            WHEN CRYP_CTRL_ADDR  => 
              hrdata_mss <= puf_ctrl_wrd_i;
            WHEN PUF_STAT_ADDR   =>
              hrdata_mss <= puf_stat_reg;
            WHEN OTHERS => 
              hrdata_mss     <= (OTHERS =>'0');
          END CASE;
          ahb_state <= AHB_WAIT;  -- go back to address phase
        WHEN AHB_WAIT =>
          -- if data are ready on fifo output -> save data to ahb bus
          mb2db_wr  <= '0';
          ahb_state <= AHB_ADDR;
        WHEN OTHERS => NULL;
      END CASE;
    END IF;
  END PROCESS;
  hready_mss <= '0' WHEN ((ahb_state  = AHB_DATA_RD) OR 
                          (ahb_state  = AHB_DATA_WR)   ) ELSE '1';

  new_cmd    <= '1' WHEN (ahb_state  = AHB_DATA_WR AND 
                          int_addr   = CMD_ADDR        ) ELSE '0';

  puf_ctrl_wrd <= puf_ctrl_wrd_i;

  -- read data from fifo only in one case 
  db2mb_rd  <= '1' WHEN ((ahb_state  = AHB_ADDR     ) AND 
                         (haddr_mss  = CRYP_OUT_ADDR) AND
                         (hsel_mss   = '1'          ) AND
                         (hwrite_mss = '0'          )     ) ELSE '0';
  

  cmd_reg    <= cmd_reg_i;
  fabin_reg  <= fabin_reg_i;
  wstart_reg <= wstart_reg_i;
  wsize_reg  <= wsize_reg_i;
  rstart_reg <= rstart_reg_i;
  rsize_reg  <= rsize_reg_i;
  mssio_out_reg  <= mssio_out_i;
  db_nrst     <= mssio_out_i(8);     
end ahb_reg;
