-------------------------------------------------------------------------------
--
-- Design unit:   SSI communication module
--                 
--
-- File name:     ssi.vhd
--
-- Description:   Transfer data between motherboard and daughter board using 
--                three wires (clk, tx, rx). The interface consists of the two
--                submodules ssi_rx, ssi_tx. 
-- 
--
-- System:        VHDL'93
--
-- Author:        Oto Petura LabHC
--
-- Copyright:     MICRONIC a.s., LabHC
--  
-- Created in frame of HECTOR project (No. 644052).
--
-- Revision:      Version v5.00, February 2017
-- History:       17/02/2017 - v5.00 Sctructure modified (ML, MIC)
--
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY ssi IS
  GENERIC (
    BSIZE    : INTEGER := 64    -- Data bus BSIZE (MAX=511)
  );
  PORT (
    reset    : IN  STD_LOGIC;   -- Positive reset
    clk      : IN  STD_LOGIC;   -- Internal clocks(3 times faster than ssi_clk)
    ssi_clk  : IN  STD_LOGIC;   -- ssi clocks 
    --
    -- data input/output from device
    rx       : IN  STD_LOGIC;   -- RX serial line
    tx       : OUT STD_LOGIC;   -- TX serial line
    --
    -- tx side ------------------------------------------  
    -- Parallel data input
    data_tx  : IN  STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 ); 
    tx_iprg  : OUT STD_LOGIC;   -- TX in progress
    start    : IN  STD_LOGIC;   -- when asserted, data are transmitted by ssi
    --
    -- rx side ------------------------------------------
    -- Parallel data output
    data_rx  : OUT STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 );
    data_rdy : OUT STD_LOGIC    -- RX data ready
  );  
END ssi;

ARCHITECTURE ssi_arch OF ssi IS
  COMPONENT ssi_tx IS
    GENERIC (
      BSIZE    : INTEGER := 64          -- Data bus width
    );
    PORT (
      reset    : IN  STD_LOGIC;         -- reset
      clk      : IN  STD_LOGIC;         -- clock
      --
      tx       : OUT STD_LOGIC;         -- output TX line
      data_tx  : IN  STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 ); -- TX-ed data
      --
      tx_iprg  : OUT STD_LOGIC;         -- tx in progress flag
      start    : IN  STD_LOGIC          -- transmitting start signal 
    );
  END COMPONENT;

  COMPONENT ssi_rx IS
    GENERIC (
      BSIZE        : INTEGER := 64      -- Data bus width
    );
    PORT (
      reset    : IN  STD_LOGIC;         -- reset
      clk      : IN  STD_LOGIC;         -- clock
      --
      rx       : IN  STD_LOGIC;         -- input RX line
      data_rx  : OUT STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 ); -- RX-ed data
      --
      rx_iprg  : OUT STD_LOGIC;         -- receiving in progress
      ready    : OUT STD_LOGIC          -- data ready on data_rx output
    );
  END COMPONENT;
  
  -- command state machine states
  TYPE tx_state_type IS( IDLE,
                         WAIT_TX_START,
                         WAIT_TX_END
                        ); 
  SIGNAL tx_state        : tx_state_type; 

  SIGNAL data_tx_i: STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 );
  SIGNAL start_i  : STD_LOGIC;
  SIGNAL tx_iprg_i: STD_LOGIC;  
  
  -- command state machine states
  TYPE rx_state_type IS( WAIT_RX_START,
                         WAIT_DATA,
                         SAVE_DATA
                        ); 
  SIGNAL rx_state        : rx_state_type; 

  SIGNAL data_rx_i: STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 );
  SIGNAL rx_iprg_i: STD_LOGIC;  
  SIGNAL ready_i  : STD_LOGIC;
  
BEGIN

ssi_tx_i : ssi_tx
  GENERIC MAP (
    BSIZE    => BSIZE
  )
  PORT MAP (
    reset    => reset,
    clk      => ssi_clk,
    tx       => tx,
    data_tx  => data_tx_i,
    tx_iprg  => tx_iprg_i,
    start    => start_i
  );

ssi_rx_i : ssi_rx
  GENERIC MAP (
    BSIZE    => BSIZE 
  )
  PORT MAP (
    reset    => reset,
    clk      => ssi_clk,
    rx       => rx,
    data_rx  => data_rx_i,
    rx_iprg  => rx_iprg_i,
    ready    => ready_i
  );
    
  -- synchronization of TXing to internal clocks
  PROCESS(clk, reset)
  BEGIN
    IF (reset = '1') THEN
      data_tx_i  <= (OTHERS =>'0');
      tx_state   <= IDLE;
      start_i    <= '0';
    ELSIF rising_edge(clk) THEN
      CASE tx_state IS
        WHEN IDLE =>
          IF ( start = '1' ) THEN
            data_tx_i <= data_tx;
            tx_state  <= WAIT_TX_START;
          ELSE
            tx_state  <= IDLE;
          END IF;
          
        -- send control data
        WHEN WAIT_TX_START =>      
          IF (tx_iprg_i = '1') THEN
            start_i     <= '0';
            tx_state    <= WAIT_TX_END;
          ELSE
            start_i     <= '1';
            tx_state    <= WAIT_TX_START;
          END IF;
          
        -- wait for the end of transfer
        WHEN WAIT_TX_END =>
          IF (tx_iprg_i = '0') THEN
            tx_state <= IDLE;
          ELSE
            tx_state <= WAIT_TX_END;
          END IF;
      END CASE;    
    END IF;
  END PROCESS;  
  
  tx_iprg  <= '1' WHEN ((tx_state  = IDLE AND start = '1') OR
                        (tx_state /= IDLE                )    ) ELSE
              '0';          
    
  -- synchronization of RXing to internal clocks
  PROCESS(clk, reset)
  BEGIN
    IF (reset = '1') THEN
      data_rx    <= (OTHERS =>'0');
      data_rdy   <= '0';
      rx_state   <= WAIT_RX_START;
    ELSIF rising_edge(clk) THEN
      CASE rx_state IS
        -- wait to rx start
        WHEN WAIT_RX_START =>
          data_rdy  <= '0';        
          IF (rx_iprg_i = '1') THEN
            rx_state   <= WAIT_DATA;
          ELSE
            rx_state   <= WAIT_RX_START;
          END IF;
          
        -- wait for data
        WHEN WAIT_DATA =>         
          IF (ready_i = '1') THEN
            rx_state   <= SAVE_DATA;
          ELSE
            rx_state   <= WAIT_DATA;
          END IF;          
          
        -- take data in the next cycle
        WHEN SAVE_DATA =>          
          IF (ready_i = '1') THEN
            data_rx     <= data_rx_i;
            data_rdy    <= '1';
            rx_state    <= WAIT_RX_START;
          ELSE
            rx_state    <= SAVE_DATA;
          END IF;             
      END CASE;    
    END IF;
  END PROCESS;      
    
    
END ssi_arch;

