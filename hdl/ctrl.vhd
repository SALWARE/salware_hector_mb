-------------------------------------------------------------------------------
--
-- Design unit:   Control block
--                 
--
-- File name:     ctrl.vhd
--
-- Description:   Control block is fundamental block of HECTOR evaluation 
--                platform, it consists of two components: ahb_reg and ram_if. 
--                Ahb_reg operates registers which are available from MSS. 
--                Ram_if ensure data transfer from/to external RAM. Control 
--                block uses these two components to operate with user entity 
--                and transfer data to PC host. A lot of signals and registers 
--                are not used, they are used only as a template for making 
--                user adjustable system. 
--
--                This user adjustable system consists of two tools, the first 
--                one is a command execution state machine (CMD_EXE) and the 
--                second one is user application logic. 
--
--                CMD_EXE executes command when new data are written to the CMD
--                register. Commands can be triggered by Fab packet (packet 
--                writes data to command and fabin register). New command can be 
--                added by creating of new command definition (constant) and its 
--                inserting to CMD_EXE.
--
--                Example is designed to acquire ouput of TRNG in external 
--                daughter board. It collects 32-bit words from s_p converter and 
--                save it to the external RAM memory.
-- 
--
-- System:        VHDL'93
--
-- Author:        Marek Laban, MICRONIC a.s., LabHC
--                Oto Petura, LabHC
--
-- Copyright:     MICRONIC a.s., LabHC
--
-- Created in frame of HECTOR project (No. 644052).
--
-- Revision:      Version 1.00, May 2016
-- History:       16/05/2016 - v1.00 Created (ML, MIC)
--                18/11/2016 - v4.00 Synchronour serial interface put in place
--
-------------------------------------------------------------------------------



LIBRARY IEEE;

USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
--USE ieee.std_logic_arith.ALL;
--USE ieee.std_logic_unsigned.ALL;

LIBRARY work;
USE work.lab_hc_mb_pkg.ALL;


ENTITY ctrl IS
  PORT (
  -----------------------------------------------------------------------------
  -- AHB MASTER bus (external RAM) --------------------------------------------
  -----------------------------------------------------------------------------
    htrans     : OUT STD_LOGIC_VECTOR(1  DOWNTO 0);   
    hsize      : OUT STD_LOGIC_VECTOR(1  DOWNTO 0);
    hwrite     : OUT STD_LOGIC;    
    hwdata     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);   
    haddr      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    hmastlock  : OUT STD_LOGIC;            
    hsel       : OUT STD_LOGIC;  
    hreadyout  : IN  STD_LOGIC; 
    hresp      : IN  STD_LOGIC;       
    hrdata     : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    hburst     : OUT STD_LOGIC_VECTOR(2  DOWNTO 0);
    --
  -----------------------------------------------------------------------------
  -- AHB SLAVE bus (MSS)-------------------------------------------------------
  -----------------------------------------------------------------------------
    haddr_mss  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    hwdata_mss : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    hrdata_mss : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    hsel_mss   : IN  STD_LOGIC;   
    hready_mss : OUT STD_LOGIC;   
    hwrite_mss : IN  STD_LOGIC;
    hrdy_mss   : IN  STD_LOGIC;
    --
  -----------------------------------------------------------------------------
  -- Control block signals and registers --------------------------------------
  -----------------------------------------------------------------------------
  -- global signals 
    n_rst      : IN  STD_LOGIC;
    clk        : IN  STD_LOGIC;
    ssi_clk    : IN  STD_LOGIC;
  -- TRNG status
    ssi_tx     : OUT STD_LOGIC;
    ssi_rx     : IN  STD_LOGIC;
  -- multiplexor out (0 - HDMI,1 - SATA)
    db_mux_out : OUT STD_LOGIC;
  -- daughter board reset signal  
    db_nrst    : OUT STD_LOGIC;
  -- just the LED signal                 
    led_out     : OUT STD_LOGIC
 
  );
END ctrl;

ARCHITECTURE architecture_ctrl OF ctrl IS

  -- BEGIN COMPONENT DEFINITIONS **********************************************

  COMPONENT ram_if IS
  PORT(
  -- global reset and clock signals
    n_rst            : IN    STD_LOGIC;                  
    clk              : IN    STD_LOGIC;                  
  ---------------------------------------------------------------------------- 
  -- AHB bus signals ---------------------------------------------------------
  ----------------------------------------------------------------------------
    -- AHB-Lite transfer type for master. 
    -- Indicates the type of the current transfer
    htrans            : OUT   STD_LOGIC_VECTOR(1 DOWNTO 0);   
    -- AHB-Lite transfer size for master. Indicates the size of the transfer. 
    -- Only byte, half-word and word transactions are supported
    hsize             : OUT   STD_LOGIC_VECTOR(1 DOWNTO 0);
    -- AHB-Lite write indication for master 0. 
    -- High for a write, Low for a read.           
    hwrite            : OUT   STD_LOGIC;    
    -- AHB-Lite write data for master                   
    hwdata            : OUT   STD_LOGIC_VECTOR(31 DOWNTO 0);   
    -- AHB-Lite address
    haddr             : OUT   STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- AHB-Lite locked sequence indication for master 0      
    hmastlock         : OUT   STD_LOGIC;            
    -- AHB-Lite slave 0 select                     
    hsel              : OUT   STD_LOGIC;  
    -- AHB-Lite ready signal from slave 0                    
    hreadyout         : IN    STD_LOGIC; 
    -- AHB-Lite transfer response for master                     
    hresp             : IN    STD_LOGIC;       
    -- AHB-Lite read data for master              
    hrdata            : IN    STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- AHB-Lite burst register output
    hburst            : OUT   STD_LOGIC_VECTOR(2 DOWNTO 0);
    --
  ----------------------------------------------------------------------------
  -- Interface for R/W data to/from external RAM memmory ---------------------
  ----------------------------------------------------------------------------
    -- data input and valid input data signal
    data_in           : IN    STD_LOGIC_VECTOR(31 DOWNTO 0);
    data_in_vld       : IN    STD_LOGIC;  -- data are valid during asserting 1 
    -- data output and valid output data signal
    data_out          : OUT   STD_LOGIC_VECTOR(31 DOWNTO 0);
    data_out_vld      : OUT   STD_LOGIC;  -- data are valid during asserting 1
    --
    -- start address of DMA 
    addr_in           : IN    STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- bytes count (number of 8-bit words (rounded to multiple of 4))
    byte_cnt          : IN    STD_LOGIC_VECTOR(31 DOWNTO 0);
    --
    -- control signals  
    data_wr           : IN    STD_LOGIC;  -- data read(0) or data write(1) 
    ram_strt          : IN    STD_LOGIC;  -- ram start (1 during one clk period)
    -- info signals  
    wr_buf_fl         : OUT   STD_LOGIC;  -- write buffer full (1 when full)
    buff_ofw          : OUT   STD_LOGIC;  -- buffer overflow (1 when overflow)
    ram_bsy           : OUT   STD_LOGIC;  -- ram busy (1 when busy, 0 when idle) 
    -- actual address of ram opertion
    ram_stg           : OUT   STD_LOGIC_VECTOR(31 DOWNTO 0) 
  );
  END COMPONENT;
  
  COMPONENT ahb_reg IS
  PORT (
  -- global signals 
    n_rst      : IN    STD_LOGIC;                  
    clk        : IN    STD_LOGIC;
    --
  -- AHB SLAVE bus (MSS)
    haddr_mss  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    hwdata_mss : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    hrdata_mss : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    hsel_mss   : IN  STD_LOGIC;   
    hready_mss : OUT STD_LOGIC;   
    hwrite_mss : IN  STD_LOGIC;
    hrdy_mss   : IN  STD_LOGIC;
    -- 
  -- registers available from MSS
    cmd_reg    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- command register
    state_reg  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);  -- state of operation
    stage_reg  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);  -- actual used address
    --
    fabin_reg  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- fabric input (from MSS)
    fabout_reg : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);  -- fabric output (to MSS)
    fabst_reg  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);  -- fabric status (to MSS)
    --
    -- parameters where will be saved data for user file generation 
    -- (RAM to MSS file)
    wstart_reg : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- start address
    wsize_reg  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- size in bytes
    --
    -- parameters where will be loaded user file (MSS file to RAM)
    rstart_reg : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- start address
    rsize_reg  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  -- size in bytes
    -- registers used for GPIO input/output from MSS
    mssio_out_reg  : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);  
    mssio_in_reg   : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    --
    -- fifo used for communication using the SSI
    db2mb_in  : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
    db2mb_wr  : IN  STD_LOGIC;
    mb2db_rd  : IN  STD_LOGIC;
    mb2db_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    --
    -- PUF control word
    --  Number of bits  |  8   |  8   |     8     |   8       |
    --  Function:       | RSVD | MODE | mb2db_nmb | db2mb_nmb |
    -- 
    --  RSVD      - reserved space
    --  in_nmb    - mode of the puf function
    --  out_nmb   - number of 
    --  mb2db_nmb - number of words send from PC to motherboard
    --  db2mb_nmb - number of words send from motherboard to PC    
    puf_ctrl_wrd: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- puf status register 
    puf_stat_reg: IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- new command received signal (assert high)
    new_cmd   : OUT STD_LOGIC
  -----------------------------------------------------------------------------
  );
  END COMPONENT;

-- DB SSI
  COMPONENT ssi IS
    GENERIC (
      BSIZE    : INTEGER := 64    -- Data bus width (MAX=511)
    );
    PORT (
      reset    : IN  STD_LOGIC;   -- Positive reset
      clk      : IN  STD_LOGIC;   -- Internal clocks
      ssi_clk  : IN  STD_LOGIC;   -- ssi clocks 

      -- slave device interface
      rx       : IN  STD_LOGIC;   -- RX serial line
      tx       : OUT STD_LOGIC;   -- TX serial line

      -- tx side of instance    
      start    : IN  STD_LOGIC;   -- Input data load (TX start)
      -- Parallel data input (to be TX-ed)
      data_tx  : IN  STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 ); 
      tx_iprg  : OUT STD_LOGIC;   -- transmission in progress
 
      -- rx side of instance
      -- Parallel data output (RX-ed data)
      data_rx  : OUT STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 ); 
      data_rdy : OUT STD_LOGIC    -- RX data ready

    );
  END COMPONENT ssi;

  -- END COMPONENT DEFINITIONS ************************************************


  -- BEGIN COMMAND ************************************************************
  -- command execution signals and registers 
    -- command state machine states
    TYPE cmd_state_type IS( CMD_IDLE,

                            CMD_CRYPTO_RD, 
                            CMD_EXE
                           ); 
    SIGNAL cmd_state        : cmd_state_type; 
    -- new command was received signal
    SIGNAL new_cmd          : STD_LOGIC;       
    -- command register
    SIGNAL cmd_reg          : STD_LOGIC_VECTOR(31 DOWNTO 0);  
    -- fabric input register (from MSS)
    SIGNAL fabin_reg        : STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- fabric output register (to MSS)
    SIGNAL fabout_reg       : STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- fabric status register (to MSS)
    SIGNAL fabst_reg        : STD_LOGIC_VECTOR(31 DOWNTO 0);
    --
    -- multiplexing between daughter boards (0 - HDMI, 1 - SATA connector)
    SIGNAL mux_out          : STD_LOGIC;      
  -- END COMMAND ***************************************************************
  


  -- BEGIN TEMPLATE DESIGN SIGNALS ********************************************* 
  -- state of acquistion register definitions  
    -- acquisition idle state
    CONSTANT OP_IDLE_STATE  : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"FFFFFFFF"; 
    -- busy state
    CONSTANT OP_BUSY_STATE  : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"00000001"; 
    -- buffer overflow state
    CONSTANT OP_OFLW_STATE  : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"00000002"; 
    -- buffer overflow state
    CONSTANT OP_ERR_STATE   : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"00000003"; 
    --
    --
    -- mss output gpio registers
    SIGNAL mssio_out_reg        : STD_LOGIC_VECTOR(31 DOWNTO 0);  
    -- mss input gpio register
    SIGNAL mssio_in_reg        : STD_LOGIC_VECTOR(31 DOWNTO 0);  
    --    
    -- operation state register
    SIGNAL state_reg        : STD_LOGIC_VECTOR(31 DOWNTO 0);  
    --
    -- DB SSI connection
    SIGNAL ssi_data_out     : STD_LOGIC_VECTOR(63 DOWNTO 0);
    SIGNAL ssi_data_in      : STD_LOGIC_VECTOR(63 DOWNTO 0);
    SIGNAL db2mb_nmb      :           UNSIGNED( 7 DOWNTO 0);
    SIGNAL mb2db_nmb      :           UNSIGNED( 7 DOWNTO 0);
    SIGNAL ssi_data_latch   : STD_LOGIC_VECTOR(63 DOWNTO 0);
    SIGNAL ssi_data_start   : STD_LOGIC;
    SIGNAL ssi_data_start_s : STD_LOGIC;
    SIGNAL ssi_data_rdy     : STD_LOGIC;
    SIGNAL ssi_data_ready_s : STD_LOGIC;
    SIGNAL ssi_tx_iprg      : STD_LOGIC;
    SIGNAL ssi_in_prog_s    : STD_LOGIC;
    TYPE   ssi_states IS (IDLE, WAIT_CTRL, SEND_DATA, 
                          RECEIVE_DATA, RECEIVE_STATUS);
    SIGNAL ssi_state     : ssi_states := IDLE;

    TYPE data_requests IS ( REQ_NONE,
                            REQ_RNG_STATUS,
                            REQ_TIME,
                            REQ_SSI_LOW_32,
                            REQ_SSI_HIGH_32
                          );
    SIGNAL requested_data : data_requests := REQ_NONE;
 
    -- PUF interface to transfer many words
    -- input to fifo (way to motherboard)
    SIGNAL db2mb_in    : STD_LOGIC_VECTOR(63 DOWNTO 0);
    -- fifo write signal
    SIGNAL db2mb_wr     : STD_LOGIC;
    -- output from fifo (way to daughter board)
    SIGNAL mb2db_out    : STD_LOGIC_VECTOR(63 DOWNTO 0);
    -- fifo read signal
    SIGNAL mb2db_rd     : STD_LOGIC;
    -- PUF control word
    --  Number of bits  |  8   |  8   |     8     |   8       |
    --  Function:       | RSVD | MODE | mb2db_nmb | db2mb_nmb |
    -- 
    --  RSVD      - reserved space
    --  in_nmb    - mode of the puf function
    --  out_nmb   - number of 
    --  mb2db_nmb - number of words send from PC to motherboard
    --  db2mb_nmb - number of words send from motherboard to PC  
    SIGNAL puf_ctrl_wrd : STD_LOGIC_VECTOR(31 DOWNTO 0);
    -- puf status register
    SIGNAL puf_stat_reg : STD_LOGIC_VECTOR(31 DOWNTO 0);
    --
    SIGNAL db_rst : STD_LOGIC := '0'; -- Daughter board positive reset
    SIGNAL reset  : STD_LOGIC := '0'; -- Positive reset

BEGIN
  i_ahb_reg: ahb_reg
  PORT MAP (
  -- global signals  
    n_rst      => n_rst,
    clk        => clk,
    --
  -- AHB SLAVE bus (MSS) 
    haddr_mss  => haddr_mss,
    hwdata_mss => hwdata_mss,
    hrdata_mss => hrdata_mss,
    hsel_mss   => hsel_mss,   
    hready_mss => hready_mss,   
    hwrite_mss => hwrite_mss,
    hrdy_mss   => hrdy_mss,
    --
  -- registers available from MSS
    cmd_reg    => cmd_reg,
    state_reg  => state_reg,
    stage_reg  => X"00000000",
    -- registers available from user instructions (UART)
    fabin_reg  => fabin_reg, 
    fabout_reg => fabout_reg,
    fabst_reg  => fabst_reg,
    --
    wstart_reg => open,
    wsize_reg  => open,
    rstart_reg => open,
    rsize_reg  => open,
    -- user optional registers
    mssio_out_reg => mssio_out_reg,
    mssio_in_reg  => mssio_in_reg,
    -- reset of the daughter board
    -- 
    db2mb_in  => db2mb_in,
    db2mb_wr  => db2mb_wr,
    mb2db_rd  => mb2db_rd,
    mb2db_out => mb2db_out,
    --
    -- PUF control word
    --  Number of bits  |  8   |  8   |     8     |   8       |
    --  Function:       | RSVD | MODE | mb2db_nmb | db2mb_nmb |
    -- 
    --  RSVD      - reserved space
    --  in_nmb    - mode of the puf function
    --  out_nmb   - number of 
    --  mb2db_nmb - number of words send from PC to motherboard
    --  db2mb_nmb - number of words send from motherboard to PC    
    puf_ctrl_wrd => puf_ctrl_wrd,
    -- puf status register 
    puf_stat_reg => puf_stat_reg,
  -- new command received signal (assert high)
    new_cmd    => new_cmd
  );

  reset <= NOT n_rst;
 
  ssi_master_inst : ssi
    GENERIC MAP (
      BSIZE   => 64
    )
    PORT MAP (
      reset    => db_rst,
      clk      => clk,
      ssi_clk  => ssi_clk,
      
      -- slave device interface
      rx       => ssi_rx,
      tx       => ssi_tx,
      
      -- tx side of instance    
      start    => ssi_data_start,
      tx_iprg  => ssi_tx_iprg,
      data_tx  => ssi_data_out,

      -- rx side of interface 
      data_rx  => ssi_data_in,
      data_rdy => ssi_data_rdy
    );

-- SSI data output FSM
  PROCESS(clk, db_rst)
  BEGIN
    IF ( db_rst = '1' ) THEN
      ssi_state        <= IDLE;
      ssi_data_latch   <= (OTHERS => '0');
      db2mb_in         <= (OTHERS => '0');
      db2mb_nmb        <= (OTHERS => '0');
      mb2db_nmb        <= (OTHERS => '0');
      mb2db_rd         <= '0';
      db2mb_wr         <= '0';
      ssi_data_start   <= '0';
    ELSIF rising_edge(clk) THEN
      CASE ssi_state IS
      
        WHEN IDLE =>
          IF ( ssi_data_start_s = '1' ) THEN
            ssi_data_start   <= '1';
            ssi_state   <= WAIT_CTRL;
            db2mb_nmb   <= UNSIGNED(puf_ctrl_wrd(7  DOWNTO 0));
            mb2db_nmb   <= UNSIGNED(puf_ctrl_wrd(15 DOWNTO 8));
          ELSE
            ssi_state <= IDLE;
          END IF;
          
        -- send control data
        WHEN WAIT_CTRL =>
          ssi_data_start  <= '0';
          IF (ssi_tx_iprg = '0') THEN
            ssi_state   <= SEND_DATA;
          ELSE
            ssi_state   <= WAIT_CTRL;
          END IF;
          
        -- wait for the confirmation
        WHEN SEND_DATA =>
          IF (mb2db_nmb > X"00") THEN
            IF(ssi_tx_iprg = '0' AND mb2db_rd = '0')THEN
              mb2db_rd       <= '1';
              mb2db_nmb      <= mb2db_nmb - 1;
            ELSE
              mb2db_rd       <= '0';
            END IF;
          ELSE
            mb2db_rd    <= '0';
            ssi_state   <= RECEIVE_STATUS;
          END IF;  

          IF(mb2db_rd = '1')THEN
            ssi_data_start <= '1';
          ELSE
            ssi_data_start <= '0';
          END IF;
        
        -- wait for data and insert the status to the register       
        WHEN RECEIVE_STATUS =>          
          IF(ssi_data_rdy = '1')THEN
            puf_stat_reg <= ssi_data_in(31 DOWNTO 0);
            ssi_state    <= RECEIVE_DATA;
          ELSE
            ssi_state    <= RECEIVE_STATUS;
          END IF;       
          ssi_data_start <= '0';

        -- receive data  
        WHEN RECEIVE_DATA =>
          IF (db2mb_nmb /= X"00") THEN
            IF(ssi_data_rdy = '1')THEN
              db2mb_in    <= ssi_data_in;
              db2mb_wr    <= '1';
              db2mb_nmb   <= db2mb_nmb - 1;
            ELSE
              db2mb_wr    <= '0';
            END IF;
          ELSE
            db2mb_wr  <= '0';
            ssi_state <= IDLE;
          END IF;
          ssi_data_start <= '0';
        WHEN OTHERS =>
          ssi_state <= IDLE;
      END CASE;
    END IF;
  END PROCESS;
  
  
  ssi_data_out   <= X"00000000" & puf_ctrl_wrd  
                      WHEN ((ssi_state = WAIT_CTRL) OR (ssi_state = IDLE)) ELSE
                    mb2db_out;
  
  -- ssi transmission in progress
  ssi_in_prog_s  <= '1' WHEN  ssi_state /= IDLE        ELSE '0';
  
  fabst_reg(31 DOWNTO 24) <= (OTHERS => '0');
  
  fabst_reg(23 DOWNTO 16) <= STD_LOGIC_VECTOR(mb2db_nmb);
  
  fabst_reg(15 DOWNTO 8)  <= STD_LOGIC_VECTOR(db2mb_nmb);
  
  WITH ssi_state SELECT
  fabst_reg(7 DOWNTO 0) <= X"00" WHEN IDLE,
                           X"04" WHEN WAIT_CTRL,
                           X"08" WHEN SEND_DATA,
                           X"0C" WHEN RECEIVE_DATA,
                           X"FF" WHEN OTHERS;
  
    -- Fabric status register
   --fabst_reg(31 DOWNTO 8) <= X"000000";
  --fabst_reg(7 DOWNTO 2)  <=  "000000";
 
  -- ssi_in_prog_s     ->    '0' when device is idle or finished
  --                         '1' when device is currently transmiting data
  --fabst_reg(1 DOWNTO 0)  <= ssi_in_prog_s & "0";
  

--***************************************************************************--
--*************** COMMAND execution state machine template ******************--
--***************************************************************************--
  PROCESS(clk, n_rst)
  BEGIN
    IF (n_rst = '0') THEN
      cmd_state        <= CMD_IDLE;
      mux_out          <= '1';
      requested_data   <= REQ_NONE;
      ssi_data_start_s <= '0';
    ELSIF rising_edge(clk) THEN
      CASE cmd_state IS
        WHEN CMD_IDLE =>              -- command awaiting
          ssi_data_start_s <= '0';
          IF (new_cmd = '1') THEN
            cmd_state <= CMD_EXE;
          END IF;
        WHEN CMD_CRYPTO_RD =>
          cmd_state <= CMD_IDLE;        
        WHEN CMD_EXE  =>              -- command execution
          CASE cmd_reg IS
            WHEN CMD_OP_ST =>         -- operation is started from MSS
              cmd_state <= CMD_IDLE;  -- by inserting of this command
            WHEN CMD_SETDB1 =>        -- set daughter board 1 as active board
              mux_out   <= '0';       -- (HDMI)
              cmd_state <= CMD_IDLE;
            WHEN CMD_SETDB2 =>        -- set daughter board 2 as active board
              mux_out   <= '1';       -- (SATA)
              cmd_state <= CMD_IDLE;
            WHEN CMD_CRYPTO_ENA =>
              ssi_data_start_s  <= '1';
              cmd_state         <= CMD_CRYPTO_RD;
            WHEN OTHERS => NULL;
          END CASE; 
        WHEN OTHERS => NULL;
      END CASE;
    END IF;
  END PROCESS;

  db_mux_out <= mux_out;  -- multiplexor output for connector choose


--***************************************************************************--


--***************************************************************************--
--************************ USER TEMPLATE DESIGN *****************************--
--***************************************************************************--



  led_out <= ssi_rx;
  
  fabout_reg <= (OTHERS => '0');

  mssio_in_reg(31 DOWNTO 0) <= (OTHERS =>'0');
  
      --
  db_nrst   <= mssio_out_reg(8);    -- daughter board negative reset signal  
  db_rst    <= NOT mssio_out_reg(8);-- daughter board positive reset signal

  --  Not used signals for AHB interface
  htrans    <= (OTHERS => '0');
  hsize     <= (OTHERS => '0');
  hwrite    <= '0';
  hwdata    <= (OTHERS => '0');
  haddr     <= (OTHERS => '0');
  hmastlock <= '0';  
  hsel      <= '0';
  hburst    <= (OTHERS => '0');
--***************************************************************=************--

END architecture_ctrl;
