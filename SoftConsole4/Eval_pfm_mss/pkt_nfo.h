/**
  ******************************************************************************
  * @file      pkt_nfo.h
  * @author    Marek Laban, Marcel Kleja, MICRONIC a.s., LabHC
  * @version   v1.0
  * @note      Created in frame of HECTOR project (No. 644052).
  * @copyright MICRONIC a.s., LabHC
  * @date      16-May-2016
  * @revision  v1.0 created (ML)
  * @brief     Contains packet information definitions
  *
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

#ifndef PKT_NFO_H_
#define PKT_NFO_H_


#define PHEAD 0x13 /**< Packet head byte */

#define STAT_PKT_SIZE 12             /**< State packet size */
#define CRYP_PKT_SIZE ((1024/8) + 8) /**< Crypto packet size + prefix + status */
#define CRYP_FIFO_SIZE 32            /**< Size of cryp fifo in FPGA            */
/** @defgroup Packet_prefixes
  * @{
  */
#define PCODE 0xC0   /**< Code or command prefix */
#define PWDAT 0x3D   /**< Write data file prefix */
#define PRDAT 0xFD   /**< Read data file prefix */
#define PSTAT 0x57   /**< State prefix */
#define PFAB  0xFB   /**< Fabric command prefix */
#define PFBST 0xF5   /**< Fabric status */
#define PCRYP 0xC9   /**< Fabric crypt. input packet */
#define PCST  0xC5   /**< Fabric crypt. status */

 /** @defgroup Packet_nulls_number
  * @{
  */
#define PCODE_NULLS 2
#define PRDAT_NULLS 2

/** @defgroup Response_error_states
  * @{
  */
#define MB_SUCCESS       0		/**< Successful or idle state */
#define MB_OP_INPRG      1      /**< Operation in progress */
#define MB_FSYS_ERR      2      /**< File system error */
#define MB_CMD_NFND      3      /**< Command not found */
#define MB_AQ_BIG_STREAM 4      /**< Required stream size is too big */
#define MB_NOTHING_TODO  5      /**< Idle command was received */
#define MB_NOT_MULTIPLE  6      /**< Stream must be multiple of 4 */
#define MB_RD_FILE_ERR   7      /**< File read error */

/** @defgroup acq_state definitions
  * @brief instruction MB_GET_AQ_STATE return one of these words as state word
  * @{
  */
#define OP_IDLE     0			/**< operation idle */
#define OP_BUSY     1			/**< operation busy */
#define OP_OVFLW    2			/**< operation buffer overflow */
#define OP_WR_ERR   3			/**< operation write error */
#define OP_UNKNW    4			/**< operation unknown error */

/** @defgroup Configuration state of GPIO
  * @{
  */
#define GPIO_CFG_OUT 1
#define GPIO_CFG_IN  0


/** @defgroup Reserved_word
  * @{
  */
#define MB_NOTHING 0  			/**< Zero word, only for fata filling */

#endif /* PKT_NFO_H_*/
