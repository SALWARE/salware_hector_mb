/***************************************************************************//**
 * (c) Copyright 2012-2013 Microsemi SoC Products Group.  All rights reserved.
 * (c) Copyright 2015 Micronic, a. s.  All rights reserved.
 *
 * USB MSC Class Storage Device example application to demonstrate the
 * SmartFusion2 MSS USB operations in device mode.
 *
 * Drivers used:
 * Smartfusion2 MSS USB Driver stack (inclusive of USBD-MSC class driver).
 *
 * SVN $Revision: 5468 $
 * SVN $Date: 2013-03-29 15:38:01 +0530 (Fri, 29 Mar 2013) $
 */
#include <stdio.h>
#include <string.h>
#include "ram_drive_app.h"
#include "lpddr/lpddr.h"
#include "CMSIS/mss_assert.h"
#include "hal/hal.h"
#include "FatFs/src/ffconf.h"
#include "drivers/mss_gpio/mss_gpio.h"
#include "drivers/mss_usb/mss_usb_device.h"
#include "drivers/mss_usb/mss_usb_device_msd.h"
#include "drivers/mss_uart/mss_uart.h"

extern int MEM_protected;

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
  Private data structures
*/
/*
 Number of LUNs supported 1. LUN0
 */

#define STORAGE_RAM

#define NUMBER_OF_LUNS_ON_DRIVE                                1u

/*Type to store information of each LUN*/
typedef struct ram_lun_data {
    uint32_t number_of_blocks;
    uint32_t erase_block_size;
    uint32_t lba_block_size;
} ram_lun_data_t;

/******************************************************************************
  Private function declarations
*/
uint8_t* usb_ram_media_inquiry(uint8_t lun, uint32_t *len);
uint8_t  usb_ram_media_init (uint8_t lun);
uint8_t  usb_ram_media_get_capacity(uint8_t lun, uint32_t *no_of_blocks, uint32_t *block_size);
uint8_t  usb_ram_media_is_ready(uint8_t lun);
uint8_t  usb_ram_media_is_write_protected(uint8_t lun);
uint32_t usb_ram_media_read(uint8_t lun, uint8_t **buf, uint32_t lba_addr, uint32_t len);
uint8_t* usb_ram_media_acquire_write_buf(uint8_t lun, uint32_t blk_addr, uint32_t *len);
uint32_t usb_ram_media_write_ready(uint8_t lun, uint32_t blk_addr, uint32_t len);
uint8_t  usb_ram_media_get_max_lun(void);
uint8_t  usb_ram_media_release(uint8_t cfgidx);
uint8_t big_msg[100];
/* Implementation of mss_usbd_msc_media_t needed by USB MSD Class Driver*/
mss_usbd_msc_media_t usb_ram_media = {
    usb_ram_media_init,
    usb_ram_media_get_capacity,
    usb_ram_media_is_ready,
    usb_ram_media_is_write_protected,
    usb_ram_media_read,
    usb_ram_media_acquire_write_buf,
    usb_ram_media_write_ready,
    usb_ram_media_get_max_lun,
    usb_ram_media_inquiry,
	usb_ram_media_release
};

extern mss_usbd_user_descr_cb_t usb_drive_descriptors_cb;

/*
On board Micron MT46H32M16LF LPDDR is controlled through MSS_DDR.
Size of RAM = 64MiBytes (512Mibits)
SCSI protocol block size = 512
Number of SCSI logical blocks = 0x10000
*/

ram_lun_data_t lun_data = {RAM_DISK_SIZE/_MIN_SS, 1, _MIN_SS};


mss_usbd_msc_scsi_inq_resp_t usb_ram_media_inquiry_data =
{
    0x00u,                                              /* peripheral */
    0x80u,                                              /* removable */
    0x04u,                                              /* version */
    0x02u,                                              /* resp_data_format */
    0x20u,                                              /* additional_length */
    0x00u,                                              /* sccstp */
    0x00u,                                              /* bqueetc */
    0x00u,                                              /* cmd_que */
    "MICRONIC",                                         /* vendor_id[8] */
    "HECTOR_EvalBoard",                                 /* product_id[16] */
    "0100"                                              /* product_rev[4] */
};

/******************************************************************************
  See ram_drive_app.h for details of how to use this function.
*/
void RAM_DRIVE_init (void){

    SYSREG->WDOG_CR = 0x0000;         /*Disable Watch-dog*/

    /*Keep USB PHY out of Reset*/
    MSS_GPIO_set_output(MSS_GPIO_29 , 0);

    /*Initialize USB driver*/
    MSS_USBD_init(MSS_USB_DEVICE_HS);

    /*Assign call-back function Interface needed by USBD driver*/
    MSS_USBD_set_descr_cb_handler(&usb_drive_descriptors_cb);

    /*Assign call-back function handler structure needed by MSD class driver*/
    MSS_USBD_MSC_init(&usb_ram_media, MSS_USB_DEVICE_HS);
}

/******************************************************************************
  Local function definitions
*/
uint8_t* usb_ram_media_inquiry(uint8_t lun, uint32_t *len){
	if(lun == 0){
		*len = sizeof(usb_ram_media_inquiry_data);
		return ((uint8_t*)&usb_ram_media_inquiry_data);
	}else{
		*len = 0;
		return (uint8_t*) 0;
    }
}

uint8_t usb_ram_media_init(uint8_t lun){
    return 1;
}

uint8_t usb_ram_media_get_max_lun (void){
    return 1;
}

uint8_t usb_ram_media_get_capacity (uint8_t lun, uint32_t *no_of_blocks, uint32_t *block_size){
    if(lun == 0){
    	*no_of_blocks = lun_data.number_of_blocks;
    	*block_size = lun_data.lba_block_size;
    }else{
    	*no_of_blocks = 0;
    	*block_size = 0;
    }
 	return 1;
}

uint32_t usb_ram_media_read(uint8_t lun, uint8_t **buf, uint32_t lba_addr, uint32_t len){
    if((lun == 0) && (lba_addr <= RAM_DISK_SIZE)){
		if((lba_addr + len) > RAM_SIZE)
			len = RAM_DISK_SIZE - lba_addr;
		*buf =(uint8_t*) (RAM_DISK_ADDR + lba_addr);
    }else{
    	len  = 0;
    	*buf =(uint8_t*) 0;
    }
	return len;
}

uint8_t* usb_ram_media_acquire_write_buf(uint8_t lun, uint32_t blk_addr, uint32_t *len){
	if(blk_addr > RAM_DISK_SIZE)
		*len = 0;
	else
		*len = RAM_DISK_SIZE - blk_addr;

	if(lun == 0)
		return( (uint8_t*) (RAM_DISK_ADDR + blk_addr));
    else
        return( (uint8_t*) 0 );
}

uint32_t usb_ram_media_write_ready(uint8_t lun, uint32_t blk_addr, uint32_t len){
	return 1;
}

uint8_t usb_ram_media_is_ready(uint8_t lun){
    return 1;
}

uint8_t usb_ram_media_is_write_protected(uint8_t lun){
    return MEM_protected;
}

uint8_t usb_ram_media_release(uint8_t cfgidx){
    return 1;
}


#ifdef __cplusplus
}
#endif
