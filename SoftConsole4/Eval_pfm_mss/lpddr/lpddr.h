/***************************************************************************//**
 * (c) Copyright 2009 Actel Corporation.  All rights reserved.
 * (c) Copyright 2015 Micronic AS.  All rights reserved.
 * 
 *  Micron MT46H32M16LF LPDDR driver API.
 *
 * SVN $Revision:$
 * SVN $Date:$
 */

#ifndef __MT46H32M16LF_LPDDR_H_
#define __MT46H32M16LF_LPDDR_H_

#include <stdint.h>
#include <stdlib.h>

#define RAM_BASE_ADDR	0xA0000000
#define RAM_SIZE		0x04000000					// 512Mb = 64MB (67108864B)
#define RAM_DISK_ADDR	0xA0000000
#define RAM_CACHE_ADDR	0xA2000000
#define RAM_END_ADDR	0xA4000000
#define RAM_CACHE_SIZE	(RAM_END_ADDR - RAM_CACHE_ADDR)
#define RAM_DISK_SIZE	(RAM_SIZE - RAM_CACHE_SIZE)

void MEM_init( void );

void MEM_read_device_id
(
    uint8_t * manufacturer_id,
    uint8_t * device_id
);

void MEM_read
(
    uint32_t address,
    uint8_t * rx_buffer,
    size_t size_in_bytes
);

void MEM_global_unprotect( void );

void MEM_chip_erase( void );

void MEM_erase_64k_block
(
    uint32_t address
);

void MEM_erase_4k_block
(
    uint32_t address
);

uint8_t MEM_get_status( void );

void MEM_program
(
    uint32_t address,
    uint8_t * write_buffer,
    size_t size_in_bytes
);

#endif
