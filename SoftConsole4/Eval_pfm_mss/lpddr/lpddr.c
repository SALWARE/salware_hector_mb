/***************************************************************************//**
 * (c) Copyright 2009 Actel Corporation.  All rights reserved.
 * (c) Copyright 2015 Micronic AS.  All rights reserved.
 * 
 *  Micron MT46H32M16LF LPDDR driver implementation.
 *
 * SVN $Revision:$
 * SVN $Date:$
 */

#include "lpddr.h"

#include "../CMSIS/m2sxxx.h"
#include "../hal/hal.h"
#include "../drivers/mss_uart/mss_uart.h"
#include "string.h"
#include "stdio.h"

int MEM_protected = 1;

/*******************************************************************************
 * LPDDR controller initialization
 */

void MEM_init( void )
{
	MDDR->core.ddrc.DYN_SOFT_RESET_CR = 0x0000;
	MDDR->core.ddrc.DYN_REFRESH_1_CR = 0x27de;
	MDDR->core.ddrc.DYN_REFRESH_2_CR = 0x030f;
	MDDR->core.ddrc.DYN_POWERDOWN_CR = 0x0002;
	MDDR->core.ddrc.DYN_DEBUG_CR = 0x0000;
	MDDR->core.ddrc.MODE_CR = 0x00C1;
	MDDR->core.ddrc.ADDR_MAP_BANK_CR = 0x099f;
	MDDR->core.ddrc.ECC_DATA_MASK_CR = 0x0000;
	MDDR->core.ddrc.ADDR_MAP_COL_1_CR = 0x3333;
	MDDR->core.ddrc.ADDR_MAP_COL_2_CR = 0xffff;
	MDDR->core.ddrc.ADDR_MAP_ROW_1_CR = 0x7777;
	MDDR->core.ddrc.ADDR_MAP_ROW_2_CR = 0x0fff;
	MDDR->core.ddrc.INIT_1_CR = 0x0001;
	MDDR->core.ddrc.CKE_RSTN_CYCLES_CR[0] = 0x4242;
	MDDR->core.ddrc.CKE_RSTN_CYCLES_CR[1] = 0x0008;
	MDDR->core.ddrc.INIT_MR_CR = 0x0033;
	MDDR->core.ddrc.INIT_EMR_CR = 0x0020;
	MDDR->core.ddrc.INIT_EMR2_CR = 0x0000;
	MDDR->core.ddrc.INIT_EMR3_CR = 0x0000;
	MDDR->core.ddrc.DRAM_BANK_TIMING_PARAM_CR = 0x00c0;
	MDDR->core.ddrc.DRAM_RD_WR_LATENCY_CR = 0x0023;
	MDDR->core.ddrc.DRAM_RD_WR_PRE_CR = 0x0235;
	MDDR->core.ddrc.DRAM_MR_TIMING_PARAM_CR = 0x0064;
	MDDR->core.ddrc.DRAM_RAS_TIMING_CR = 0x0108;
	MDDR->core.ddrc.DRAM_RD_WR_TRNARND_TIME_CR = 0x0178;
	MDDR->core.ddrc.DRAM_T_PD_CR = 0x0033;
	MDDR->core.ddrc.DRAM_BANK_ACT_TIMING_CR = 0x1947;
	MDDR->core.ddrc.ODT_PARAM_1_CR = 0x0010;
	MDDR->core.ddrc.ODT_PARAM_2_CR = 0x0000;
	MDDR->core.ddrc.ADDR_MAP_COL_3_CR = 0x3300;
	MDDR->core.ddrc.MODE_REG_RD_WR_CR = 0x0000;
	MDDR->core.ddrc.MODE_REG_DATA_CR = 0x0000;
	MDDR->core.ddrc.PWR_SAVE_1_CR = 0x0514;
	MDDR->core.ddrc.PWR_SAVE_2_CR = 0x0000;
	MDDR->core.ddrc.ZQ_LONG_TIME_CR = 0x0200;
	MDDR->core.ddrc.ZQ_SHORT_TIME_CR = 0x0040;
	MDDR->core.ddrc.ZQ_SHORT_INT_REFRESH_MARGIN_CR[0] = 0x0012;
	MDDR->core.ddrc.ZQ_SHORT_INT_REFRESH_MARGIN_CR[1] = 0x0002;
	MDDR->core.ddrc.PERF_PARAM_1_CR = 0x4000;
	MDDR->core.ddrc.HPR_QUEUE_PARAM_CR[0] = 0x80f8;
	MDDR->core.ddrc.HPR_QUEUE_PARAM_CR[1] = 0x0007;
	MDDR->core.ddrc.LPR_QUEUE_PARAM_CR[0] = 0x80f8;
	MDDR->core.ddrc.LPR_QUEUE_PARAM_CR[1] = 0x0007;
	MDDR->core.ddrc.WR_QUEUE_PARAM_CR = 0x0200;
	MDDR->core.ddrc.PERF_PARAM_2_CR = 0x0001;
	MDDR->core.ddrc.PERF_PARAM_3_CR = 0x0000;
	MDDR->core.ddrc.DFI_RDDATA_EN_CR = 0x0003;
	MDDR->core.ddrc.DFI_MIN_CTRLUPD_TIMING_CR = 0x0003;
	MDDR->core.ddrc.DFI_MAX_CTRLUPD_TIMING_CR = 0x0040;
	MDDR->core.ddrc.DFI_WR_LVL_CONTROL_CR[0] = 0x0000;
	MDDR->core.ddrc.DFI_WR_LVL_CONTROL_CR[1] = 0x0000;
	MDDR->core.ddrc.DFI_RD_LVL_CONTROL_CR[0] = 0x0000;
	MDDR->core.ddrc.DFI_RD_LVL_CONTROL_CR[1] = 0x0000;
	MDDR->core.ddrc.DFI_CTRLUPD_TIME_INTERVAL_CR = 0x0309;
	MDDR->core.ddrc.AXI_FABRIC_PRI_ID_CR = 0x0000;
	MDDR->core.ddrc.ECC_INT_CLR_REG = 0x0000;

	MDDR->core.phy.LOOPBACK_TEST_CR = 0x0000;
	MDDR->core.phy.CTRL_SLAVE_RATIO_CR = 0x0080;
	MDDR->core.phy.DATA_SLICE_IN_USE_CR = 0x0003;
	MDDR->core.phy.DQ_OFFSET_CR[0] = 0x00000000;
	MDDR->core.phy.DQ_OFFSET_CR[2] = 0x0000;
	MDDR->core.phy.DLL_LOCK_DIFF_CR = 0x000B;
	MDDR->core.phy.FIFO_WE_SLAVE_RATIO_CR[0] = 0x0040;
	MDDR->core.phy.FIFO_WE_SLAVE_RATIO_CR[1] = 0x0401;
	MDDR->core.phy.FIFO_WE_SLAVE_RATIO_CR[2] = 0x4010;
	MDDR->core.phy.FIFO_WE_SLAVE_RATIO_CR[3] = 0x0000;
	MDDR->core.phy.LOCAL_ODT_CR = 0x0001;
	MDDR->core.phy.RD_DQS_SLAVE_RATIO_CR[0] = 0x0040;
	MDDR->core.phy.RD_DQS_SLAVE_RATIO_CR[1] = 0x0401;
	MDDR->core.phy.RD_DQS_SLAVE_RATIO_CR[2] = 0x4010;
	MDDR->core.phy.WR_DATA_SLAVE_RATIO_CR[0] = 0x0040;
	MDDR->core.phy.WR_DATA_SLAVE_RATIO_CR[1] = 0x0401;
	MDDR->core.phy.WR_DATA_SLAVE_RATIO_CR[2] = 0x4010;
	MDDR->core.phy.WR_RD_RL_CR = 0x0021;
	MDDR->core.phy.RDC_WE_TO_RE_DELAY_CR = 0x0003;
	MDDR->core.phy.USE_FIXED_RE_CR = 0x0001;
	MDDR->core.phy.USE_RANK0_DELAYS_CR = 0x0001;
	MDDR->core.phy.CONFIG_CR = 0x0009;
	MDDR->core.phy.DYN_RESET_CR = 0x01;
	MDDR->core.ddrc.DYN_SOFT_RESET_CR = 0x01;

	while ((MDDR->core.ddrc.DDRC_SR) == 0x0000);
}

/*******************************************************************************
 *
 */
void MEM_read_device_id
(
    uint8_t * manufacturer_id,
    uint8_t * device_id
)
{
    uint8_t read_buffer[2] = {0,0};
    
    *manufacturer_id = read_buffer[0];
    *device_id = read_buffer[1];
}

/*******************************************************************************
 *
 */
void MEM_read
(
    uint32_t address,
    uint8_t * rx_buffer,
    size_t size_in_bytes
)
{


	if(address % 4)
		printf("HECTOR: WARNING: write address not multiple of 4\r\n");

	if((uint32_t)rx_buffer % 4)
		printf("HECTOR: WARNING: buffer address not multiple of 4\r\n");

	if(size_in_bytes % 4)
		printf("HECTOR: WARNING: data length not multiple of 4\r\n");

	uint8_t* ptr_mem  = (uint8_t*) (RAM_BASE_ADDR + address);
    uint8_t* ptr_data = rx_buffer;
    int i;

	for (i=0; i < size_in_bytes; i++, ptr_mem++, ptr_data++){
		*ptr_data  = ((uint8_t) *ptr_mem);
	}
}

/*******************************************************************************
 *
 */
void MEM_global_unprotect( void )
{
	MEM_protected = 0;
	MEM_chip_erase();

}

/*******************************************************************************
 *
 */
void MEM_chip_erase( void )
{
	uint8_t* ptr_mem  = (uint8_t*) RAM_BASE_ADDR;
	uint8_t* ptr_stop = (uint8_t*) ptr_mem + RAM_SIZE;

	for (; ptr_mem < ptr_stop; ptr_mem++) {
		*ptr_mem  = 0;
	}
}

/*******************************************************************************
 *
 */
void MEM_erase_4k_block
(
    uint32_t address
)
{
	uint8_t* ptr_mem  = (uint8_t*) RAM_BASE_ADDR + address;
	uint8_t* ptr_stop = (uint8_t*) ptr_mem + 4*1024;

	for (; ptr_mem < ptr_stop; ptr_mem++) {
		*ptr_mem  = 0;
	}
}

/*******************************************************************************
 *
 */
void MEM_program
(
    uint32_t  address,
    uint8_t*  write_buffer,
    size_t    size_in_bytes
)
{
	uint32_t* ptr_mem  = (uint32_t*) (RAM_BASE_ADDR + address);
	uint32_t* ptr_mem_end = (uint32_t*) (RAM_BASE_ADDR + address + size_in_bytes);
    uint32_t* ptr_data = (uint32_t*) write_buffer;

	if((address + size_in_bytes) > RAM_SIZE){
		printf("HECTOR: writing %d bytes at %08X out of the memory bounds\r\n", size_in_bytes, (unsigned int) address);
		return;
	}

	if(address % 4)
		printf("HECTOR: WARNING: write address not multiple of 4\r\n");

	if((uint32_t)write_buffer % 4)
		printf("HECTOR: WARNING: buffer address not multiple of 4\r\n");

	if(size_in_bytes % 4)
		printf("HECTOR: WARNING: data length not multiple of 4\r\n");


	for (; ptr_mem < ptr_mem_end;) *(ptr_mem++)  = *(ptr_data++);

}

/*******************************************************************************
 *
 */
uint8_t MEM_get_status( void )
{
    uint8_t status = 0;
    return status;
}


