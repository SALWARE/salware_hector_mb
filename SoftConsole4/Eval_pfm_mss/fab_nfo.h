/**
  ******************************************************************************
  * @file      fab_nfo.h
  * @author    Marek Laban, Marcel Kleja, MICRONIC a.s., LabHC
  * @version   v1.0
  * @note      Created in frame of HECTOR project (No. 644052).
  * @copyright MICRONIC a.s., LabHC
  * @date      16-May-2016
  * @revision  v1.0 created (ML)
  * @brief     Contains registers and values used at FABRIC control block
  *            (ctrl.vhd)
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef FAB_NFO_H_
#define FAB_NFO_H_

/** @defgroup FPGA_register_map
  * @{
  */
#define CMD_ADDR        0x70000000  /**< Command address                      */
#define STATE_ADDR      0x70000004  /**< Acq State address                    */
#define STAGE_ADDR      0x70000008  /**< Actual ram interface stage           */
#define FABIN_ADDR      0x7000000C  /**< Fabric input register (from MSS)     */
#define FABOUT_ADDR     0x70000010  /**< Fabric output register (to MSS)      */
#define FABST_ADDR      0x70000014  /**< Fabric status register (to MSS)      */
#define WSTART_ADDR     0x70000018  /**< Write to RAM interface start address */
#define WSIZE_ADDR      0x7000001C  /**< Write to RAM interface stream size   */
#define RSTART_ADDR     0x70000020  /**< Read from RAM interface start address*/
#define RSIZE_ADDR      0x70000024  /**< Read from RAM interface stream size  */
#define MSSIO_OUT_ADDR  0x70000028  /**< User optional register address       */
#define MSSIO_IN_ADDR   0x7000002C  /**< User optional register address       */
#define CRYP_IN_ADDR    0x70000030  /**< Crypto word input                    */
#define CRYP_OUT_ADDR   0x70000034  /**< Crypto word output                   */
//    -- Crypto control word
//    --  Number of bits  | 7  |  1   |  8   |    8     |    8      |
//    --  Function:       | CS | R/W  | ADDR |  in_nmb  |  out_nmb  |
//    --
//    --  CS   - chips select for the current block
//    --  R/W  - 0 read   from instance
//    --       - 1 write  to instance
//    --  ADDR - reg. address of the instance
//    --  in_words  - number of words send from PC to motherboard
//    --  out_words - number of words send from motherboard to PC
#define CRYP_CTRL_ADDR  0x70000038  /**< Crypto control word                  */
#define PUF_STAT_ADDR   0x7000003C  /**< Crypto word output                   */

/** @defgroup Acquisition_states
  * @{
  */
#define OP_IDLE_STATE 0xFFFFFFFF	/**< idle state                 */
#define OP_BUSY_STATE 0x00000001	/**< busy state                 */
#define OP_OFLW_STATE 0x00000002    /**< FIFO buffer overflow state */
#define OP_ERR_STATE  0x00000003    /**< Other error of operation   */


/** @defgroup Commands
  * @{
  */
#define CMD_OP_IDLE 0x00000000 /**< operation start command */
#define CMD_OP_ST   0x00000001 /**< operation start command */
#define CMD_SETDB1  0x00000002 /**< set daughter board 1    */
#define CMD_SETDB2  0x00000003 /**< set daughter board 2    */
#define CMD_SSI_LOAD_SEND_LOW 0x00000006 /**< get TRNG status                 */
#define CMD_GET_TIME          0x00000007 /**< get time                        */
#define CMD_SSI_LOAD_HIGH     0x00000008 /**< load high 32 bits of SSI word   */
#define CMD_READ_SSI_HIGH     0x00000009 /**< read high 32 bits from SSI word */
#define CMD_READ_SSI_LOW      0x0000000A /**< read low 32 bits from SSI word  */
#define CMD_CRYPTO_ENA        0x0000000B /**< start crypto operation          */

#endif /* FAB_NFO_H_ */
