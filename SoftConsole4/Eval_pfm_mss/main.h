/**
  ******************************************************************************
  * @file      main.h
  * @author    Marek Laban, Marcel Kleja, MICRONIC a.s., LabHC
  * @version   v1.0
  * @note      Created in frame of HECTOR project (No. 644052).
  * @copyright MICRONIC a.s., LabHC
  * @date      16-May-2016
  * @revision  v1.0 created (ML)
  * @brief     Contains public functions and prototypes for the HECTOR
  *            evaluation platform main file
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MAIN_H_
#define MAIN_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Functions used to change words at main state machine */
void set_mss_cmd(uint32_t new_cmd);
void set_fab_cmd(uint32_t new_word);
void set_stream_size(uint32_t stream_word);
void set_rsp_flag(void);

#endif /* MAIN_H_ */
