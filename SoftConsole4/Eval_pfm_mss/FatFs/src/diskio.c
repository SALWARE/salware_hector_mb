/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2007        */
/*                                                  (C)MICRONIC, 2015    */
/*-----------------------------------------------------------------------*/
/* This is a stub disk I/O module that acts as front end of the existing */
/* disk I/O modules and attach it to FatFs module with common interface. */
/*-----------------------------------------------------------------------*/

#include "diskio.h"
#include "ffconf.h"
#include <string.h>
#include <stdio.h>
#include "../../lpddr/lpddr.h"
#include "../../ram_drive_app.h"

/*-----------------------------------------------------------------------*/
/* Correspondence between physical drive number and physical drive.      */
/*-----------------------------------------------------------------------*/

#define USB		0
#define MMC		1
#define ATA		2

/*-----------------------------------------------------------------------*/
/* Initialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive number (0..) */
)
{
    return 0;   //success
}

/*-----------------------------------------------------------------------*/
/* Return Disk Status                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE drv		/* Physical drive number (0..) */
)
{
	return 0;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/
DRESULT disk_read (
	BYTE drv,		/* Physical drive number (0..) */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Sector address (LBA) */
	BYTE count		/* Number of sectors to read (1..255) */
)
{
	uint32_t* ptr_mem;
	uint32_t* ptr_mem_end;
	uint32_t* ptr_data;
    if(0u != drv)
        return(RES_ERROR);

    if(((sector + count)*(_MIN_SS)) > RAM_DISK_SIZE)
        return(RES_ERROR);

	ptr_mem      = (uint32_t*) (RAM_DISK_ADDR + ( sector * _MIN_SS));
	ptr_mem_end  = (uint32_t*) (RAM_DISK_ADDR + ((sector+count) * _MIN_SS));
	ptr_data     = (uint32_t*)  buff;

    for(    ; ptr_mem < ptr_mem_end; ptr_mem++, ptr_data++){
    	*ptr_data = *ptr_mem;
    }


    return(RES_OK);

}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/
/* The FatFs module will issue multiple sector transfer request
/  (count > 1) to the disk I/O layer. The disk function should process
/  the multiple sector transfer properly Do. not translate it into
/  multiple single sector transfers to the media, or the data read/write
/  performance may be drastically decreased. */

#if _READONLY == 0
DRESULT disk_write (
	BYTE pdrv,			/* Physical drive number (0..) */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Sector address (LBA) */
	BYTE count			/* Number of sectors to write (1..255) */
)
{
	uint32_t* ptr_mem;
	uint32_t* ptr_mem_end;
	uint32_t* ptr_data;

    if(0u != pdrv)
        return(RES_ERROR);

    if(((sector + count)*(_MIN_SS)) > RAM_DISK_SIZE)
        return(RES_ERROR);

	ptr_mem      = (uint32_t*) (RAM_DISK_ADDR + ( sector * _MIN_SS));
	ptr_mem_end  = (uint32_t*) (RAM_DISK_ADDR + ((sector+count) * _MIN_SS));
	ptr_data     = (uint32_t*)  buff;

    for(    ; ptr_mem < ptr_mem_end; ptr_mem++, ptr_data++){
    	*ptr_mem = *ptr_data;
    }

    return(RES_OK);

}
#endif /* _READONLY */



/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/
uint32_t sect_size1;
uint32_t sect_size2;

/*
 Number of Drives(Logical Units) is always 1.
 _DRIVES must be equal to 1 in ffconfig.h
 */

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive number (0..) */
	BYTE ctrl,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	UINT *result = (UINT *)buff;

	if(0u != pdrv)
        return(RES_ERROR);

	switch (ctrl) {
    case CTRL_SYNC:
    	break;
    case CTRL_POWER:
    	break;
    case CTRL_LOCK:
    	break;
    case CTRL_EJECT:

    	break;
    case GET_SECTOR_COUNT:
       	*result = RAM_DISK_SIZE/512;
       	return RES_OK;
    case GET_SECTOR_SIZE:
       	*result = 512;
        return RES_OK;
    case GET_BLOCK_SIZE:
    	*result = 1;
    	break;
    default:
    	break;
    }
	return RES_OK;

}
