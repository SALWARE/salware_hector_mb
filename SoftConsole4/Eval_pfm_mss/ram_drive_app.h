/***************************************************************************//**
 * (c) Copyright 2012-2013 Microsemi SoC Products Group.  All rights reserved.
 * (c) Copyright 2015 Micronic, a. s.  All rights reserved.
 *
 * USB MSC Class Storage Device example aplication to demonstrate the
 * SmartFusion2 MSS USB operations in device mode.
 *
 *
 * Header for ram_drive_app.h
 *
 * SVN $Revision: 5468 $
 * SVN $Date: 2013-03-29 15:38:01 +0530 (Fri, 29 Mar 2013) $
 */

#ifndef RAM_DRIVE_APP_H_
#define RAM_DRIVE_APP_H_

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif


#define MSG_NO			 0
#define MSG_ERROR		 1
#define MSG_INFO		 2
#define MSG_DATA		 4
#define MSG_FATFS		16

#define DEBUG_UARTMSG	(MSG_ERROR + MSG_INFO)
/******************************************************************************
  Exported functions from this file
*/

/***************************************************************************//**
  @brief RAM_DRIVE_init()


  @param
    This function does not take any parameters.
  @return
    This function does not return a value.

  Example:
  @code
  @endcode
*/
void
RAM_DRIVE_init
(
    void
);

///////////////////////////////////////////////////////////////////////////////////////
uint8_t* usb_ram_media_inquiry( uint8_t lun, uint32_t *len);
uint8_t  usb_ram_media_init( uint8_t lun);
uint8_t  usb_ram_media_get_max_lun(void);
uint8_t  usb_ram_media_get_capacity( uint8_t lun, uint32_t *no_of_blocks, uint32_t *block_size);
uint32_t usb_ram_media_read( uint8_t lun, uint8_t **buf, uint32_t lba_addr, uint32_t len);
uint8_t* usb_ram_media_acquire_write_buf( uint8_t lun, uint32_t blk_addr, uint32_t *len);
uint32_t usb_ram_media_write_ready(uint8_t lun, uint32_t blk_addr, uint32_t len);
uint8_t  usb_ram_media_is_ready(uint8_t lun);
uint8_t  usb_ram_media_is_write_protected(uint8_t lun);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifdef __cplusplus
}
#endif

#endif /* ram_DRIVE_APP_H_*/
