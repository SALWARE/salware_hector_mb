/**
  ******************************************************************************
  * @file      my_uart.c
  * @author    Marek Laban, Marcel Kleja, MICRONIC a.s., LabHC
  * @version   v1.0
  * @note      Created in frame of HECTOR project (No. 644052).
  * @copyright MICRONIC a.s., LabHC
  * @date      16-May-2016
  * @revision  v1.0 created (ML)
  * @brief     UART functions used on Hector Evaluation mother board
  *            - UART initialization
  *            - UART routines
  *            - Packet receiving, recognition and transfer routines
  *  @verbatim
  *
  *          UART configuration:
  *            - 115200 bauds
  *             - 8 bit
  *             - No parity
  *             - One stop bit
  *
  *          UART receiving is configured in interrupt mode and UART
  *          transmitting in polling mode.
  *
  *
  *  @endverbatim
  *
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "drivers/mss_uart/mss_uart.h"
#include "my_uart.h"
#include "pkt_nfo.h"
#include "lpddr/lpddr.h"
#include "main.h"
#include "mss_instr.h"
#include "fab_nfo.h"
#include "hal/hal.h"
#include "time.h"

/* Private define ------------------------------------------------------------*/
/** @defgroup UART_STATES
  * @brief UART state machine definitions
  * @{
  */
#define HEAD_AWAITING    0
#define PRFX_AWAITING    1
#define NULLS_COLLECT    2
#define PCODE_COLLECT    3
#define PRDAT_COLLECT    4
#define MSS_CMD_COLLECT  5
#define RES_WORD_COLLECT 6
#define FILENAME_COLLECT 7
#define DSIZE_COLLECT    8
#define FAB_WORD_COLLECT 9
#define CMD_WORD_COLLECT 10
#define PCRYP_COLLECT    11
#define PDATA_COLLECT    12

#define HECTOR_GREETING 1

uint8_t static pkt_state = HEAD_AWAITING;   /**< State of packet recognition */


static char file_name[20] = {"NO_NAME.bin"}; /*< configured filename of acquisition*/
static uint8_t pkt_type;
static uint8_t mb2db_nmb, db2mb_nmb;
/**
  * @brief  Initialization of MB UART
  * @param  None
  * @retval None
  */
void my_uart_cfg(void){
  MSS_UART_init(&g_mss_uart1,
		  	  	 MSS_UART_460800_BAUD,
                 MSS_UART_DATA_8_BITS | MSS_UART_NO_PARITY | MSS_UART_ONE_STOP_BIT);

  MSS_UART_set_rx_handler(&g_mss_uart1,
                          uart1_rx_handler,
                          MSS_UART_FIFO_SINGLE_BYTE);

  #ifdef HECTOR_GREETING
    MSS_UART_polled_tx_string(&g_mss_uart1, (uint8_t*) "HECTOR: START\n\r");
  #endif
}

/**
  * @brief  Interrupt handler of UART receiving
  *       - recognize packets, set new instructions etc.
  * @param[in]  mss_uart_instance_t Name of interrupt instance set by my_uart_cfg function
  * @retval None
  */
void uart1_rx_handler(mss_uart_instance_t * this_uart){
  uint8_t rx_buff;              /**< Receiving buffer - used for UART reading */
  static uint8_t words_cnt;     /**< Actual number of words */
  static uint32_t big_word;     /**< Packet content is collected to this word */
  static uint32_t big_word_high;/**< Low word for the inserting to 64bit fifo */
//  ela_t uart_timeout;

  /* If interrupt occurs read a 1 byte from buffer */
  if(MSS_UART_get_rx(this_uart, &rx_buff, 1) == 0)
    return;

  /* If there is big time space between receiving packet, discard previous packet */
//  if (t_elapsed(uart_timeout) == TIM_ELA)
//	pkt_state = HEAD_AWAITING;
//  else{
//    set_elapsed(uart_timeout,1000);
//  }

  /* Use data by actual packet state */
  switch(pkt_state){
  case HEAD_AWAITING:
    if(rx_buff == PHEAD){
      pkt_state = PRFX_AWAITING;
    }
    break;

  /* Prefix awaiting */
  case PRFX_AWAITING:
    switch(rx_buff){

      /* When code prefix is received */
      case PCODE:
        /* Set new packet state and reset words counter*/
        pkt_state = NULLS_COLLECT;
        pkt_type  = PCODE;
        words_cnt = 0;
        break;

      /* When read data prefix is received */
      case PRDAT:
        /* Set new packet state and reset words counter*/
        pkt_state = NULLS_COLLECT;
        pkt_type  = PRDAT;
        words_cnt = 0;
        break;

      /* When write data prefix is received */
      case PWDAT:
        /* Set new packet state and reset words counter*/
        pkt_state = NULLS_COLLECT;
        pkt_type  = PWDAT;
        words_cnt = 0;
        break;

      /* When write data prefix is received */
      case PFAB:
        /* Set new packet state and reset words counter*/
        pkt_state = NULLS_COLLECT;
        pkt_type  = PFAB;
        words_cnt = 0;
        break;

      /* When crypto packet is received */
      case PCRYP:
        /* Set new packet state and reset words counter*/
        pkt_state = NULLS_COLLECT;
        pkt_type  = PCRYP;
        words_cnt = 0;
        break;

      /* When HEAD is received again -> do nothing */
      case PHEAD:
        break;

      /* Every bad prefix -> set head awaiting */
      default: pkt_state = HEAD_AWAITING; break;

      }
      break;

      /* When code is received */
      case NULLS_COLLECT:
        /* Check if correct number of nulls is received */
        if(rx_buff == 0 ){
          words_cnt++;
        }
        else
          pkt_state = HEAD_AWAITING;

        /* When nulls is received, switch to FAB word receiving */
        if(words_cnt == PCODE_NULLS){
          words_cnt = 0;
          switch(pkt_type){
          case PCODE: pkt_state = RES_WORD_COLLECT; break;
          case PRDAT: pkt_state = FILENAME_COLLECT; break;
          case PWDAT: pkt_state = FILENAME_COLLECT; break;
          case PFAB : pkt_state = FAB_WORD_COLLECT; break;
          case PCRYP: pkt_state = PCRYP_COLLECT;    break;
          default:    pkt_state = HEAD_AWAITING;
          }
        }
        break;

      /* Reserved word collecting */
      case RES_WORD_COLLECT:

    	/* Every new 8bites move to 32bit fifo */
        big_word = ((big_word << 8) | rx_buff);
        words_cnt ++;

        /* When whole word is loaded switch to MSS command collecting */
        if(words_cnt == 4){
          words_cnt = 0;
          pkt_state =  MSS_CMD_COLLECT;
          set_fab_cmd(big_word);
        }
        break;

      /* MSS command collecting */
      case MSS_CMD_COLLECT:
        /* Every new 8bites move to 32bit fifo */
        big_word = ((big_word << 8) | rx_buff);
        words_cnt ++;

        /* When whole word is loaded switch to Head awaiting */
        if(words_cnt == 4){
          words_cnt = 0;
          pkt_state = HEAD_AWAITING;
          set_mss_cmd(big_word);
        }
        break;

      /* File name collecting */
      case FILENAME_COLLECT:
        /* save received file name -> file name is terminated by space character */
        file_name[words_cnt] = (char)rx_buff;
        if (file_name[words_cnt] == ' '){
          file_name[words_cnt] = 0;
          pkt_state = DSIZE_COLLECT;
          words_cnt=0;
        }
        else
          words_cnt++;
        break;

      /* Save size of measured file */
      case DSIZE_COLLECT:
        /* Size is 32 bit file. Every 8 bits is moved to 32bit fifo */
        big_word = ((big_word << 8) | rx_buff);
        words_cnt ++;

        /* when file size is received switch to next packet awaiting */
        if(words_cnt == 4){
          words_cnt = 0;
          pkt_state = HEAD_AWAITING;
          set_stream_size(big_word);
          if(pkt_type == PRDAT)
            set_mss_cmd(GEN_DATA_COLLECT);
          else
            set_mss_cmd(MB_LOAD_FILE);
        }
        break;
      /* Fabric 32-bit word collecting */
      case FAB_WORD_COLLECT:
        /* Every new 8bites move to 32bit fifo */
        big_word = ((big_word << 8) | rx_buff);
        words_cnt ++;

        /* When whole word is loaded switch to MSS command collecting */
        if(words_cnt == 4){
          words_cnt = 0;
          HW_set_32bit_reg(FABIN_ADDR,big_word);
          pkt_state =  CMD_WORD_COLLECT;
        }
        break;

      /* Fabric command 32-bit word collecting */
      case CMD_WORD_COLLECT:
        /* Every new 8bites move to 32bit fifo */
        big_word = ((big_word << 8) | rx_buff);
        words_cnt ++;

        /* When whole word is loaded switch to Head awaiting */
        if(words_cnt == 4){
          words_cnt = 0;
          HW_set_32bit_reg(CMD_ADDR,big_word);
          pkt_state = HEAD_AWAITING;
          set_mss_cmd(SEND_FAB_RSP);
        }
        break;

      case PCRYP_COLLECT:
        /* Every new 8bites move to 32bit fifo */
        big_word = ((big_word << 8) | rx_buff);
        words_cnt ++;

        /* When whole word is loaded switch to Head awaiting */
        if(words_cnt == 4){
          words_cnt = 0;

          /* Load number of words which will be received */
          mb2db_nmb  = (uint8_t) ((big_word & 0xFF00) >> 8);
          /* 64bit words are received but 32 bit words are inserted to FPGA */
          mb2db_nmb  = (uint8_t)(mb2db_nmb * 2);

          /* word is limited to 1024 bits */
          if(mb2db_nmb > 32){
            mb2db_nmb = 32;
          }

          /* Number of words is limited to 1024 bits */
          if(db2mb_nmb > 32){
            db2mb_nmb = 32;
          }

          /* Load the control word to FPGA */
          HW_set_32bit_reg(CRYP_CTRL_ADDR,big_word);

          /* If no data will be followed start data processing */
          if(mb2db_nmb != 0){
            pkt_state = PDATA_COLLECT;
          }
          else{
            pkt_state = HEAD_AWAITING;;
            HW_set_32bit_reg(CMD_ADDR,CMD_CRYPTO_ENA);
            set_rsp_flag();
          }
	      }
        break;
      case PDATA_COLLECT:
        /* Every new 8bits move to 32bit fifo */
        big_word = ((big_word << 8) | rx_buff);
        words_cnt ++;

        if(words_cnt == 4){
        words_cnt = 0;

          /* Low significant word is inserted first */
          if((mb2db_nmb % 2 == 0) && (mb2db_nmb != 0)){
            mb2db_nmb--;
            big_word_high = big_word;
          }
          else{
            mb2db_nmb--;
            /* Load lower word to the position (data are shifted internally) */
            HW_set_32bit_reg(CRYP_IN_ADDR,big_word);
            /* 2 words are loaded to 64bit fifo register */
            HW_set_32bit_reg(CRYP_IN_ADDR,big_word_high);
            /* When whole data was loaded switch to Head awaiting */
          }

          if(mb2db_nmb == 0){
            pkt_state =  HEAD_AWAITING;
            HW_set_32bit_reg(CMD_ADDR,CMD_CRYPTO_ENA);
            set_rsp_flag();
          }
          else{
            pkt_state = PDATA_COLLECT;
          }
        }
        break;
      default: pkt_state = HEAD_AWAITING; break;
  }
}


/**
  * @brief  Send 12 bytes response after every proper received packet
  * @param[in]  res_word     Reserved word for general purpose
  * @param[in]  progress     progress of current or last operation
  * @param[in]  state        state of mss operation
  * @param[in]  gpio_state   state of GPIOs connected to fabric
  * @param[in]  response     MSS response to received packet
  * @retval None
  */
void send_resp(uint32_t res_word, uint8_t progress, uint8_t state, uint8_t gpio_state, uint8_t response){
  uint8_t tx_packet[STAT_PKT_SIZE];  /*< Transmitted packet */
  int i;                             /*< Auxiliary variable*/

  /* Packet head and prefix making */
  tx_packet[0] = PHEAD;
  tx_packet[1] = PSTAT;
  tx_packet[2] = 0;
  tx_packet[3] = 0;

  /* Insert reserved word */
  for(i=0;i<4;i++){
    tx_packet[4 + i] = (uint8_t)((res_word >> (8*(3-i))) & 0xFF);
  }

  tx_packet[8]  = progress;
  tx_packet[9]  = state;
  tx_packet[10] = gpio_state;
  tx_packet[11] = response;

  /* Send packet */
  MSS_UART_polled_tx(&g_mss_uart1, tx_packet, STAT_PKT_SIZE);
}

/**
  * @brief  Send 12 bytes response after every proper received packet
  * @param[in]  none
  * @retval None
  */
void send_fab_resp(void){
  uint8_t tx_packet[STAT_PKT_SIZE];  /*< Transmitted packet */
  int i;                 /*< Auxiliary variable*/
  uint32_t fabout_word;
  uint32_t fabstat_word;

  fabout_word  = HW_get_32bit_reg(FABOUT_ADDR);
  fabstat_word = HW_get_32bit_reg(FABST_ADDR);

  /* Packet head and prefix making */
  tx_packet[0] = PHEAD;
  tx_packet[1] = PFBST;
  tx_packet[2] = 0;
  tx_packet[3] = 0;

  /* Insert reserved word */
  for(i=0;i<4;i++){
    tx_packet[4 + i] = (uint8_t)((fabout_word >> (8*(3-i))) & 0xFF);
  }

  /* Insert reserved word */
  for(i=0;i<4;i++){
    tx_packet[8 + i] = (uint8_t)((fabstat_word >> (8*(3-i))) & 0xFF);
  }

  /* Send packet */
  MSS_UART_polled_tx(&g_mss_uart1, tx_packet, STAT_PKT_SIZE);
}


/**
  * @brief  Send response after every proper received packet
  * @param[in]  none
  * @retval None
  */
void send_cryp_resp(void){
  uint8_t tx_packet  [CRYP_PKT_SIZE]; /*< Transmitted packet */
  int i,j;                            /*< Auxiliary variable */
  uint32_t tmp_array[CRYP_FIFO_SIZE]; /*< Array for reading entire fifo */
  uint32_t tmp_word;                  /*< Temporary word     */

  tx_packet[0] = PHEAD;
  tx_packet[1] = PCST;
  tx_packet[2] = 0;
  tx_packet[3] = 0;

  db2mb_nmb =  (uint8_t) (HW_get_32bit_reg(CRYP_CTRL_ADDR) & 0xFF);
  db2mb_nmb =  (uint8_t) (db2mb_nmb * 2);

  /* Read status of the daugter board and insert it to the packet */
  tmp_word  =  HW_get_32bit_reg(PUF_STAT_ADDR);
  tx_packet[4] = (uint8_t) ((tmp_word >> 24) & 0xFF);
  tx_packet[5] = (uint8_t) ((tmp_word >> 16) & 0xFF);
  tx_packet[6] = (uint8_t) ((tmp_word >> 8 ) & 0xFF);
  tx_packet[7] = (uint8_t) ((tmp_word >> 0 ) & 0xFF);

  /* Read every word from the fifo */
  for(i=(db2mb_nmb-1); i>=0; i--){
    tmp_array[i] = HW_get_32bit_reg(CRYP_OUT_ADDR);
  }

  /* Insert the words to the output packet */
  for(i=0;i<db2mb_nmb;i++){
    tmp_word = tmp_array[i];
    for(j=0;j<4;j++){
      /* Uart data are 8bit - fifo data are 32bit words */
      /*     -> transform the words to bytes            */
      tx_packet[8 + ((i*4) + j)] = (uint8_t)((tmp_word >> (8*(3-j))) & 0xFF);
    }
  }

  /* Send packet -> header (4 bytes) and data (4*number of words bytes) */
  MSS_UART_polled_tx(&g_mss_uart1, tx_packet, (uint32_t)((db2mb_nmb*4)+8));
}




/**
  * @brief      Function return pointer to acquisition filename
  * @param[in]  None
  * @retval     Pointer to acquisition filename
  */
char* get_filename(void){
  return file_name;
}

