/**
  ******************************************************************************
  * @file      mss_instr.h
  * @author    Marek Laban, Marcel Kleja, MICRONIC a.s., LabHC
  * @version   v1.0
  * @note      Created in frame of HECTOR project (No. 644052).
  * @copyright MICRONIC a.s., LabHC
  * @date      16-May-2016
  * @revision  v1.0 created (ML)
  * @brief     Contains definition of functions implemented in ARM firmware
  *
  ******************************************************************************
  */

#ifndef MSS_INSTR_H_
#define MSS_INSTR_H_

// MSS_INSTRUCTIONS
#define MB_IDLE           0x00  /**< Mother board idle state - nothing to do */
#define MB_FAB_CMD        0x01	/**< send command to fabric */
#define GEN_DATA_COLLECT  0x02  /**< Generator data collect
                                     (used only with PRDAT packet) */
#define MAKE_FAB_RST      0x04  /**< Make fabric reset */
#define MAKE_GEN_RST      0x05  /**< Make generator reset */
#define MB_MOUNT_DISK     0x06  /**< Mount disk to the PC*/
#define MB_GET_OP_STATE   0x07  /**< Get operation state */
#define MB_MAKE_FSYS      0x08  /**< Make filesystem */
#define MB_GPIO_CGF       0x09  /**< Gpio input/output configuration */
#define MB_GPIO_SET       0x0A  /**< Gpio output state configuration */
#define MB_OP_RST         0x0B  /**< MSS operation reset */
#define SEND_FAB_RSP      0x0C  /**< send fabric response (used only with
                                     PFAB packet) */
#define MB_LOAD_FILE      0x0D  /**< Load file from mother board (used only with
                                     PWDAT packet) */
#define SEND_CRYP_RSP     0x0E  /**< Check delay signal and send response */
#endif /* MSS_INSTR_H_ */
