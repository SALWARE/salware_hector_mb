#ifndef test3_HW_PLATFORM_H_
#define test3_HW_PLATFORM_H_
/*****************************************************************************
*
*Created by Microsemi SmartDesign  Wed Mar 09 18:53:19 2016
*
*Memory map specification for peripherals in test3
*/

/*-----------------------------------------------------------------------------
* CM3 subsystem memory map
* Master(s) for this subsystem: CM3 FABRIC2MSSFIC2 
*---------------------------------------------------------------------------*/
#define TEST3_SB_MSS_0                  0x40020800U


#endif /* test3_HW_PLATFORM_H_*/
