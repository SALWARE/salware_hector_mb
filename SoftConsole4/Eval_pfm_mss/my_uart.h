/**
  ******************************************************************************
  * @file      my_uart.h
  * @author    Marek Laban, Marcel Kleja, MICRONIC a.s., LabHC
  * @version   v1.0
  * @note      Created in frame of HECTOR project (No. 644052).
  * @copyright MICRONIC a.s., LabHC
  * @date      16-May-2016
  * @revision  v1.0 created (ML)
  * @brief     Contains public functions and prototypes for the UART transfer
  *            of HECTOR evaluation Mother board
  *
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MY_UART_H_
#define MY_UART_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Function prototypes definitions -------------------------------------------*/
void  my_uart_cfg		(void);
void  uart1_rx_handler	(mss_uart_instance_t * this_uart);
void  send_resp(uint32_t res_word,  uint8_t progress, uint8_t state,
                uint8_t gpio_state, uint8_t response);
char* get_filename		(void);
void  send_fab_resp     (void);
void  send_cryp_resp    (void);

#endif /* MY_UART_H_ */
