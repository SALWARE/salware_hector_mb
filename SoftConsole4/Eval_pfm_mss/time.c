/**
  ******************************************************************************
  * @file      time.c
  * @author    Marek Laban, Marcel Kleja, MICRONIC a.s., LabHC
  * @version   v1.0
  * @note      Created in frame of HECTOR project (No. 644052).
  * @copyright MICRONIC a.s., LabHC
  * @date      16-May-2016
  * @revision  v1.0 created (ML)
  * @brief     Provide time measurement, delay functions and systick timer
  *            routines
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "time.h"
#include "hal/hal.h"

/* Private function prototypes -----------------------------------------------*/
void TimingDelay_Decrement(void);

/* Private variables ---------------------------------------------------------*/
static   volatile uint32_t TimingDelay;
static   volatile uint32_t time_cnt = 0;

/* Private definitions -------------------------------------------------------*/
/* parts of ela_t typedef */
#define TIM_SET  0
#define TIM_STOP 1

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  	Make a time delay.
  * @param[in]  nTime Time delay in ms (if systick is configured to make
  * 			interrupt to every ms)
  * @retval None
  */
void Delay(uint32_t nTime)
{ 
  TimingDelay = nTime;

  while(TimingDelay != 0);
}

/**
  * @brief  	Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{
  if (TimingDelay != 0x00)
  { 
    TimingDelay--;
  }
}

/**
  * @brief  SysTick timer interrupt handler
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{ 
  time_cnt++;

  /* delay function control */
  TimingDelay_Decrement();

}

/**
  * @brief  		Timer elapsed check function
  * @param  my_ela  Timer handler -> store timer configuration
  * @retval elapsed state -> TIM_ELA  - TIMer ELApsed
  *                       -> TIM_NELA - TIMer Not ELApsed
  */
uint8_t t_elapsed(ela_t my_ela){

  /*  event time   - actual time   > size of counted time */
  if((my_ela[TIM_STOP] - time_cnt) > my_ela[TIM_SET])  
    return TIM_ELA;
  else
    return TIM_NELA;  
}

/**
  * @brief  		Set timer
  * @param  my_ela  Timer handler -> store timer configuration
  * @param  tim_set Size of counted time
  * @retval elapsed state -> TIM_ELA  - TIMer ELApsed
  *                       -> TIM_NELA - TIMer Not ELApsed
  */
void set_elapsed(ela_t my_ela,uint32_t tim_set){
  /* size of counted time */
  my_ela[TIM_SET]  = tim_set;
  /* event time */
  my_ela[TIM_STOP] = time_cnt + tim_set;
}

/**
  * @brief  		  Copy parameters of ela_t timer variable
  * @param  from_ela  Input variable
  * @param  to_ela    Output variable
  * @retval None
  */
void copy_ela(ela_t from_ela,ela_t to_ela){
  to_ela[TIM_SET] = from_ela[TIM_SET];
  to_ela[TIM_STOP]= from_ela[TIM_STOP];
}

/**
  * @brief  Read actual time
  * @param  None
  * @retval actual time (value of time_cnt register)
  */
uint32_t get_act_time(void){
  return time_cnt;
};

  
