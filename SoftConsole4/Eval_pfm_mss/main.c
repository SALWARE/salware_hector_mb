/**
  ******************************************************************************
  * @file      main.c
  * @author    Marek Laban, Marcel Kleja, MICRONIC a.s., LabHC
  * @version   v1.0
  * @note      Created in frame of HECTOR project (No. 644052).
  * @copyright MICRONIC a.s., LabHC
  * @date      16-May-2016
  * @revision  v1.0 created (ML)
  * @brief     This file provides control of HECTOR evaluation motherboard.
  *            Functionalities:
  *             - Configure MSS (Microcontroller SubSystem)
  *             - Operate State machine
  *             - Receive and send packets from/to Host PC
  *             - Operate USB mass storage class device
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "hal/hal.h"
#include "drivers/mss_uart/mss_uart.h"
#include "drivers/mss_gpio/mss_gpio.h"
#include "lpddr/lpddr.h"
#include "ram_drive_app.h"
#include "FatFs/src/ff.h"
#include "drivers_config/sys_config/sys_config.h"
#include "my_uart.h"
#include "main.h"
#include "time.h"
#include "pkt_nfo.h"
#include "fab_nfo.h"
#include "mss_instr.h"

/* Private define ------------------------------------------------------------*/

/* Data acquisition start address */
#define DATA_AQ_START   RAM_CACHE_ADDR
/* Maximum data acquistion stream size */
#define DATA_AQ_MAX     RAM_CACHE_SIZE

/** @defgroup cmd_state definititions
  * @{
  */
#define NO_CMD    0                     /**< no command received */
#define CMD_RCVD  1                      /**< command received */

#define ONE_BYTE 0xFF                    /**< used to read only last byte */

/*Definitions makes from 32 bit state word 8 bit word */
#define INSTR_WORD(a)  (uint8_t) ( a          & 0xFF) /**< Make instruction word */

#define GPIO_WORD(a)   (uint8_t) ((a >> 8   ) & 0xFF) /**< Make GPIO values word */


/* Private functions ---------------------------------------------------------*/
void    my_system_init(void);
uint8_t acq_check     (void);
void    make_fab_reset(void);
void    make_gen_reset(void);
void    gpio_set      (uint8_t gpio_new_value);
void    gpio_config   (uint8_t gpio_set);
uint8_t gpio_state    (void);

/* Private variables ---------------------------------------------------------*/
static uint8_t  cmd_state = NO_CMD;  /**< command received state */
static uint32_t mss_cmd;             /**< MCU command */
static uint32_t fab_cmd;             /**< fabric command */
static uint32_t stream_size = 0x200; /**< size of transfered stream */
uint8_t op_stage;                    /**< Actual stage of operation in percent */
FIL fil;                             /**< File object */
static uint8_t gpio_mask;            /**< IO Mask type of pins 0 - 7 */
FATFS fs;                            /**< File system object (volume work area) */
static uint8_t cryp_rsp_flag = 0;    /**< Send response flag */
/**
  * @brief  Initialization and main state machine of mother evaluation board
  * @param  None
  * @retval None
  */
int main(void){
  uint8_t mb_state = MB_IDLE;    /**< Mother Board state */
  uint8_t op_state = OP_IDLE;    /**< Acquisition state */
  FIL my_fil;
  UINT br;
  FRESULT res;                   /**< Result of filesystem */

  /* System initialization */
  my_system_init();

  HW_set_32bit_reg(CMD_ADDR,CMD_SETDB2);
  make_gen_reset();

  /* Stay in State machine cycling */
  while(1){
    switch(mb_state){
    /* Idle state of Mother board */
    case MB_IDLE:

      /* When new command received - change state */
      if(cmd_state == CMD_RCVD){
        cmd_state = NO_CMD;
        mb_state = INSTR_WORD(mss_cmd);
        if(mb_state == MB_IDLE)
          send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_NOTHING_TODO);
      }

      /* Check acquisition */
      if(op_state == OP_BUSY){
        op_state = acq_check();
      }

      /* When FPGA operation finished - send response data */
      if(cryp_rsp_flag == 1){
        if((HW_get_32bit_reg(FABST_ADDR) ) == 0){  //& 0x02
          send_cryp_resp();
          cryp_rsp_flag = 0;
        }
      }


      break;

    /* Send FAB Command */
    case MB_FAB_CMD:
      HW_set_32bit_reg(CMD_ADDR,fab_cmd);
      send_resp(fab_cmd,op_stage, op_state, gpio_state(), MB_SUCCESS);
      mb_state = MB_IDLE;
      break;

    /* Make reset of fabric logic */
    case MAKE_FAB_RST:

      /* If MSS acquistion is in progress - FAB reset is prohibited */
      if(op_state == OP_BUSY){
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_OP_INPRG);
      }
      else{
        make_fab_reset();
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_SUCCESS);
      }
      mb_state = MB_IDLE;
      break;

    /* Make reset of generator */
    case MAKE_GEN_RST:
      make_gen_reset();
      send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_SUCCESS);
      mb_state = MB_IDLE;
      break;

    /* Collect generator data  */
    case GEN_DATA_COLLECT:
      /* size of stream must be multiple of 4 */
      stream_size = stream_size + (stream_size % 4);

      /* check size of required stream */
      if(stream_size > DATA_AQ_MAX){
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_AQ_BIG_STREAM);
        mb_state = MB_IDLE;
        break;
      }

      /* check state of data collecting */
      if((op_state != OP_BUSY) && (HW_get_32bit_reg(STATE_ADDR) == OP_IDLE_STATE))
        op_state = OP_BUSY;
      else{
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_OP_INPRG);
        mb_state = MB_IDLE;
        break;
      }

      /* reset stage of acquisition to zero */
      op_stage = 0;

      /* Set start address of stream */
      HW_set_32bit_reg(WSTART_ADDR,DATA_AQ_START);

      /* Set end address of stream */
      HW_set_32bit_reg(WSIZE_ADDR, stream_size);

      /* Start acquisition */
      HW_set_32bit_reg(CMD_ADDR,CMD_OP_ST);

      send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_SUCCESS);

      mb_state = MB_IDLE;
      break;
    case MB_LOAD_FILE:
      /* size of stream must be multiple of 4 */
      if(stream_size % 4 != 0 )
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_NOT_MULTIPLE);

      /* check size of required stream */
      if(stream_size > DATA_AQ_MAX){
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_AQ_BIG_STREAM);
        mb_state = MB_IDLE;
        break;
      }

      /* check state of FPGA */
      if(HW_get_32bit_reg(STATE_ADDR) == OP_BUSY_STATE){
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_OP_INPRG);
        mb_state = MB_IDLE;
        break;
      }

      /* Mount filesystem */
      f_mount(&fs, "" ,0);

      /* open file */
      if( f_open(&my_fil, (TCHAR *)get_filename(), FA_OPEN_EXISTING | FA_READ) != FR_OK ){
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_FSYS_ERR);
        mb_state = MB_IDLE;
        break;
      }

      /* read data from file */
      if( f_read(&my_fil, (uint8_t*) RAM_CACHE_ADDR,stream_size, &br) != FR_OK){
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_RD_FILE_ERR);
        mb_state = MB_IDLE;
        break;
      }

      /* check number of read bytes */
      if(br != stream_size ){
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_RD_FILE_ERR);
        mb_state = MB_IDLE;
        break;
      }

      /* close file */
      f_close(&my_fil);

      /* umount work area */
      res = f_mount(0, "", 0);

      /* Set start address of stream */
      HW_set_32bit_reg(RSTART_ADDR,DATA_AQ_START);

      /* Set end address of stream */
      HW_set_32bit_reg(RSIZE_ADDR, stream_size);

      /* send response and set idle state */
      send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_SUCCESS);
      mb_state = MB_IDLE;
      break;
    /* Mount USB drive */
    case MB_MOUNT_DISK:
      RAM_DRIVE_init();
      send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_SUCCESS);
      mb_state = MB_IDLE;
      break;

    /* Get mss operation state */
    case MB_GET_OP_STATE:
      send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_SUCCESS);
      mb_state = MB_IDLE;
      break;

    /* Format drive */
    case MB_MAKE_FSYS:
      /* registers file system object to the FatFs module */
      res = f_mount(&fs, "" ,0);
      if(res != FR_OK){
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_FSYS_ERR);
        mb_state = MB_IDLE;
        break;
      }

      /* make filesystem */
      res = f_mkfs("", 0, 1);
      if(res != FR_OK){
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_FSYS_ERR);
        mb_state = MB_IDLE;
        break;
      }

      /* Unregister work area */
      res = f_mount(0, "", 0);
      if(res != FR_OK){
        send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_FSYS_ERR);
        mb_state = MB_IDLE;
        break;
      }

      send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_SUCCESS);
      mb_state = MB_IDLE;
      break;

    case MB_OP_RST:
      /* make fabric reset */
      make_fab_reset();

      /* reset operation state progress to zero */
      op_stage = 0;
      op_state = OP_IDLE;
      mb_state = MB_IDLE;
      send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_SUCCESS);
      break;
    case MB_GPIO_CGF:

      gpio_config(GPIO_WORD(mss_cmd));

      send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_SUCCESS);
      mb_state = MB_IDLE;
      break;

    case MB_GPIO_SET:

      gpio_set(GPIO_WORD(mss_cmd));

      send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_SUCCESS);
      mb_state = MB_IDLE;
      break;
    case SEND_FAB_RSP:
      send_fab_resp();
      mb_state = MB_IDLE;
      break;
    case SEND_CRYP_RSP:
      /* Wait while Check delay (cryp_wait) signal */
      while((HW_get_32bit_reg(MSSIO_IN_ADDR) & 0x01) == 1){};
      send_cryp_resp();
      mb_state = MB_IDLE;
      break;
    default:
      send_resp(MB_NOTHING,op_stage, op_state, gpio_state(), MB_CMD_NFND);
      mb_state = MB_IDLE;
      break;
    }

  }
}

/**
  * @brief  Initialization of evaluation board system
  * @param  None
  * @retval None
  */
void my_system_init(void){

  /* set systick to every 1 ms */
  SysTick_Config(MSS_SYS_M3_CLK_FREQ/1000);
  NVIC_EnableIRQ(SysTick_IRQn);

  MSS_GPIO_init();

    /* configure reset of fabric logic */
  MSS_GPIO_config(MSS_GPIO_8, MSS_GPIO_OUTPUT_MODE);
  MSS_GPIO_set_output(MSS_GPIO_8, 0);  // enable reset

  /* configure reset of fabric generator logic */
  MSS_GPIO_config(MSS_GPIO_9, MSS_GPIO_OUTPUT_MODE);
  MSS_GPIO_set_output(MSS_GPIO_9, 0);   // enable reset

  /* configure reset of USB physical layer */
  MSS_GPIO_config(MSS_GPIO_29, MSS_GPIO_OUTPUT_MODE);
  MSS_GPIO_set_output(MSS_GPIO_29, 1);   // enable reset

  /* set all pins 0 - 7 as inputs*/
  gpio_config(0x00);

  /* make reset of FPGA integrated logic */
  make_fab_reset();
  make_gen_reset();

  /* external RAM configuration */
  MEM_init();

   /* UART configuration */
  my_uart_cfg();
}


/**
  * @brief  Set new MSS state machine command
  * @param[in] new_cmd  new MSS command
  * @retval None
  */
void set_mss_cmd(uint32_t new_cmd){
  cmd_state = CMD_RCVD;
  mss_cmd = new_cmd;
}

/**
  * @brief  Set new word for FABRIC
  * @param[in] new_word  new FABRIC word
  * @retval None
  */
void set_fab_cmd(uint32_t new_word){
  fab_cmd = new_word;
}

/**
  * @brief  Make reset of control fabric logic
  * @param  None
  * @retval None
  */
void make_fab_reset(void){
  // not used
}

/**
  * @brief  Make generator reset
  * @param  None
  * @retval None
  */
void make_gen_reset(void){
  uint32_t tmp_word;     /*< Temporary word     */
  tmp_word = HW_get_32bit_reg(MSSIO_OUT_ADDR);
  tmp_word = tmp_word & 0xFFFFFEFF;
  HW_set_32bit_reg(MSSIO_OUT_ADDR,tmp_word);
  Delay(1);
  tmp_word = tmp_word | 0x00000100;
  HW_set_32bit_reg(MSSIO_OUT_ADDR,tmp_word);
  cryp_rsp_flag = 0;

}

/**
  * @brief  Set size of acquisition stream size
  * @param  None
  * @retval None
  */
void set_stream_size(uint32_t stream_word){
  stream_size = stream_word;
}

/**
  * @brief  Check operation state and if it is successful finished,
  *         copy file from cache to file system
  * @param  None
  * @retval New acquisition state
  */
uint8_t acq_check(void){
  FRESULT res;       /**< Result of filesystem */
  uint32_t gen_state;  /**< Generator state */
  uint32_t op_stage_i;/**< Operation stage auxiliary */
  UINT bw;           /**< Bytes written */

  /* read state of generator and return state */
  gen_state = HW_get_32bit_reg(STATE_ADDR);
  switch(gen_state){

  /* When acquisition is in progress, calculate actual state of data collecting */
  case OP_BUSY_STATE:
    op_stage_i = HW_get_32bit_reg(STAGE_ADDR);
    op_stage = (uint8_t)(((float)(op_stage_i - DATA_AQ_START) * (float) 1.0 / ((float)stream_size))*100.0);
    return(OP_BUSY);

  /* When buffer overflow */
  case OP_OFLW_STATE:
    return(OP_OVFLW);

  /* When acquisition is finished */
  case OP_IDLE_STATE:
    /*set stage to end of the operation */
    op_stage = 100;

    res = f_mount(&fs, "" ,0);
    if(res != FR_OK){
      return(OP_WR_ERR);
    }

    res = f_open(&fil, get_filename(), FA_CREATE_NEW | FA_WRITE);
    if(res != FR_OK){
      f_close(&fil);
      return(OP_WR_ERR);
    }

    /* Copy cache to a file */
    res = f_write(&fil, (uint32_t *)(DATA_AQ_START),stream_size, &bw);
    if(res != FR_OK || bw != stream_size){
      f_close(&fil);
      return(OP_WR_ERR);
    }

    /* Close the file */
    f_close(&fil);

    /* Unregister work area */
    res = f_mount(0, "", 0);
    if(res != FR_OK){
      return(OP_WR_ERR);
    }
    return(OP_IDLE);

  default: return(OP_UNKNW);
  }


}


/**
  * @brief  Set values to 8 GPIOs  number 0 - 7.
  * @param  gpio_new_value    New value for gpio pins, where MSB bit means 7. pin
  *                           and LSB bit means 0. pin
  * @retval None
  */
void gpio_set(uint8_t gpio_new_value){
  uint32_t gpio_values;

  /* Set new 8bits value of 32 bit word */
  gpio_values = ((uint32_t) gpio_new_value) & 0xFF;

  /* Get new 32 bit of actual gpio pins and set new value to word*/
  gpio_values = (HW_get_32bit_reg(MSSIO_OUT_ADDR) & 0xFFFFFF00) | gpio_values;

  /* Set new 8 bit word to 0 - 7 pins */
  HW_set_32bit_reg(MSSIO_OUT_ADDR,gpio_values);
}

/**
  * @brief  Return values of used GPIOs (8 - 15.)
  * @param  gpio_new_value    New value for gpio pins, where MSB bit means 15. pin
  *                           and LSB bit means 8. pin
  * @retval 8 bit of GPIOs value, where MSB bit is pin 15. and LSB bit is bit 8.
  */
uint8_t gpio_state(void){
  uint8_t gpio_in;
  uint8_t gpio_out;

  /* read gpio input value */
  gpio_in  = (uint8_t) (((HW_get_32bit_reg(MSSIO_IN_ADDR)) & (uint32_t)~gpio_mask) & 0xFF);

  /* read gpio output value */
  gpio_out = (uint8_t) ((HW_get_32bit_reg(MSSIO_OUT_ADDR)) & (uint32_t) gpio_mask);

  /* read 7. - 0. gpio pins */
  return(gpio_in | gpio_out);
}

/**
  * @brief  Configure used GPIOs (0 - 7.)
  * @param  gpio_set    New configuration for gpio pin, where MSB bit means 15. pin
  *                     and LSB bit means 8. pin.
  *                       - GPIO_CFG_OUT (non-zero bit) means output
  *                       - GPIO_CFG_IN  (zero bit) means input
  * @retval None
  */
void gpio_config(uint8_t gpio_set){
  /* remember configured mask of gpio */
  gpio_mask = gpio_set;

}

/* Set flag for the response of the puf or cryp instance */
void set_rsp_flag(void){
  cryp_rsp_flag = 1;
}

