
 File name:     readme.txt
 Project name:  HECTOR evaluation platform firmware
 Target:        HECTOR Motherboard SmartFusion 2 - MBSF2v11/MBSF2v12
 SW tools:      Libero v11.7.0.119 and SoftConsole v4.0.0.13

 Author:        Marek Laban, Marcel Kleja, MICRONIC a.s., LabHC

 Copyright:     MICRONIC a.s., LabHC

 Note:          Created in frame of HECTOR project (No. 644052).

 Revision:      Version 1.00, May 2016
 History:       16/05/2016 - v1.00 Created (ML, MIC)

 Description:   Contains predefined functions for communication and control
                of motherboard by HOST PC. It uses UART interface to receive
                and send packets from/to HOST PC and USB mass storage class
                driver to transfer data between motherboard and HOST PC.	
