/**
  ******************************************************************************
  * @file      time.h
  * @author    Marek Laban, Marcel Kleja, MICRONIC a.s., LabHC
  * @version   v1.0
  * @note      Created in frame of HECTOR project (No. 644052).
  * @copyright MICRONIC a.s., LabHC
  * @date      16-May-2016
  * @revision  v1.0 created (ML)
  * @brief     Systick timer and timeout are implemented here prototypes.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef TIME_H___
#define TIME_H___

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>


#define TIM_ELA 1   /**< timer elapsed */
#define TIM_NELA 0  /**< timer not elapsed */


typedef uint32_t ela_t[2]; /**< ela_t type consist from 2 parts
                                ela_t[TIM_SET]   - size of measured time
                                ela_t[TIM_STOP]  - time when timer elapsed */

/* Public  functions --------------------------------------------------------*/
void     Delay(uint32_t nTime);

uint8_t  t_elapsed  (ela_t my_ela);
void     set_elapsed(ela_t my_ela,uint32_t tim_set);

void     copy_ela  (ela_t from_ela,ela_t to_ela);
uint32_t get_act_time(void);
#endif
