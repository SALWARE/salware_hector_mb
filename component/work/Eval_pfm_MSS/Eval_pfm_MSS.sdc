set_component Eval_pfm_MSS
# Microsemi Corp.
# Date: 2017-Feb-15 15:33:17
#

create_clock -period 48.1928 [ get_pins { MSS_ADLIB_INST/CLK_CONFIG_APB } ]
