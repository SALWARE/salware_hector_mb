-- Version: v11.7 11.7.0.119

library ieee;
use ieee.std_logic_1164.all;
library smartfusion2;
use smartfusion2.all;

entity DB2MB_FIFO_DB2MB_FIFO_0_USRAM_top is

    port( A_DOUT        : out   std_logic_vector(31 downto 0);
          B_DOUT        : out   std_logic_vector(31 downto 0);
          C_DIN         : in    std_logic_vector(63 downto 0);
          A_ADDR        : in    std_logic_vector(4 downto 0);
          B_ADDR        : in    std_logic_vector(4 downto 0);
          C_ADDR        : in    std_logic_vector(3 downto 0);
          A_BLK         : in    std_logic;
          B_BLK         : in    std_logic;
          C_BLK         : in    std_logic;
          A_ADDR_ARST_N : in    std_logic;
          B_ADDR_ARST_N : in    std_logic;
          A_ADDR_SRST_N : in    std_logic;
          B_ADDR_SRST_N : in    std_logic;
          A_ADDR_EN     : in    std_logic;
          B_ADDR_EN     : in    std_logic;
          CLK           : in    std_logic;
          C_WEN         : in    std_logic
        );

end DB2MB_FIFO_DB2MB_FIFO_0_USRAM_top;

architecture DEF_ARCH of DB2MB_FIFO_DB2MB_FIFO_0_USRAM_top is 

  component RAM64x18
    generic (MEMORYFILE:string := "");

    port( A_DOUT        : out   std_logic_vector(17 downto 0);
          B_DOUT        : out   std_logic_vector(17 downto 0);
          BUSY          : out   std_logic;
          A_ADDR_CLK    : in    std_logic := 'U';
          A_DOUT_CLK    : in    std_logic := 'U';
          A_ADDR_SRST_N : in    std_logic := 'U';
          A_DOUT_SRST_N : in    std_logic := 'U';
          A_ADDR_ARST_N : in    std_logic := 'U';
          A_DOUT_ARST_N : in    std_logic := 'U';
          A_ADDR_EN     : in    std_logic := 'U';
          A_DOUT_EN     : in    std_logic := 'U';
          A_BLK         : in    std_logic_vector(1 downto 0) := (others => 'U');
          A_ADDR        : in    std_logic_vector(9 downto 0) := (others => 'U');
          B_ADDR_CLK    : in    std_logic := 'U';
          B_DOUT_CLK    : in    std_logic := 'U';
          B_ADDR_SRST_N : in    std_logic := 'U';
          B_DOUT_SRST_N : in    std_logic := 'U';
          B_ADDR_ARST_N : in    std_logic := 'U';
          B_DOUT_ARST_N : in    std_logic := 'U';
          B_ADDR_EN     : in    std_logic := 'U';
          B_DOUT_EN     : in    std_logic := 'U';
          B_BLK         : in    std_logic_vector(1 downto 0) := (others => 'U');
          B_ADDR        : in    std_logic_vector(9 downto 0) := (others => 'U');
          C_CLK         : in    std_logic := 'U';
          C_ADDR        : in    std_logic_vector(9 downto 0) := (others => 'U');
          C_DIN         : in    std_logic_vector(17 downto 0) := (others => 'U');
          C_WEN         : in    std_logic := 'U';
          C_BLK         : in    std_logic_vector(1 downto 0) := (others => 'U');
          A_EN          : in    std_logic := 'U';
          A_ADDR_LAT    : in    std_logic := 'U';
          A_DOUT_LAT    : in    std_logic := 'U';
          A_WIDTH       : in    std_logic_vector(2 downto 0) := (others => 'U');
          B_EN          : in    std_logic := 'U';
          B_ADDR_LAT    : in    std_logic := 'U';
          B_DOUT_LAT    : in    std_logic := 'U';
          B_WIDTH       : in    std_logic_vector(2 downto 0) := (others => 'U');
          C_EN          : in    std_logic := 'U';
          C_WIDTH       : in    std_logic_vector(2 downto 0) := (others => 'U');
          SII_LOCK      : in    std_logic := 'U'
        );
  end component;

  component GND
    port(Y : out std_logic); 
  end component;

  component VCC
    port(Y : out std_logic); 
  end component;

    signal \VCC\, \GND\, ADLIB_VCC : std_logic;
    signal GND_power_net1 : std_logic;
    signal VCC_power_net1 : std_logic;
    signal nc47, nc34, nc70, nc60, nc74, nc64, nc9, nc13, nc23, 
        nc55, nc80, nc33, nc16, nc26, nc45, nc73, nc58, nc63, 
        nc27, nc17, nc36, nc48, nc37, nc5, nc52, nc76, nc51, nc66, 
        nc77, nc67, nc4, nc42, nc41, nc59, nc25, nc15, nc35, nc49, 
        nc28, nc18, nc75, nc65, nc38, nc1, nc2, nc50, nc22, nc12, 
        nc21, nc11, nc78, nc54, nc68, nc3, nc32, nc40, nc31, nc44, 
        nc7, nc72, nc6, nc71, nc62, nc61, nc19, nc29, nc53, nc39, 
        nc8, nc79, nc43, nc69, nc56, nc20, nc10, nc57, nc24, nc14, 
        nc46, nc30 : std_logic;

begin 

    \GND\ <= GND_power_net1;
    \VCC\ <= VCC_power_net1;
    ADLIB_VCC <= VCC_power_net1;

    DB2MB_FIFO_DB2MB_FIFO_0_USRAM_top_R0C2 : RAM64x18
      port map(A_DOUT(17) => nc47, A_DOUT(16) => nc34, A_DOUT(15)
         => nc70, A_DOUT(14) => nc60, A_DOUT(13) => nc74, 
        A_DOUT(12) => nc64, A_DOUT(11) => nc9, A_DOUT(10) => nc13, 
        A_DOUT(9) => nc23, A_DOUT(8) => nc55, A_DOUT(7) => 
        A_DOUT(23), A_DOUT(6) => A_DOUT(22), A_DOUT(5) => 
        A_DOUT(21), A_DOUT(4) => A_DOUT(20), A_DOUT(3) => 
        A_DOUT(19), A_DOUT(2) => A_DOUT(18), A_DOUT(1) => 
        A_DOUT(17), A_DOUT(0) => A_DOUT(16), B_DOUT(17) => nc80, 
        B_DOUT(16) => nc33, B_DOUT(15) => nc16, B_DOUT(14) => 
        nc26, B_DOUT(13) => nc45, B_DOUT(12) => nc73, B_DOUT(11)
         => nc58, B_DOUT(10) => nc63, B_DOUT(9) => nc27, 
        B_DOUT(8) => nc17, B_DOUT(7) => B_DOUT(23), B_DOUT(6) => 
        B_DOUT(22), B_DOUT(5) => B_DOUT(21), B_DOUT(4) => 
        B_DOUT(20), B_DOUT(3) => B_DOUT(19), B_DOUT(2) => 
        B_DOUT(18), B_DOUT(1) => B_DOUT(17), B_DOUT(0) => 
        B_DOUT(16), BUSY => OPEN, A_ADDR_CLK => CLK, A_DOUT_CLK
         => \VCC\, A_ADDR_SRST_N => A_ADDR_SRST_N, A_DOUT_SRST_N
         => \VCC\, A_ADDR_ARST_N => A_ADDR_ARST_N, A_DOUT_ARST_N
         => \VCC\, A_ADDR_EN => A_ADDR_EN, A_DOUT_EN => \VCC\, 
        A_BLK(1) => A_BLK, A_BLK(0) => \VCC\, A_ADDR(9) => \GND\, 
        A_ADDR(8) => \GND\, A_ADDR(7) => A_ADDR(4), A_ADDR(6) => 
        A_ADDR(3), A_ADDR(5) => A_ADDR(2), A_ADDR(4) => A_ADDR(1), 
        A_ADDR(3) => A_ADDR(0), A_ADDR(2) => \GND\, A_ADDR(1) => 
        \GND\, A_ADDR(0) => \GND\, B_ADDR_CLK => CLK, B_DOUT_CLK
         => \VCC\, B_ADDR_SRST_N => B_ADDR_SRST_N, B_DOUT_SRST_N
         => \VCC\, B_ADDR_ARST_N => B_ADDR_ARST_N, B_DOUT_ARST_N
         => \VCC\, B_ADDR_EN => B_ADDR_EN, B_DOUT_EN => \VCC\, 
        B_BLK(1) => B_BLK, B_BLK(0) => \VCC\, B_ADDR(9) => \GND\, 
        B_ADDR(8) => \GND\, B_ADDR(7) => B_ADDR(4), B_ADDR(6) => 
        B_ADDR(3), B_ADDR(5) => B_ADDR(2), B_ADDR(4) => B_ADDR(1), 
        B_ADDR(3) => B_ADDR(0), B_ADDR(2) => \GND\, B_ADDR(1) => 
        \GND\, B_ADDR(0) => \GND\, C_CLK => CLK, C_ADDR(9) => 
        \GND\, C_ADDR(8) => \GND\, C_ADDR(7) => C_ADDR(3), 
        C_ADDR(6) => C_ADDR(2), C_ADDR(5) => C_ADDR(1), C_ADDR(4)
         => C_ADDR(0), C_ADDR(3) => \GND\, C_ADDR(2) => \GND\, 
        C_ADDR(1) => \GND\, C_ADDR(0) => \GND\, C_DIN(17) => 
        \GND\, C_DIN(16) => C_DIN(55), C_DIN(15) => C_DIN(54), 
        C_DIN(14) => C_DIN(53), C_DIN(13) => C_DIN(52), C_DIN(12)
         => C_DIN(51), C_DIN(11) => C_DIN(50), C_DIN(10) => 
        C_DIN(49), C_DIN(9) => C_DIN(48), C_DIN(8) => \GND\, 
        C_DIN(7) => C_DIN(23), C_DIN(6) => C_DIN(22), C_DIN(5)
         => C_DIN(21), C_DIN(4) => C_DIN(20), C_DIN(3) => 
        C_DIN(19), C_DIN(2) => C_DIN(18), C_DIN(1) => C_DIN(17), 
        C_DIN(0) => C_DIN(16), C_WEN => C_WEN, C_BLK(1) => C_BLK, 
        C_BLK(0) => \VCC\, A_EN => \VCC\, A_ADDR_LAT => \GND\, 
        A_DOUT_LAT => \VCC\, A_WIDTH(2) => \GND\, A_WIDTH(1) => 
        \VCC\, A_WIDTH(0) => \VCC\, B_EN => \VCC\, B_ADDR_LAT => 
        \GND\, B_DOUT_LAT => \VCC\, B_WIDTH(2) => \GND\, 
        B_WIDTH(1) => \VCC\, B_WIDTH(0) => \VCC\, C_EN => \VCC\, 
        C_WIDTH(2) => \VCC\, C_WIDTH(1) => \GND\, C_WIDTH(0) => 
        \GND\, SII_LOCK => \GND\);
    
    DB2MB_FIFO_DB2MB_FIFO_0_USRAM_top_R0C0 : RAM64x18
      port map(A_DOUT(17) => nc36, A_DOUT(16) => nc48, A_DOUT(15)
         => nc37, A_DOUT(14) => nc5, A_DOUT(13) => nc52, 
        A_DOUT(12) => nc76, A_DOUT(11) => nc51, A_DOUT(10) => 
        nc66, A_DOUT(9) => nc77, A_DOUT(8) => nc67, A_DOUT(7) => 
        A_DOUT(7), A_DOUT(6) => A_DOUT(6), A_DOUT(5) => A_DOUT(5), 
        A_DOUT(4) => A_DOUT(4), A_DOUT(3) => A_DOUT(3), A_DOUT(2)
         => A_DOUT(2), A_DOUT(1) => A_DOUT(1), A_DOUT(0) => 
        A_DOUT(0), B_DOUT(17) => nc4, B_DOUT(16) => nc42, 
        B_DOUT(15) => nc41, B_DOUT(14) => nc59, B_DOUT(13) => 
        nc25, B_DOUT(12) => nc15, B_DOUT(11) => nc35, B_DOUT(10)
         => nc49, B_DOUT(9) => nc28, B_DOUT(8) => nc18, B_DOUT(7)
         => B_DOUT(7), B_DOUT(6) => B_DOUT(6), B_DOUT(5) => 
        B_DOUT(5), B_DOUT(4) => B_DOUT(4), B_DOUT(3) => B_DOUT(3), 
        B_DOUT(2) => B_DOUT(2), B_DOUT(1) => B_DOUT(1), B_DOUT(0)
         => B_DOUT(0), BUSY => OPEN, A_ADDR_CLK => CLK, 
        A_DOUT_CLK => \VCC\, A_ADDR_SRST_N => A_ADDR_SRST_N, 
        A_DOUT_SRST_N => \VCC\, A_ADDR_ARST_N => A_ADDR_ARST_N, 
        A_DOUT_ARST_N => \VCC\, A_ADDR_EN => A_ADDR_EN, A_DOUT_EN
         => \VCC\, A_BLK(1) => A_BLK, A_BLK(0) => \VCC\, 
        A_ADDR(9) => \GND\, A_ADDR(8) => \GND\, A_ADDR(7) => 
        A_ADDR(4), A_ADDR(6) => A_ADDR(3), A_ADDR(5) => A_ADDR(2), 
        A_ADDR(4) => A_ADDR(1), A_ADDR(3) => A_ADDR(0), A_ADDR(2)
         => \GND\, A_ADDR(1) => \GND\, A_ADDR(0) => \GND\, 
        B_ADDR_CLK => CLK, B_DOUT_CLK => \VCC\, B_ADDR_SRST_N => 
        B_ADDR_SRST_N, B_DOUT_SRST_N => \VCC\, B_ADDR_ARST_N => 
        B_ADDR_ARST_N, B_DOUT_ARST_N => \VCC\, B_ADDR_EN => 
        B_ADDR_EN, B_DOUT_EN => \VCC\, B_BLK(1) => B_BLK, 
        B_BLK(0) => \VCC\, B_ADDR(9) => \GND\, B_ADDR(8) => \GND\, 
        B_ADDR(7) => B_ADDR(4), B_ADDR(6) => B_ADDR(3), B_ADDR(5)
         => B_ADDR(2), B_ADDR(4) => B_ADDR(1), B_ADDR(3) => 
        B_ADDR(0), B_ADDR(2) => \GND\, B_ADDR(1) => \GND\, 
        B_ADDR(0) => \GND\, C_CLK => CLK, C_ADDR(9) => \GND\, 
        C_ADDR(8) => \GND\, C_ADDR(7) => C_ADDR(3), C_ADDR(6) => 
        C_ADDR(2), C_ADDR(5) => C_ADDR(1), C_ADDR(4) => C_ADDR(0), 
        C_ADDR(3) => \GND\, C_ADDR(2) => \GND\, C_ADDR(1) => 
        \GND\, C_ADDR(0) => \GND\, C_DIN(17) => \GND\, C_DIN(16)
         => C_DIN(39), C_DIN(15) => C_DIN(38), C_DIN(14) => 
        C_DIN(37), C_DIN(13) => C_DIN(36), C_DIN(12) => C_DIN(35), 
        C_DIN(11) => C_DIN(34), C_DIN(10) => C_DIN(33), C_DIN(9)
         => C_DIN(32), C_DIN(8) => \GND\, C_DIN(7) => C_DIN(7), 
        C_DIN(6) => C_DIN(6), C_DIN(5) => C_DIN(5), C_DIN(4) => 
        C_DIN(4), C_DIN(3) => C_DIN(3), C_DIN(2) => C_DIN(2), 
        C_DIN(1) => C_DIN(1), C_DIN(0) => C_DIN(0), C_WEN => 
        C_WEN, C_BLK(1) => C_BLK, C_BLK(0) => \VCC\, A_EN => 
        \VCC\, A_ADDR_LAT => \GND\, A_DOUT_LAT => \VCC\, 
        A_WIDTH(2) => \GND\, A_WIDTH(1) => \VCC\, A_WIDTH(0) => 
        \VCC\, B_EN => \VCC\, B_ADDR_LAT => \GND\, B_DOUT_LAT => 
        \VCC\, B_WIDTH(2) => \GND\, B_WIDTH(1) => \VCC\, 
        B_WIDTH(0) => \VCC\, C_EN => \VCC\, C_WIDTH(2) => \VCC\, 
        C_WIDTH(1) => \GND\, C_WIDTH(0) => \GND\, SII_LOCK => 
        \GND\);
    
    DB2MB_FIFO_DB2MB_FIFO_0_USRAM_top_R0C1 : RAM64x18
      port map(A_DOUT(17) => nc75, A_DOUT(16) => nc65, A_DOUT(15)
         => nc38, A_DOUT(14) => nc1, A_DOUT(13) => nc2, 
        A_DOUT(12) => nc50, A_DOUT(11) => nc22, A_DOUT(10) => 
        nc12, A_DOUT(9) => nc21, A_DOUT(8) => nc11, A_DOUT(7) => 
        A_DOUT(15), A_DOUT(6) => A_DOUT(14), A_DOUT(5) => 
        A_DOUT(13), A_DOUT(4) => A_DOUT(12), A_DOUT(3) => 
        A_DOUT(11), A_DOUT(2) => A_DOUT(10), A_DOUT(1) => 
        A_DOUT(9), A_DOUT(0) => A_DOUT(8), B_DOUT(17) => nc78, 
        B_DOUT(16) => nc54, B_DOUT(15) => nc68, B_DOUT(14) => nc3, 
        B_DOUT(13) => nc32, B_DOUT(12) => nc40, B_DOUT(11) => 
        nc31, B_DOUT(10) => nc44, B_DOUT(9) => nc7, B_DOUT(8) => 
        nc72, B_DOUT(7) => B_DOUT(15), B_DOUT(6) => B_DOUT(14), 
        B_DOUT(5) => B_DOUT(13), B_DOUT(4) => B_DOUT(12), 
        B_DOUT(3) => B_DOUT(11), B_DOUT(2) => B_DOUT(10), 
        B_DOUT(1) => B_DOUT(9), B_DOUT(0) => B_DOUT(8), BUSY => 
        OPEN, A_ADDR_CLK => CLK, A_DOUT_CLK => \VCC\, 
        A_ADDR_SRST_N => A_ADDR_SRST_N, A_DOUT_SRST_N => \VCC\, 
        A_ADDR_ARST_N => A_ADDR_ARST_N, A_DOUT_ARST_N => \VCC\, 
        A_ADDR_EN => A_ADDR_EN, A_DOUT_EN => \VCC\, A_BLK(1) => 
        A_BLK, A_BLK(0) => \VCC\, A_ADDR(9) => \GND\, A_ADDR(8)
         => \GND\, A_ADDR(7) => A_ADDR(4), A_ADDR(6) => A_ADDR(3), 
        A_ADDR(5) => A_ADDR(2), A_ADDR(4) => A_ADDR(1), A_ADDR(3)
         => A_ADDR(0), A_ADDR(2) => \GND\, A_ADDR(1) => \GND\, 
        A_ADDR(0) => \GND\, B_ADDR_CLK => CLK, B_DOUT_CLK => 
        \VCC\, B_ADDR_SRST_N => B_ADDR_SRST_N, B_DOUT_SRST_N => 
        \VCC\, B_ADDR_ARST_N => B_ADDR_ARST_N, B_DOUT_ARST_N => 
        \VCC\, B_ADDR_EN => B_ADDR_EN, B_DOUT_EN => \VCC\, 
        B_BLK(1) => B_BLK, B_BLK(0) => \VCC\, B_ADDR(9) => \GND\, 
        B_ADDR(8) => \GND\, B_ADDR(7) => B_ADDR(4), B_ADDR(6) => 
        B_ADDR(3), B_ADDR(5) => B_ADDR(2), B_ADDR(4) => B_ADDR(1), 
        B_ADDR(3) => B_ADDR(0), B_ADDR(2) => \GND\, B_ADDR(1) => 
        \GND\, B_ADDR(0) => \GND\, C_CLK => CLK, C_ADDR(9) => 
        \GND\, C_ADDR(8) => \GND\, C_ADDR(7) => C_ADDR(3), 
        C_ADDR(6) => C_ADDR(2), C_ADDR(5) => C_ADDR(1), C_ADDR(4)
         => C_ADDR(0), C_ADDR(3) => \GND\, C_ADDR(2) => \GND\, 
        C_ADDR(1) => \GND\, C_ADDR(0) => \GND\, C_DIN(17) => 
        \GND\, C_DIN(16) => C_DIN(47), C_DIN(15) => C_DIN(46), 
        C_DIN(14) => C_DIN(45), C_DIN(13) => C_DIN(44), C_DIN(12)
         => C_DIN(43), C_DIN(11) => C_DIN(42), C_DIN(10) => 
        C_DIN(41), C_DIN(9) => C_DIN(40), C_DIN(8) => \GND\, 
        C_DIN(7) => C_DIN(15), C_DIN(6) => C_DIN(14), C_DIN(5)
         => C_DIN(13), C_DIN(4) => C_DIN(12), C_DIN(3) => 
        C_DIN(11), C_DIN(2) => C_DIN(10), C_DIN(1) => C_DIN(9), 
        C_DIN(0) => C_DIN(8), C_WEN => C_WEN, C_BLK(1) => C_BLK, 
        C_BLK(0) => \VCC\, A_EN => \VCC\, A_ADDR_LAT => \GND\, 
        A_DOUT_LAT => \VCC\, A_WIDTH(2) => \GND\, A_WIDTH(1) => 
        \VCC\, A_WIDTH(0) => \VCC\, B_EN => \VCC\, B_ADDR_LAT => 
        \GND\, B_DOUT_LAT => \VCC\, B_WIDTH(2) => \GND\, 
        B_WIDTH(1) => \VCC\, B_WIDTH(0) => \VCC\, C_EN => \VCC\, 
        C_WIDTH(2) => \VCC\, C_WIDTH(1) => \GND\, C_WIDTH(0) => 
        \GND\, SII_LOCK => \GND\);
    
    DB2MB_FIFO_DB2MB_FIFO_0_USRAM_top_R0C3 : RAM64x18
      port map(A_DOUT(17) => nc6, A_DOUT(16) => nc71, A_DOUT(15)
         => nc62, A_DOUT(14) => nc61, A_DOUT(13) => nc19, 
        A_DOUT(12) => nc29, A_DOUT(11) => nc53, A_DOUT(10) => 
        nc39, A_DOUT(9) => nc8, A_DOUT(8) => nc79, A_DOUT(7) => 
        A_DOUT(31), A_DOUT(6) => A_DOUT(30), A_DOUT(5) => 
        A_DOUT(29), A_DOUT(4) => A_DOUT(28), A_DOUT(3) => 
        A_DOUT(27), A_DOUT(2) => A_DOUT(26), A_DOUT(1) => 
        A_DOUT(25), A_DOUT(0) => A_DOUT(24), B_DOUT(17) => nc43, 
        B_DOUT(16) => nc69, B_DOUT(15) => nc56, B_DOUT(14) => 
        nc20, B_DOUT(13) => nc10, B_DOUT(12) => nc57, B_DOUT(11)
         => nc24, B_DOUT(10) => nc14, B_DOUT(9) => nc46, 
        B_DOUT(8) => nc30, B_DOUT(7) => B_DOUT(31), B_DOUT(6) => 
        B_DOUT(30), B_DOUT(5) => B_DOUT(29), B_DOUT(4) => 
        B_DOUT(28), B_DOUT(3) => B_DOUT(27), B_DOUT(2) => 
        B_DOUT(26), B_DOUT(1) => B_DOUT(25), B_DOUT(0) => 
        B_DOUT(24), BUSY => OPEN, A_ADDR_CLK => CLK, A_DOUT_CLK
         => \VCC\, A_ADDR_SRST_N => A_ADDR_SRST_N, A_DOUT_SRST_N
         => \VCC\, A_ADDR_ARST_N => A_ADDR_ARST_N, A_DOUT_ARST_N
         => \VCC\, A_ADDR_EN => A_ADDR_EN, A_DOUT_EN => \VCC\, 
        A_BLK(1) => A_BLK, A_BLK(0) => \VCC\, A_ADDR(9) => \GND\, 
        A_ADDR(8) => \GND\, A_ADDR(7) => A_ADDR(4), A_ADDR(6) => 
        A_ADDR(3), A_ADDR(5) => A_ADDR(2), A_ADDR(4) => A_ADDR(1), 
        A_ADDR(3) => A_ADDR(0), A_ADDR(2) => \GND\, A_ADDR(1) => 
        \GND\, A_ADDR(0) => \GND\, B_ADDR_CLK => CLK, B_DOUT_CLK
         => \VCC\, B_ADDR_SRST_N => B_ADDR_SRST_N, B_DOUT_SRST_N
         => \VCC\, B_ADDR_ARST_N => B_ADDR_ARST_N, B_DOUT_ARST_N
         => \VCC\, B_ADDR_EN => B_ADDR_EN, B_DOUT_EN => \VCC\, 
        B_BLK(1) => B_BLK, B_BLK(0) => \VCC\, B_ADDR(9) => \GND\, 
        B_ADDR(8) => \GND\, B_ADDR(7) => B_ADDR(4), B_ADDR(6) => 
        B_ADDR(3), B_ADDR(5) => B_ADDR(2), B_ADDR(4) => B_ADDR(1), 
        B_ADDR(3) => B_ADDR(0), B_ADDR(2) => \GND\, B_ADDR(1) => 
        \GND\, B_ADDR(0) => \GND\, C_CLK => CLK, C_ADDR(9) => 
        \GND\, C_ADDR(8) => \GND\, C_ADDR(7) => C_ADDR(3), 
        C_ADDR(6) => C_ADDR(2), C_ADDR(5) => C_ADDR(1), C_ADDR(4)
         => C_ADDR(0), C_ADDR(3) => \GND\, C_ADDR(2) => \GND\, 
        C_ADDR(1) => \GND\, C_ADDR(0) => \GND\, C_DIN(17) => 
        \GND\, C_DIN(16) => C_DIN(63), C_DIN(15) => C_DIN(62), 
        C_DIN(14) => C_DIN(61), C_DIN(13) => C_DIN(60), C_DIN(12)
         => C_DIN(59), C_DIN(11) => C_DIN(58), C_DIN(10) => 
        C_DIN(57), C_DIN(9) => C_DIN(56), C_DIN(8) => \GND\, 
        C_DIN(7) => C_DIN(31), C_DIN(6) => C_DIN(30), C_DIN(5)
         => C_DIN(29), C_DIN(4) => C_DIN(28), C_DIN(3) => 
        C_DIN(27), C_DIN(2) => C_DIN(26), C_DIN(1) => C_DIN(25), 
        C_DIN(0) => C_DIN(24), C_WEN => C_WEN, C_BLK(1) => C_BLK, 
        C_BLK(0) => \VCC\, A_EN => \VCC\, A_ADDR_LAT => \GND\, 
        A_DOUT_LAT => \VCC\, A_WIDTH(2) => \GND\, A_WIDTH(1) => 
        \VCC\, A_WIDTH(0) => \VCC\, B_EN => \VCC\, B_ADDR_LAT => 
        \GND\, B_DOUT_LAT => \VCC\, B_WIDTH(2) => \GND\, 
        B_WIDTH(1) => \VCC\, B_WIDTH(0) => \VCC\, C_EN => \VCC\, 
        C_WIDTH(2) => \VCC\, C_WIDTH(1) => \GND\, C_WIDTH(0) => 
        \GND\, SII_LOCK => \GND\);
    
    GND_power_inst1 : GND
      port map( Y => GND_power_net1);

    VCC_power_inst1 : VCC
      port map( Y => VCC_power_net1);


end DEF_ARCH; 
