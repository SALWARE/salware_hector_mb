-- Version: v11.7 11.7.0.119

library ieee;
use ieee.std_logic_1164.all;
library smartfusion2;
use smartfusion2.all;

entity CRYP_FIFO_CRYP_FIFO_0_USRAM_top is

    port( A_DOUT        : out   std_logic_vector(63 downto 0);
          B_DOUT        : out   std_logic_vector(63 downto 0);
          C_DIN         : in    std_logic_vector(31 downto 0);
          A_ADDR        : in    std_logic_vector(4 downto 0);
          B_ADDR        : in    std_logic_vector(4 downto 0);
          C_ADDR        : in    std_logic_vector(5 downto 0);
          A_BLK         : in    std_logic;
          B_BLK         : in    std_logic;
          C_BLK         : in    std_logic;
          A_ADDR_ARST_N : in    std_logic;
          B_ADDR_ARST_N : in    std_logic;
          A_ADDR_SRST_N : in    std_logic;
          B_ADDR_SRST_N : in    std_logic;
          A_ADDR_EN     : in    std_logic;
          B_ADDR_EN     : in    std_logic;
          CLK           : in    std_logic;
          C_WEN         : in    std_logic
        );

end CRYP_FIFO_CRYP_FIFO_0_USRAM_top;

architecture DEF_ARCH of CRYP_FIFO_CRYP_FIFO_0_USRAM_top is 

  component RAM64x18
    generic (MEMORYFILE:string := "");

    port( A_DOUT        : out   std_logic_vector(17 downto 0);
          B_DOUT        : out   std_logic_vector(17 downto 0);
          BUSY          : out   std_logic;
          A_ADDR_CLK    : in    std_logic := 'U';
          A_DOUT_CLK    : in    std_logic := 'U';
          A_ADDR_SRST_N : in    std_logic := 'U';
          A_DOUT_SRST_N : in    std_logic := 'U';
          A_ADDR_ARST_N : in    std_logic := 'U';
          A_DOUT_ARST_N : in    std_logic := 'U';
          A_ADDR_EN     : in    std_logic := 'U';
          A_DOUT_EN     : in    std_logic := 'U';
          A_BLK         : in    std_logic_vector(1 downto 0) := (others => 'U');
          A_ADDR        : in    std_logic_vector(9 downto 0) := (others => 'U');
          B_ADDR_CLK    : in    std_logic := 'U';
          B_DOUT_CLK    : in    std_logic := 'U';
          B_ADDR_SRST_N : in    std_logic := 'U';
          B_DOUT_SRST_N : in    std_logic := 'U';
          B_ADDR_ARST_N : in    std_logic := 'U';
          B_DOUT_ARST_N : in    std_logic := 'U';
          B_ADDR_EN     : in    std_logic := 'U';
          B_DOUT_EN     : in    std_logic := 'U';
          B_BLK         : in    std_logic_vector(1 downto 0) := (others => 'U');
          B_ADDR        : in    std_logic_vector(9 downto 0) := (others => 'U');
          C_CLK         : in    std_logic := 'U';
          C_ADDR        : in    std_logic_vector(9 downto 0) := (others => 'U');
          C_DIN         : in    std_logic_vector(17 downto 0) := (others => 'U');
          C_WEN         : in    std_logic := 'U';
          C_BLK         : in    std_logic_vector(1 downto 0) := (others => 'U');
          A_EN          : in    std_logic := 'U';
          A_ADDR_LAT    : in    std_logic := 'U';
          A_DOUT_LAT    : in    std_logic := 'U';
          A_WIDTH       : in    std_logic_vector(2 downto 0) := (others => 'U');
          B_EN          : in    std_logic := 'U';
          B_ADDR_LAT    : in    std_logic := 'U';
          B_DOUT_LAT    : in    std_logic := 'U';
          B_WIDTH       : in    std_logic_vector(2 downto 0) := (others => 'U');
          C_EN          : in    std_logic := 'U';
          C_WIDTH       : in    std_logic_vector(2 downto 0) := (others => 'U');
          SII_LOCK      : in    std_logic := 'U'
        );
  end component;

  component GND
    port(Y : out std_logic); 
  end component;

  component VCC
    port(Y : out std_logic); 
  end component;

    signal \VCC\, \GND\, ADLIB_VCC : std_logic;
    signal GND_power_net1 : std_logic;
    signal VCC_power_net1 : std_logic;
    signal nc7, nc6, nc12, nc5, nc15, nc1, nc16, nc14, nc9, nc13, 
        nc8, nc4, nc11, nc3, nc10, nc2 : std_logic;

begin 

    \GND\ <= GND_power_net1;
    \VCC\ <= VCC_power_net1;
    ADLIB_VCC <= VCC_power_net1;

    CRYP_FIFO_CRYP_FIFO_0_USRAM_top_R0C2 : RAM64x18
      port map(A_DOUT(17) => nc7, A_DOUT(16) => A_DOUT(55), 
        A_DOUT(15) => A_DOUT(54), A_DOUT(14) => A_DOUT(53), 
        A_DOUT(13) => A_DOUT(52), A_DOUT(12) => A_DOUT(51), 
        A_DOUT(11) => A_DOUT(50), A_DOUT(10) => A_DOUT(49), 
        A_DOUT(9) => A_DOUT(48), A_DOUT(8) => nc6, A_DOUT(7) => 
        A_DOUT(23), A_DOUT(6) => A_DOUT(22), A_DOUT(5) => 
        A_DOUT(21), A_DOUT(4) => A_DOUT(20), A_DOUT(3) => 
        A_DOUT(19), A_DOUT(2) => A_DOUT(18), A_DOUT(1) => 
        A_DOUT(17), A_DOUT(0) => A_DOUT(16), B_DOUT(17) => nc12, 
        B_DOUT(16) => B_DOUT(55), B_DOUT(15) => B_DOUT(54), 
        B_DOUT(14) => B_DOUT(53), B_DOUT(13) => B_DOUT(52), 
        B_DOUT(12) => B_DOUT(51), B_DOUT(11) => B_DOUT(50), 
        B_DOUT(10) => B_DOUT(49), B_DOUT(9) => B_DOUT(48), 
        B_DOUT(8) => nc5, B_DOUT(7) => B_DOUT(23), B_DOUT(6) => 
        B_DOUT(22), B_DOUT(5) => B_DOUT(21), B_DOUT(4) => 
        B_DOUT(20), B_DOUT(3) => B_DOUT(19), B_DOUT(2) => 
        B_DOUT(18), B_DOUT(1) => B_DOUT(17), B_DOUT(0) => 
        B_DOUT(16), BUSY => OPEN, A_ADDR_CLK => CLK, A_DOUT_CLK
         => \VCC\, A_ADDR_SRST_N => A_ADDR_SRST_N, A_DOUT_SRST_N
         => \VCC\, A_ADDR_ARST_N => A_ADDR_ARST_N, A_DOUT_ARST_N
         => \VCC\, A_ADDR_EN => A_ADDR_EN, A_DOUT_EN => \VCC\, 
        A_BLK(1) => A_BLK, A_BLK(0) => \VCC\, A_ADDR(9) => \GND\, 
        A_ADDR(8) => A_ADDR(4), A_ADDR(7) => A_ADDR(3), A_ADDR(6)
         => A_ADDR(2), A_ADDR(5) => A_ADDR(1), A_ADDR(4) => 
        A_ADDR(0), A_ADDR(3) => \GND\, A_ADDR(2) => \GND\, 
        A_ADDR(1) => \GND\, A_ADDR(0) => \GND\, B_ADDR_CLK => CLK, 
        B_DOUT_CLK => \VCC\, B_ADDR_SRST_N => B_ADDR_SRST_N, 
        B_DOUT_SRST_N => \VCC\, B_ADDR_ARST_N => B_ADDR_ARST_N, 
        B_DOUT_ARST_N => \VCC\, B_ADDR_EN => B_ADDR_EN, B_DOUT_EN
         => \VCC\, B_BLK(1) => B_BLK, B_BLK(0) => \VCC\, 
        B_ADDR(9) => \GND\, B_ADDR(8) => B_ADDR(4), B_ADDR(7) => 
        B_ADDR(3), B_ADDR(6) => B_ADDR(2), B_ADDR(5) => B_ADDR(1), 
        B_ADDR(4) => B_ADDR(0), B_ADDR(3) => \GND\, B_ADDR(2) => 
        \GND\, B_ADDR(1) => \GND\, B_ADDR(0) => \GND\, C_CLK => 
        CLK, C_ADDR(9) => \GND\, C_ADDR(8) => C_ADDR(5), 
        C_ADDR(7) => C_ADDR(4), C_ADDR(6) => C_ADDR(3), C_ADDR(5)
         => C_ADDR(2), C_ADDR(4) => C_ADDR(1), C_ADDR(3) => 
        C_ADDR(0), C_ADDR(2) => \GND\, C_ADDR(1) => \GND\, 
        C_ADDR(0) => \GND\, C_DIN(17) => \GND\, C_DIN(16) => 
        \GND\, C_DIN(15) => \GND\, C_DIN(14) => \GND\, C_DIN(13)
         => \GND\, C_DIN(12) => \GND\, C_DIN(11) => \GND\, 
        C_DIN(10) => \GND\, C_DIN(9) => \GND\, C_DIN(8) => \GND\, 
        C_DIN(7) => C_DIN(23), C_DIN(6) => C_DIN(22), C_DIN(5)
         => C_DIN(21), C_DIN(4) => C_DIN(20), C_DIN(3) => 
        C_DIN(19), C_DIN(2) => C_DIN(18), C_DIN(1) => C_DIN(17), 
        C_DIN(0) => C_DIN(16), C_WEN => C_WEN, C_BLK(1) => C_BLK, 
        C_BLK(0) => \VCC\, A_EN => \VCC\, A_ADDR_LAT => \GND\, 
        A_DOUT_LAT => \VCC\, A_WIDTH(2) => \VCC\, A_WIDTH(1) => 
        \GND\, A_WIDTH(0) => \GND\, B_EN => \VCC\, B_ADDR_LAT => 
        \GND\, B_DOUT_LAT => \VCC\, B_WIDTH(2) => \VCC\, 
        B_WIDTH(1) => \GND\, B_WIDTH(0) => \GND\, C_EN => \VCC\, 
        C_WIDTH(2) => \GND\, C_WIDTH(1) => \VCC\, C_WIDTH(0) => 
        \VCC\, SII_LOCK => \GND\);
    
    CRYP_FIFO_CRYP_FIFO_0_USRAM_top_R0C1 : RAM64x18
      port map(A_DOUT(17) => nc15, A_DOUT(16) => A_DOUT(47), 
        A_DOUT(15) => A_DOUT(46), A_DOUT(14) => A_DOUT(45), 
        A_DOUT(13) => A_DOUT(44), A_DOUT(12) => A_DOUT(43), 
        A_DOUT(11) => A_DOUT(42), A_DOUT(10) => A_DOUT(41), 
        A_DOUT(9) => A_DOUT(40), A_DOUT(8) => nc1, A_DOUT(7) => 
        A_DOUT(15), A_DOUT(6) => A_DOUT(14), A_DOUT(5) => 
        A_DOUT(13), A_DOUT(4) => A_DOUT(12), A_DOUT(3) => 
        A_DOUT(11), A_DOUT(2) => A_DOUT(10), A_DOUT(1) => 
        A_DOUT(9), A_DOUT(0) => A_DOUT(8), B_DOUT(17) => nc16, 
        B_DOUT(16) => B_DOUT(47), B_DOUT(15) => B_DOUT(46), 
        B_DOUT(14) => B_DOUT(45), B_DOUT(13) => B_DOUT(44), 
        B_DOUT(12) => B_DOUT(43), B_DOUT(11) => B_DOUT(42), 
        B_DOUT(10) => B_DOUT(41), B_DOUT(9) => B_DOUT(40), 
        B_DOUT(8) => nc14, B_DOUT(7) => B_DOUT(15), B_DOUT(6) => 
        B_DOUT(14), B_DOUT(5) => B_DOUT(13), B_DOUT(4) => 
        B_DOUT(12), B_DOUT(3) => B_DOUT(11), B_DOUT(2) => 
        B_DOUT(10), B_DOUT(1) => B_DOUT(9), B_DOUT(0) => 
        B_DOUT(8), BUSY => OPEN, A_ADDR_CLK => CLK, A_DOUT_CLK
         => \VCC\, A_ADDR_SRST_N => A_ADDR_SRST_N, A_DOUT_SRST_N
         => \VCC\, A_ADDR_ARST_N => A_ADDR_ARST_N, A_DOUT_ARST_N
         => \VCC\, A_ADDR_EN => A_ADDR_EN, A_DOUT_EN => \VCC\, 
        A_BLK(1) => A_BLK, A_BLK(0) => \VCC\, A_ADDR(9) => \GND\, 
        A_ADDR(8) => A_ADDR(4), A_ADDR(7) => A_ADDR(3), A_ADDR(6)
         => A_ADDR(2), A_ADDR(5) => A_ADDR(1), A_ADDR(4) => 
        A_ADDR(0), A_ADDR(3) => \GND\, A_ADDR(2) => \GND\, 
        A_ADDR(1) => \GND\, A_ADDR(0) => \GND\, B_ADDR_CLK => CLK, 
        B_DOUT_CLK => \VCC\, B_ADDR_SRST_N => B_ADDR_SRST_N, 
        B_DOUT_SRST_N => \VCC\, B_ADDR_ARST_N => B_ADDR_ARST_N, 
        B_DOUT_ARST_N => \VCC\, B_ADDR_EN => B_ADDR_EN, B_DOUT_EN
         => \VCC\, B_BLK(1) => B_BLK, B_BLK(0) => \VCC\, 
        B_ADDR(9) => \GND\, B_ADDR(8) => B_ADDR(4), B_ADDR(7) => 
        B_ADDR(3), B_ADDR(6) => B_ADDR(2), B_ADDR(5) => B_ADDR(1), 
        B_ADDR(4) => B_ADDR(0), B_ADDR(3) => \GND\, B_ADDR(2) => 
        \GND\, B_ADDR(1) => \GND\, B_ADDR(0) => \GND\, C_CLK => 
        CLK, C_ADDR(9) => \GND\, C_ADDR(8) => C_ADDR(5), 
        C_ADDR(7) => C_ADDR(4), C_ADDR(6) => C_ADDR(3), C_ADDR(5)
         => C_ADDR(2), C_ADDR(4) => C_ADDR(1), C_ADDR(3) => 
        C_ADDR(0), C_ADDR(2) => \GND\, C_ADDR(1) => \GND\, 
        C_ADDR(0) => \GND\, C_DIN(17) => \GND\, C_DIN(16) => 
        \GND\, C_DIN(15) => \GND\, C_DIN(14) => \GND\, C_DIN(13)
         => \GND\, C_DIN(12) => \GND\, C_DIN(11) => \GND\, 
        C_DIN(10) => \GND\, C_DIN(9) => \GND\, C_DIN(8) => \GND\, 
        C_DIN(7) => C_DIN(15), C_DIN(6) => C_DIN(14), C_DIN(5)
         => C_DIN(13), C_DIN(4) => C_DIN(12), C_DIN(3) => 
        C_DIN(11), C_DIN(2) => C_DIN(10), C_DIN(1) => C_DIN(9), 
        C_DIN(0) => C_DIN(8), C_WEN => C_WEN, C_BLK(1) => C_BLK, 
        C_BLK(0) => \VCC\, A_EN => \VCC\, A_ADDR_LAT => \GND\, 
        A_DOUT_LAT => \VCC\, A_WIDTH(2) => \VCC\, A_WIDTH(1) => 
        \GND\, A_WIDTH(0) => \GND\, B_EN => \VCC\, B_ADDR_LAT => 
        \GND\, B_DOUT_LAT => \VCC\, B_WIDTH(2) => \VCC\, 
        B_WIDTH(1) => \GND\, B_WIDTH(0) => \GND\, C_EN => \VCC\, 
        C_WIDTH(2) => \GND\, C_WIDTH(1) => \VCC\, C_WIDTH(0) => 
        \VCC\, SII_LOCK => \GND\);
    
    CRYP_FIFO_CRYP_FIFO_0_USRAM_top_R0C0 : RAM64x18
      port map(A_DOUT(17) => nc9, A_DOUT(16) => A_DOUT(39), 
        A_DOUT(15) => A_DOUT(38), A_DOUT(14) => A_DOUT(37), 
        A_DOUT(13) => A_DOUT(36), A_DOUT(12) => A_DOUT(35), 
        A_DOUT(11) => A_DOUT(34), A_DOUT(10) => A_DOUT(33), 
        A_DOUT(9) => A_DOUT(32), A_DOUT(8) => nc13, A_DOUT(7) => 
        A_DOUT(7), A_DOUT(6) => A_DOUT(6), A_DOUT(5) => A_DOUT(5), 
        A_DOUT(4) => A_DOUT(4), A_DOUT(3) => A_DOUT(3), A_DOUT(2)
         => A_DOUT(2), A_DOUT(1) => A_DOUT(1), A_DOUT(0) => 
        A_DOUT(0), B_DOUT(17) => nc8, B_DOUT(16) => B_DOUT(39), 
        B_DOUT(15) => B_DOUT(38), B_DOUT(14) => B_DOUT(37), 
        B_DOUT(13) => B_DOUT(36), B_DOUT(12) => B_DOUT(35), 
        B_DOUT(11) => B_DOUT(34), B_DOUT(10) => B_DOUT(33), 
        B_DOUT(9) => B_DOUT(32), B_DOUT(8) => nc4, B_DOUT(7) => 
        B_DOUT(7), B_DOUT(6) => B_DOUT(6), B_DOUT(5) => B_DOUT(5), 
        B_DOUT(4) => B_DOUT(4), B_DOUT(3) => B_DOUT(3), B_DOUT(2)
         => B_DOUT(2), B_DOUT(1) => B_DOUT(1), B_DOUT(0) => 
        B_DOUT(0), BUSY => OPEN, A_ADDR_CLK => CLK, A_DOUT_CLK
         => \VCC\, A_ADDR_SRST_N => A_ADDR_SRST_N, A_DOUT_SRST_N
         => \VCC\, A_ADDR_ARST_N => A_ADDR_ARST_N, A_DOUT_ARST_N
         => \VCC\, A_ADDR_EN => A_ADDR_EN, A_DOUT_EN => \VCC\, 
        A_BLK(1) => A_BLK, A_BLK(0) => \VCC\, A_ADDR(9) => \GND\, 
        A_ADDR(8) => A_ADDR(4), A_ADDR(7) => A_ADDR(3), A_ADDR(6)
         => A_ADDR(2), A_ADDR(5) => A_ADDR(1), A_ADDR(4) => 
        A_ADDR(0), A_ADDR(3) => \GND\, A_ADDR(2) => \GND\, 
        A_ADDR(1) => \GND\, A_ADDR(0) => \GND\, B_ADDR_CLK => CLK, 
        B_DOUT_CLK => \VCC\, B_ADDR_SRST_N => B_ADDR_SRST_N, 
        B_DOUT_SRST_N => \VCC\, B_ADDR_ARST_N => B_ADDR_ARST_N, 
        B_DOUT_ARST_N => \VCC\, B_ADDR_EN => B_ADDR_EN, B_DOUT_EN
         => \VCC\, B_BLK(1) => B_BLK, B_BLK(0) => \VCC\, 
        B_ADDR(9) => \GND\, B_ADDR(8) => B_ADDR(4), B_ADDR(7) => 
        B_ADDR(3), B_ADDR(6) => B_ADDR(2), B_ADDR(5) => B_ADDR(1), 
        B_ADDR(4) => B_ADDR(0), B_ADDR(3) => \GND\, B_ADDR(2) => 
        \GND\, B_ADDR(1) => \GND\, B_ADDR(0) => \GND\, C_CLK => 
        CLK, C_ADDR(9) => \GND\, C_ADDR(8) => C_ADDR(5), 
        C_ADDR(7) => C_ADDR(4), C_ADDR(6) => C_ADDR(3), C_ADDR(5)
         => C_ADDR(2), C_ADDR(4) => C_ADDR(1), C_ADDR(3) => 
        C_ADDR(0), C_ADDR(2) => \GND\, C_ADDR(1) => \GND\, 
        C_ADDR(0) => \GND\, C_DIN(17) => \GND\, C_DIN(16) => 
        \GND\, C_DIN(15) => \GND\, C_DIN(14) => \GND\, C_DIN(13)
         => \GND\, C_DIN(12) => \GND\, C_DIN(11) => \GND\, 
        C_DIN(10) => \GND\, C_DIN(9) => \GND\, C_DIN(8) => \GND\, 
        C_DIN(7) => C_DIN(7), C_DIN(6) => C_DIN(6), C_DIN(5) => 
        C_DIN(5), C_DIN(4) => C_DIN(4), C_DIN(3) => C_DIN(3), 
        C_DIN(2) => C_DIN(2), C_DIN(1) => C_DIN(1), C_DIN(0) => 
        C_DIN(0), C_WEN => C_WEN, C_BLK(1) => C_BLK, C_BLK(0) => 
        \VCC\, A_EN => \VCC\, A_ADDR_LAT => \GND\, A_DOUT_LAT => 
        \VCC\, A_WIDTH(2) => \VCC\, A_WIDTH(1) => \GND\, 
        A_WIDTH(0) => \GND\, B_EN => \VCC\, B_ADDR_LAT => \GND\, 
        B_DOUT_LAT => \VCC\, B_WIDTH(2) => \VCC\, B_WIDTH(1) => 
        \GND\, B_WIDTH(0) => \GND\, C_EN => \VCC\, C_WIDTH(2) => 
        \GND\, C_WIDTH(1) => \VCC\, C_WIDTH(0) => \VCC\, SII_LOCK
         => \GND\);
    
    CRYP_FIFO_CRYP_FIFO_0_USRAM_top_R0C3 : RAM64x18
      port map(A_DOUT(17) => nc11, A_DOUT(16) => A_DOUT(63), 
        A_DOUT(15) => A_DOUT(62), A_DOUT(14) => A_DOUT(61), 
        A_DOUT(13) => A_DOUT(60), A_DOUT(12) => A_DOUT(59), 
        A_DOUT(11) => A_DOUT(58), A_DOUT(10) => A_DOUT(57), 
        A_DOUT(9) => A_DOUT(56), A_DOUT(8) => nc3, A_DOUT(7) => 
        A_DOUT(31), A_DOUT(6) => A_DOUT(30), A_DOUT(5) => 
        A_DOUT(29), A_DOUT(4) => A_DOUT(28), A_DOUT(3) => 
        A_DOUT(27), A_DOUT(2) => A_DOUT(26), A_DOUT(1) => 
        A_DOUT(25), A_DOUT(0) => A_DOUT(24), B_DOUT(17) => nc10, 
        B_DOUT(16) => B_DOUT(63), B_DOUT(15) => B_DOUT(62), 
        B_DOUT(14) => B_DOUT(61), B_DOUT(13) => B_DOUT(60), 
        B_DOUT(12) => B_DOUT(59), B_DOUT(11) => B_DOUT(58), 
        B_DOUT(10) => B_DOUT(57), B_DOUT(9) => B_DOUT(56), 
        B_DOUT(8) => nc2, B_DOUT(7) => B_DOUT(31), B_DOUT(6) => 
        B_DOUT(30), B_DOUT(5) => B_DOUT(29), B_DOUT(4) => 
        B_DOUT(28), B_DOUT(3) => B_DOUT(27), B_DOUT(2) => 
        B_DOUT(26), B_DOUT(1) => B_DOUT(25), B_DOUT(0) => 
        B_DOUT(24), BUSY => OPEN, A_ADDR_CLK => CLK, A_DOUT_CLK
         => \VCC\, A_ADDR_SRST_N => A_ADDR_SRST_N, A_DOUT_SRST_N
         => \VCC\, A_ADDR_ARST_N => A_ADDR_ARST_N, A_DOUT_ARST_N
         => \VCC\, A_ADDR_EN => A_ADDR_EN, A_DOUT_EN => \VCC\, 
        A_BLK(1) => A_BLK, A_BLK(0) => \VCC\, A_ADDR(9) => \GND\, 
        A_ADDR(8) => A_ADDR(4), A_ADDR(7) => A_ADDR(3), A_ADDR(6)
         => A_ADDR(2), A_ADDR(5) => A_ADDR(1), A_ADDR(4) => 
        A_ADDR(0), A_ADDR(3) => \GND\, A_ADDR(2) => \GND\, 
        A_ADDR(1) => \GND\, A_ADDR(0) => \GND\, B_ADDR_CLK => CLK, 
        B_DOUT_CLK => \VCC\, B_ADDR_SRST_N => B_ADDR_SRST_N, 
        B_DOUT_SRST_N => \VCC\, B_ADDR_ARST_N => B_ADDR_ARST_N, 
        B_DOUT_ARST_N => \VCC\, B_ADDR_EN => B_ADDR_EN, B_DOUT_EN
         => \VCC\, B_BLK(1) => B_BLK, B_BLK(0) => \VCC\, 
        B_ADDR(9) => \GND\, B_ADDR(8) => B_ADDR(4), B_ADDR(7) => 
        B_ADDR(3), B_ADDR(6) => B_ADDR(2), B_ADDR(5) => B_ADDR(1), 
        B_ADDR(4) => B_ADDR(0), B_ADDR(3) => \GND\, B_ADDR(2) => 
        \GND\, B_ADDR(1) => \GND\, B_ADDR(0) => \GND\, C_CLK => 
        CLK, C_ADDR(9) => \GND\, C_ADDR(8) => C_ADDR(5), 
        C_ADDR(7) => C_ADDR(4), C_ADDR(6) => C_ADDR(3), C_ADDR(5)
         => C_ADDR(2), C_ADDR(4) => C_ADDR(1), C_ADDR(3) => 
        C_ADDR(0), C_ADDR(2) => \GND\, C_ADDR(1) => \GND\, 
        C_ADDR(0) => \GND\, C_DIN(17) => \GND\, C_DIN(16) => 
        \GND\, C_DIN(15) => \GND\, C_DIN(14) => \GND\, C_DIN(13)
         => \GND\, C_DIN(12) => \GND\, C_DIN(11) => \GND\, 
        C_DIN(10) => \GND\, C_DIN(9) => \GND\, C_DIN(8) => \GND\, 
        C_DIN(7) => C_DIN(31), C_DIN(6) => C_DIN(30), C_DIN(5)
         => C_DIN(29), C_DIN(4) => C_DIN(28), C_DIN(3) => 
        C_DIN(27), C_DIN(2) => C_DIN(26), C_DIN(1) => C_DIN(25), 
        C_DIN(0) => C_DIN(24), C_WEN => C_WEN, C_BLK(1) => C_BLK, 
        C_BLK(0) => \VCC\, A_EN => \VCC\, A_ADDR_LAT => \GND\, 
        A_DOUT_LAT => \VCC\, A_WIDTH(2) => \VCC\, A_WIDTH(1) => 
        \GND\, A_WIDTH(0) => \GND\, B_EN => \VCC\, B_ADDR_LAT => 
        \GND\, B_DOUT_LAT => \VCC\, B_WIDTH(2) => \VCC\, 
        B_WIDTH(1) => \GND\, B_WIDTH(0) => \GND\, C_EN => \VCC\, 
        C_WIDTH(2) => \GND\, C_WIDTH(1) => \VCC\, C_WIDTH(0) => 
        \VCC\, SII_LOCK => \GND\);
    
    GND_power_inst1 : GND
      port map( Y => GND_power_net1);

    VCC_power_inst1 : VCC
      port map( Y => VCC_power_net1);


end DEF_ARCH; 
