set_component Eval_pfm_OSC_0_OSC
# Microsemi Corp.
# Date: 2017-Feb-16 10:22:10
#

create_clock -period 20 [ get_pins { I_RCOSC_25_50MHZ/CLKOUT } ]
