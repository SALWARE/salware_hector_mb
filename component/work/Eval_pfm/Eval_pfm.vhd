----------------------------------------------------------------------
-- Created by SmartDesign Thu Feb 16 10:22:10 2017
-- Version: v11.7 11.7.0.119
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
library COREAHBLITE_LIB;
use COREAHBLITE_LIB.all;
use COREAHBLITE_LIB.components.all;
----------------------------------------------------------------------
-- Eval_pfm entity declaration
----------------------------------------------------------------------
entity Eval_pfm is
    -- Port list
    port(
        -- Inputs
        DEVRST_N               : in    std_logic;
        HADDR_M0               : in    std_logic_vector(31 downto 0);
        HBURST_M0              : in    std_logic_vector(2 downto 0);
        HMASTLOCK_M0           : in    std_logic;
        HPROT_M0               : in    std_logic_vector(3 downto 0);
        HRDATA_S0              : in    std_logic_vector(31 downto 0);
        HREADYOUT_S0           : in    std_logic;
        HRESP_S0               : in    std_logic_vector(1 downto 0);
        HSIZE_M0               : in    std_logic_vector(2 downto 0);
        HTRANS_M0              : in    std_logic_vector(1 downto 0);
        HWDATA_M0              : in    std_logic_vector(31 downto 0);
        HWRITE_M0              : in    std_logic;
        MCCC_CLK_BASE          : in    std_logic;
        MCCC_CLK_BASE_PLL_LOCK : in    std_logic;
        MDDR_DQS_TMATCH_0_IN   : in    std_logic;
        MMUART_1_RXD           : in    std_logic;
        USB_ULPI_DIR           : in    std_logic;
        USB_ULPI_NXT           : in    std_logic;
        USB_ULPI_XCLK          : in    std_logic;
        -- Outputs
        GPIO_29_M2F            : out   std_logic;
        HADDR_S0               : out   std_logic_vector(31 downto 0);
        HBURST_S0              : out   std_logic_vector(2 downto 0);
        HMASTLOCK_S0           : out   std_logic;
        HPROT_S0               : out   std_logic_vector(3 downto 0);
        HRDATA_M0              : out   std_logic_vector(31 downto 0);
        HREADY_M0              : out   std_logic;
        HREADY_S0              : out   std_logic;
        HRESP_M0               : out   std_logic_vector(1 downto 0);
        HSEL_S0                : out   std_logic;
        HSIZE_S0               : out   std_logic_vector(2 downto 0);
        HTRANS_S0              : out   std_logic_vector(1 downto 0);
        HWDATA_S0              : out   std_logic_vector(31 downto 0);
        HWRITE_S0              : out   std_logic;
        MDDR_ADDR              : out   std_logic_vector(15 downto 0);
        MDDR_BA                : out   std_logic_vector(2 downto 0);
        MDDR_CAS_N             : out   std_logic;
        MDDR_CKE               : out   std_logic;
        MDDR_CLK               : out   std_logic;
        MDDR_CLK_N             : out   std_logic;
        MDDR_CS_N              : out   std_logic;
        MDDR_DQS_TMATCH_0_OUT  : out   std_logic;
        MDDR_ODT               : out   std_logic;
        MDDR_RAS_N             : out   std_logic;
        MDDR_RESET_N           : out   std_logic;
        MDDR_WE_N              : out   std_logic;
        MMUART_1_TXD           : out   std_logic;
        MSS_HPMS_READY         : out   std_logic;
        USB_ULPI_STP           : out   std_logic;
        -- Inouts
        MDDR_DM_RDQS           : inout std_logic_vector(1 downto 0);
        MDDR_DQ                : inout std_logic_vector(15 downto 0);
        MDDR_DQS               : inout std_logic_vector(1 downto 0);
        USB_ULPI_DATA          : inout std_logic_vector(7 downto 0)
        );
end Eval_pfm;
----------------------------------------------------------------------
-- Eval_pfm architecture body
----------------------------------------------------------------------
architecture RTL of Eval_pfm is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- CoreAHBLite   -   Actel:DirectCore:CoreAHBLite:5.2.100
-- using entity instantiation for component CoreAHBLite
-- CoreConfigP   -   Actel:DirectCore:CoreConfigP:7.0.105
component CoreConfigP
    generic( 
        DEVICE_090         : integer := 0 ;
        ENABLE_SOFT_RESETS : integer := 1 ;
        FDDR_IN_USE        : integer := 0 ;
        MDDR_IN_USE        : integer := 1 ;
        SDIF0_IN_USE       : integer := 0 ;
        SDIF0_PCIE         : integer := 0 ;
        SDIF1_IN_USE       : integer := 0 ;
        SDIF1_PCIE         : integer := 0 ;
        SDIF2_IN_USE       : integer := 0 ;
        SDIF2_PCIE         : integer := 0 ;
        SDIF3_IN_USE       : integer := 0 ;
        SDIF3_PCIE         : integer := 0 
        );
    -- Port list
    port(
        -- Inputs
        FDDR_PRDATA                    : in  std_logic_vector(31 downto 0);
        FDDR_PREADY                    : in  std_logic;
        FDDR_PSLVERR                   : in  std_logic;
        FIC_2_APB_M_PADDR              : in  std_logic_vector(16 downto 2);
        FIC_2_APB_M_PCLK               : in  std_logic;
        FIC_2_APB_M_PENABLE            : in  std_logic;
        FIC_2_APB_M_PRESET_N           : in  std_logic;
        FIC_2_APB_M_PSEL               : in  std_logic;
        FIC_2_APB_M_PWDATA             : in  std_logic_vector(31 downto 0);
        FIC_2_APB_M_PWRITE             : in  std_logic;
        INIT_DONE                      : in  std_logic;
        MDDR_PRDATA                    : in  std_logic_vector(31 downto 0);
        MDDR_PREADY                    : in  std_logic;
        MDDR_PSLVERR                   : in  std_logic;
        SDIF0_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF0_PREADY                   : in  std_logic;
        SDIF0_PSLVERR                  : in  std_logic;
        SDIF1_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF1_PREADY                   : in  std_logic;
        SDIF1_PSLVERR                  : in  std_logic;
        SDIF2_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF2_PREADY                   : in  std_logic;
        SDIF2_PSLVERR                  : in  std_logic;
        SDIF3_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF3_PREADY                   : in  std_logic;
        SDIF3_PSLVERR                  : in  std_logic;
        SDIF_RELEASED                  : in  std_logic;
        -- Outputs
        APB_S_PCLK                     : out std_logic;
        APB_S_PRESET_N                 : out std_logic;
        CONFIG1_DONE                   : out std_logic;
        CONFIG2_DONE                   : out std_logic;
        FDDR_PADDR                     : out std_logic_vector(15 downto 2);
        FDDR_PENABLE                   : out std_logic;
        FDDR_PSEL                      : out std_logic;
        FDDR_PWDATA                    : out std_logic_vector(31 downto 0);
        FDDR_PWRITE                    : out std_logic;
        FIC_2_APB_M_PRDATA             : out std_logic_vector(31 downto 0);
        FIC_2_APB_M_PREADY             : out std_logic;
        FIC_2_APB_M_PSLVERR            : out std_logic;
        MDDR_PADDR                     : out std_logic_vector(15 downto 2);
        MDDR_PENABLE                   : out std_logic;
        MDDR_PSEL                      : out std_logic;
        MDDR_PWDATA                    : out std_logic_vector(31 downto 0);
        MDDR_PWRITE                    : out std_logic;
        R_SDIF0_PRDATA                 : out std_logic_vector(31 downto 0);
        R_SDIF0_PSEL                   : out std_logic;
        R_SDIF0_PWRITE                 : out std_logic;
        R_SDIF1_PRDATA                 : out std_logic_vector(31 downto 0);
        R_SDIF1_PSEL                   : out std_logic;
        R_SDIF1_PWRITE                 : out std_logic;
        R_SDIF2_PRDATA                 : out std_logic_vector(31 downto 0);
        R_SDIF2_PSEL                   : out std_logic;
        R_SDIF2_PWRITE                 : out std_logic;
        R_SDIF3_PRDATA                 : out std_logic_vector(31 downto 0);
        R_SDIF3_PSEL                   : out std_logic;
        R_SDIF3_PWRITE                 : out std_logic;
        SDIF0_PADDR                    : out std_logic_vector(15 downto 2);
        SDIF0_PENABLE                  : out std_logic;
        SDIF0_PSEL                     : out std_logic;
        SDIF0_PWDATA                   : out std_logic_vector(31 downto 0);
        SDIF0_PWRITE                   : out std_logic;
        SDIF1_PADDR                    : out std_logic_vector(15 downto 2);
        SDIF1_PENABLE                  : out std_logic;
        SDIF1_PSEL                     : out std_logic;
        SDIF1_PWDATA                   : out std_logic_vector(31 downto 0);
        SDIF1_PWRITE                   : out std_logic;
        SDIF2_PADDR                    : out std_logic_vector(15 downto 2);
        SDIF2_PENABLE                  : out std_logic;
        SDIF2_PSEL                     : out std_logic;
        SDIF2_PWDATA                   : out std_logic_vector(31 downto 0);
        SDIF2_PWRITE                   : out std_logic;
        SDIF3_PADDR                    : out std_logic_vector(15 downto 2);
        SDIF3_PENABLE                  : out std_logic;
        SDIF3_PSEL                     : out std_logic;
        SDIF3_PWDATA                   : out std_logic_vector(31 downto 0);
        SDIF3_PWRITE                   : out std_logic;
        SOFT_EXT_RESET_OUT             : out std_logic;
        SOFT_FDDR_CORE_RESET           : out std_logic;
        SOFT_M3_RESET                  : out std_logic;
        SOFT_MDDR_DDR_AXI_S_CORE_RESET : out std_logic;
        SOFT_RESET_F2M                 : out std_logic;
        SOFT_SDIF0_0_CORE_RESET        : out std_logic;
        SOFT_SDIF0_1_CORE_RESET        : out std_logic;
        SOFT_SDIF0_CORE_RESET          : out std_logic;
        SOFT_SDIF0_PHY_RESET           : out std_logic;
        SOFT_SDIF1_CORE_RESET          : out std_logic;
        SOFT_SDIF1_PHY_RESET           : out std_logic;
        SOFT_SDIF2_CORE_RESET          : out std_logic;
        SOFT_SDIF2_PHY_RESET           : out std_logic;
        SOFT_SDIF3_CORE_RESET          : out std_logic;
        SOFT_SDIF3_PHY_RESET           : out std_logic
        );
end component;
-- CoreResetP   -   Actel:DirectCore:CoreResetP:7.0.104
component CoreResetP
    generic( 
        DDR_WAIT            : integer := 200 ;
        DEVICE_090          : integer := 0 ;
        DEVICE_VOLTAGE      : integer := 2 ;
        ENABLE_SOFT_RESETS  : integer := 1 ;
        EXT_RESET_CFG       : integer := 0 ;
        FDDR_IN_USE         : integer := 1 ;
        MDDR_IN_USE         : integer := 1 ;
        SDIF0_IN_USE        : integer := 0 ;
        SDIF0_PCIE          : integer := 0 ;
        SDIF0_PCIE_HOTRESET : integer := 1 ;
        SDIF0_PCIE_L2P2     : integer := 1 ;
        SDIF1_IN_USE        : integer := 0 ;
        SDIF1_PCIE          : integer := 0 ;
        SDIF1_PCIE_HOTRESET : integer := 1 ;
        SDIF1_PCIE_L2P2     : integer := 1 ;
        SDIF2_IN_USE        : integer := 0 ;
        SDIF2_PCIE          : integer := 0 ;
        SDIF2_PCIE_HOTRESET : integer := 1 ;
        SDIF2_PCIE_L2P2     : integer := 1 ;
        SDIF3_IN_USE        : integer := 0 ;
        SDIF3_PCIE          : integer := 0 ;
        SDIF3_PCIE_HOTRESET : integer := 1 ;
        SDIF3_PCIE_L2P2     : integer := 1 
        );
    -- Port list
    port(
        -- Inputs
        CLK_BASE                       : in  std_logic;
        CLK_LTSSM                      : in  std_logic;
        CONFIG1_DONE                   : in  std_logic;
        CONFIG2_DONE                   : in  std_logic;
        FAB_RESET_N                    : in  std_logic;
        FIC_2_APB_M_PRESET_N           : in  std_logic;
        FPLL_LOCK                      : in  std_logic;
        POWER_ON_RESET_N               : in  std_logic;
        RCOSC_25_50MHZ                 : in  std_logic;
        RESET_N_M2F                    : in  std_logic;
        SDIF0_PERST_N                  : in  std_logic;
        SDIF0_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF0_PSEL                     : in  std_logic;
        SDIF0_PWRITE                   : in  std_logic;
        SDIF0_SPLL_LOCK                : in  std_logic;
        SDIF1_PERST_N                  : in  std_logic;
        SDIF1_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF1_PSEL                     : in  std_logic;
        SDIF1_PWRITE                   : in  std_logic;
        SDIF1_SPLL_LOCK                : in  std_logic;
        SDIF2_PERST_N                  : in  std_logic;
        SDIF2_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF2_PSEL                     : in  std_logic;
        SDIF2_PWRITE                   : in  std_logic;
        SDIF2_SPLL_LOCK                : in  std_logic;
        SDIF3_PERST_N                  : in  std_logic;
        SDIF3_PRDATA                   : in  std_logic_vector(31 downto 0);
        SDIF3_PSEL                     : in  std_logic;
        SDIF3_PWRITE                   : in  std_logic;
        SDIF3_SPLL_LOCK                : in  std_logic;
        SOFT_EXT_RESET_OUT             : in  std_logic;
        SOFT_FDDR_CORE_RESET           : in  std_logic;
        SOFT_M3_RESET                  : in  std_logic;
        SOFT_MDDR_DDR_AXI_S_CORE_RESET : in  std_logic;
        SOFT_RESET_F2M                 : in  std_logic;
        SOFT_SDIF0_0_CORE_RESET        : in  std_logic;
        SOFT_SDIF0_1_CORE_RESET        : in  std_logic;
        SOFT_SDIF0_CORE_RESET          : in  std_logic;
        SOFT_SDIF0_PHY_RESET           : in  std_logic;
        SOFT_SDIF1_CORE_RESET          : in  std_logic;
        SOFT_SDIF1_PHY_RESET           : in  std_logic;
        SOFT_SDIF2_CORE_RESET          : in  std_logic;
        SOFT_SDIF2_PHY_RESET           : in  std_logic;
        SOFT_SDIF3_CORE_RESET          : in  std_logic;
        SOFT_SDIF3_PHY_RESET           : in  std_logic;
        -- Outputs
        DDR_READY                      : out std_logic;
        EXT_RESET_OUT                  : out std_logic;
        FDDR_CORE_RESET_N              : out std_logic;
        INIT_DONE                      : out std_logic;
        M3_RESET_N                     : out std_logic;
        MDDR_DDR_AXI_S_CORE_RESET_N    : out std_logic;
        MSS_HPMS_READY                 : out std_logic;
        RESET_N_F2M                    : out std_logic;
        SDIF0_0_CORE_RESET_N           : out std_logic;
        SDIF0_1_CORE_RESET_N           : out std_logic;
        SDIF0_CORE_RESET_N             : out std_logic;
        SDIF0_PHY_RESET_N              : out std_logic;
        SDIF1_CORE_RESET_N             : out std_logic;
        SDIF1_PHY_RESET_N              : out std_logic;
        SDIF2_CORE_RESET_N             : out std_logic;
        SDIF2_PHY_RESET_N              : out std_logic;
        SDIF3_CORE_RESET_N             : out std_logic;
        SDIF3_PHY_RESET_N              : out std_logic;
        SDIF_READY                     : out std_logic;
        SDIF_RELEASED                  : out std_logic
        );
end component;
-- Eval_pfm_MSS
component Eval_pfm_MSS
    -- Port list
    port(
        -- Inputs
        FIC_0_AHB_M_HRDATA        : in    std_logic_vector(31 downto 0);
        FIC_0_AHB_M_HREADY        : in    std_logic;
        FIC_0_AHB_M_HRESP         : in    std_logic;
        FIC_2_APB_M_PRDATA        : in    std_logic_vector(31 downto 0);
        FIC_2_APB_M_PREADY        : in    std_logic;
        FIC_2_APB_M_PSLVERR       : in    std_logic;
        MCCC_CLK_BASE             : in    std_logic;
        MCCC_CLK_BASE_PLL_LOCK    : in    std_logic;
        MDDR_APB_S_PADDR          : in    std_logic_vector(10 downto 2);
        MDDR_APB_S_PCLK           : in    std_logic;
        MDDR_APB_S_PENABLE        : in    std_logic;
        MDDR_APB_S_PRESET_N       : in    std_logic;
        MDDR_APB_S_PSEL           : in    std_logic;
        MDDR_APB_S_PWDATA         : in    std_logic_vector(15 downto 0);
        MDDR_APB_S_PWRITE         : in    std_logic;
        MDDR_DDR_AHB0_S_HADDR     : in    std_logic_vector(31 downto 0);
        MDDR_DDR_AHB0_S_HBURST    : in    std_logic_vector(2 downto 0);
        MDDR_DDR_AHB0_S_HMASTLOCK : in    std_logic;
        MDDR_DDR_AHB0_S_HREADY    : in    std_logic;
        MDDR_DDR_AHB0_S_HSEL      : in    std_logic;
        MDDR_DDR_AHB0_S_HSIZE     : in    std_logic_vector(1 downto 0);
        MDDR_DDR_AHB0_S_HTRANS    : in    std_logic_vector(1 downto 0);
        MDDR_DDR_AHB0_S_HWDATA    : in    std_logic_vector(31 downto 0);
        MDDR_DDR_AHB0_S_HWRITE    : in    std_logic;
        MDDR_DDR_CORE_RESET_N     : in    std_logic;
        MDDR_DQS_TMATCH_0_IN      : in    std_logic;
        MMUART_1_RXD              : in    std_logic;
        MSS_RESET_N_F2M           : in    std_logic;
        USB_ULPI_DIR              : in    std_logic;
        USB_ULPI_NXT              : in    std_logic;
        USB_ULPI_XCLK             : in    std_logic;
        -- Outputs
        FIC_0_AHB_M_HADDR         : out   std_logic_vector(31 downto 0);
        FIC_0_AHB_M_HSIZE         : out   std_logic_vector(1 downto 0);
        FIC_0_AHB_M_HTRANS        : out   std_logic_vector(1 downto 0);
        FIC_0_AHB_M_HWDATA        : out   std_logic_vector(31 downto 0);
        FIC_0_AHB_M_HWRITE        : out   std_logic;
        FIC_2_APB_M_PADDR         : out   std_logic_vector(15 downto 2);
        FIC_2_APB_M_PCLK          : out   std_logic;
        FIC_2_APB_M_PENABLE       : out   std_logic;
        FIC_2_APB_M_PRESET_N      : out   std_logic;
        FIC_2_APB_M_PSEL          : out   std_logic;
        FIC_2_APB_M_PWDATA        : out   std_logic_vector(31 downto 0);
        FIC_2_APB_M_PWRITE        : out   std_logic;
        GPIO_29_M2F               : out   std_logic;
        MDDR_ADDR                 : out   std_logic_vector(15 downto 0);
        MDDR_APB_S_PRDATA         : out   std_logic_vector(15 downto 0);
        MDDR_APB_S_PREADY         : out   std_logic;
        MDDR_APB_S_PSLVERR        : out   std_logic;
        MDDR_BA                   : out   std_logic_vector(2 downto 0);
        MDDR_CAS_N                : out   std_logic;
        MDDR_CKE                  : out   std_logic;
        MDDR_CLK                  : out   std_logic;
        MDDR_CLK_N                : out   std_logic;
        MDDR_CS_N                 : out   std_logic;
        MDDR_DDR_AHB0_S_HRDATA    : out   std_logic_vector(31 downto 0);
        MDDR_DDR_AHB0_S_HREADYOUT : out   std_logic;
        MDDR_DDR_AHB0_S_HRESP     : out   std_logic;
        MDDR_DQS_TMATCH_0_OUT     : out   std_logic;
        MDDR_ODT                  : out   std_logic;
        MDDR_RAS_N                : out   std_logic;
        MDDR_RESET_N              : out   std_logic;
        MDDR_WE_N                 : out   std_logic;
        MMUART_1_TXD              : out   std_logic;
        MSS_RESET_N_M2F           : out   std_logic;
        USB_ULPI_STP              : out   std_logic;
        -- Inouts
        MDDR_DM_RDQS              : inout std_logic_vector(1 downto 0);
        MDDR_DQ                   : inout std_logic_vector(15 downto 0);
        MDDR_DQS                  : inout std_logic_vector(1 downto 0);
        USB_ULPI_DATA             : inout std_logic_vector(7 downto 0)
        );
end component;
-- Eval_pfm_OSC_0_OSC   -   Actel:SgCore:OSC:2.0.101
component Eval_pfm_OSC_0_OSC
    -- Port list
    port(
        -- Inputs
        XTL                : in  std_logic;
        -- Outputs
        RCOSC_1MHZ_CCC     : out std_logic;
        RCOSC_1MHZ_O2F     : out std_logic;
        RCOSC_25_50MHZ_CCC : out std_logic;
        RCOSC_25_50MHZ_O2F : out std_logic;
        XTLOSC_CCC         : out std_logic;
        XTLOSC_O2F         : out std_logic
        );
end component;
-- SYSRESET
component SYSRESET
    -- Port list
    port(
        -- Inputs
        DEVRST_N         : in  std_logic;
        -- Outputs
        POWER_ON_RESET_N : out std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal AHBmmaster0_HRDATA                           : std_logic_vector(31 downto 0);
signal AHBmmaster0_HREADY                           : std_logic;
signal AHBmmaster0_HRESP                            : std_logic_vector(1 downto 0);
signal AHBmslave0_HADDR                             : std_logic_vector(31 downto 0);
signal AHBmslave0_HBURST                            : std_logic_vector(2 downto 0);
signal AHBmslave0_HMASTLOCK                         : std_logic;
signal AHBmslave0_HPROT                             : std_logic_vector(3 downto 0);
signal AHBmslave0_HREADY                            : std_logic;
signal AHBmslave0_HSELx                             : std_logic;
signal AHBmslave0_HSIZE                             : std_logic_vector(2 downto 0);
signal AHBmslave0_HTRANS                            : std_logic_vector(1 downto 0);
signal AHBmslave0_HWDATA                            : std_logic_vector(31 downto 0);
signal AHBmslave0_HWRITE                            : std_logic;
signal CoreAHBLite_1_AHBmslave16_HADDR              : std_logic_vector(31 downto 0);
signal CoreAHBLite_1_AHBmslave16_HBURST             : std_logic_vector(2 downto 0);
signal CoreAHBLite_1_AHBmslave16_HMASTLOCK          : std_logic;
signal CoreAHBLite_1_AHBmslave16_HPROT              : std_logic_vector(3 downto 0);
signal CoreAHBLite_1_AHBmslave16_HRDATA             : std_logic_vector(31 downto 0);
signal CoreAHBLite_1_AHBmslave16_HREADY             : std_logic;
signal CoreAHBLite_1_AHBmslave16_HREADYOUT          : std_logic;
signal CoreAHBLite_1_AHBmslave16_HSELx              : std_logic;
signal CoreAHBLite_1_AHBmslave16_HTRANS             : std_logic_vector(1 downto 0);
signal CoreAHBLite_1_AHBmslave16_HWDATA             : std_logic_vector(31 downto 0);
signal CoreAHBLite_1_AHBmslave16_HWRITE             : std_logic;
signal CoreConfigP_0_APB_S_PCLK                     : std_logic;
signal CoreConfigP_0_APB_S_PRESET_N                 : std_logic;
signal CoreConfigP_0_CONFIG1_DONE                   : std_logic;
signal CoreConfigP_0_CONFIG2_DONE                   : std_logic;
signal CoreConfigP_0_MDDR_APBmslave_PENABLE         : std_logic;
signal CoreConfigP_0_MDDR_APBmslave_PREADY          : std_logic;
signal CoreConfigP_0_MDDR_APBmslave_PSELx           : std_logic;
signal CoreConfigP_0_MDDR_APBmslave_PSLVERR         : std_logic;
signal CoreConfigP_0_MDDR_APBmslave_PWRITE          : std_logic;
signal CoreConfigP_0_SOFT_EXT_RESET_OUT             : std_logic;
signal CoreConfigP_0_SOFT_FDDR_CORE_RESET           : std_logic;
signal CoreConfigP_0_SOFT_MDDR_DDR_AXI_S_CORE_RESET : std_logic;
signal CoreConfigP_0_SOFT_RESET_F2M                 : std_logic;
signal CoreResetP_0_FDDR_CORE_RESET_N               : std_logic;
signal CoreResetP_0_INIT_DONE                       : std_logic;
signal CoreResetP_0_RESET_N_F2M                     : std_logic;
signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HADDR        : std_logic_vector(31 downto 0);
signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRDATA       : std_logic_vector(31 downto 0);
signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HREADY       : std_logic;
signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HTRANS       : std_logic_vector(1 downto 0);
signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HWDATA       : std_logic_vector(31 downto 0);
signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HWRITE       : std_logic;
signal Eval_pfm_MSS_0_FIC_2_APB_M_PCLK              : std_logic;
signal Eval_pfm_MSS_0_FIC_2_APB_M_PRESET_N          : std_logic;
signal Eval_pfm_MSS_0_FIC_2_APB_MASTER_PENABLE      : std_logic;
signal Eval_pfm_MSS_0_FIC_2_APB_MASTER_PRDATA       : std_logic_vector(31 downto 0);
signal Eval_pfm_MSS_0_FIC_2_APB_MASTER_PREADY       : std_logic;
signal Eval_pfm_MSS_0_FIC_2_APB_MASTER_PSELx        : std_logic;
signal Eval_pfm_MSS_0_FIC_2_APB_MASTER_PSLVERR      : std_logic;
signal Eval_pfm_MSS_0_FIC_2_APB_MASTER_PWDATA       : std_logic_vector(31 downto 0);
signal Eval_pfm_MSS_0_FIC_2_APB_MASTER_PWRITE       : std_logic;
signal Eval_pfm_MSS_0_MSS_RESET_N_M2F               : std_logic;
signal GPIO_29_M2F_net_0                            : std_logic;
signal MDDR_ADDR_net_0                              : std_logic_vector(15 downto 0);
signal MDDR_BA_net_0                                : std_logic_vector(2 downto 0);
signal MDDR_CAS_N_net_0                             : std_logic;
signal MDDR_CKE_net_0                               : std_logic;
signal MDDR_CLK_net_0                               : std_logic;
signal MDDR_CLK_N_net_0                             : std_logic;
signal MDDR_CS_N_net_0                              : std_logic;
signal MDDR_DQS_TMATCH_0_OUT_net_0                  : std_logic;
signal MDDR_ODT_net_0                               : std_logic;
signal MDDR_RAS_N_net_0                             : std_logic;
signal MDDR_RESET_N_net_0                           : std_logic;
signal MDDR_WE_N_net_0                              : std_logic;
signal MMUART_1_TXD_net_0                           : std_logic;
signal MSS_HPMS_READY_1                             : std_logic;
signal net_0                                        : std_logic;
signal OSC_0_RCOSC_25_50MHZ_O2F                     : std_logic;
signal SYSRESET_0_POWER_ON_RESET_N                  : std_logic;
signal USB_ULPI_STP_1                               : std_logic;
signal MMUART_1_TXD_net_1                           : std_logic;
signal MDDR_DQS_TMATCH_0_OUT_net_1                  : std_logic;
signal MDDR_CAS_N_net_1                             : std_logic;
signal MDDR_CLK_net_1                               : std_logic;
signal MDDR_CLK_N_net_1                             : std_logic;
signal MDDR_CKE_net_1                               : std_logic;
signal MDDR_CS_N_net_1                              : std_logic;
signal MDDR_ODT_net_1                               : std_logic;
signal MDDR_RAS_N_net_1                             : std_logic;
signal MDDR_RESET_N_net_1                           : std_logic;
signal MDDR_WE_N_net_1                              : std_logic;
signal USB_ULPI_STP_1_net_0                         : std_logic;
signal GPIO_29_M2F_net_1                            : std_logic;
signal AHBmmaster0_HREADY_net_0                     : std_logic;
signal AHBmslave0_HWRITE_net_0                      : std_logic;
signal AHBmslave0_HSELx_net_0                       : std_logic;
signal AHBmslave0_HREADY_net_0                      : std_logic;
signal AHBmslave0_HMASTLOCK_net_0                   : std_logic;
signal MSS_HPMS_READY_1_net_0                       : std_logic;
signal MDDR_ADDR_net_1                              : std_logic_vector(15 downto 0);
signal MDDR_BA_net_1                                : std_logic_vector(2 downto 0);
signal AHBmmaster0_HRDATA_net_0                     : std_logic_vector(31 downto 0);
signal AHBmmaster0_HRESP_net_0                      : std_logic_vector(1 downto 0);
signal AHBmslave0_HADDR_net_0                       : std_logic_vector(31 downto 0);
signal AHBmslave0_HTRANS_net_0                      : std_logic_vector(1 downto 0);
signal AHBmslave0_HSIZE_net_0                       : std_logic_vector(2 downto 0);
signal AHBmslave0_HWDATA_net_0                      : std_logic_vector(31 downto 0);
signal AHBmslave0_HBURST_net_0                      : std_logic_vector(2 downto 0);
signal AHBmslave0_HPROT_net_0                       : std_logic_vector(3 downto 0);
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal VCC_net                                      : std_logic;
signal GND_net                                      : std_logic;
signal FDDR_PRDATA_const_net_0                      : std_logic_vector(31 downto 0);
signal SDIF0_PRDATA_const_net_0                     : std_logic_vector(31 downto 0);
signal SDIF1_PRDATA_const_net_0                     : std_logic_vector(31 downto 0);
signal SDIF2_PRDATA_const_net_0                     : std_logic_vector(31 downto 0);
signal SDIF3_PRDATA_const_net_0                     : std_logic_vector(31 downto 0);
signal SDIF0_PRDATA_const_net_1                     : std_logic_vector(31 downto 0);
signal SDIF1_PRDATA_const_net_1                     : std_logic_vector(31 downto 0);
signal SDIF2_PRDATA_const_net_1                     : std_logic_vector(31 downto 0);
signal SDIF3_PRDATA_const_net_1                     : std_logic_vector(31 downto 0);
signal HBURST_M0_const_net_0                        : std_logic_vector(2 downto 0);
signal HPROT_M0_const_net_0                         : std_logic_vector(3 downto 0);
signal HADDR_M1_const_net_0                         : std_logic_vector(31 downto 0);
signal HTRANS_M1_const_net_0                        : std_logic_vector(1 downto 0);
signal HSIZE_M1_const_net_0                         : std_logic_vector(2 downto 0);
signal HBURST_M1_const_net_0                        : std_logic_vector(2 downto 0);
signal HPROT_M1_const_net_0                         : std_logic_vector(3 downto 0);
signal HWDATA_M1_const_net_0                        : std_logic_vector(31 downto 0);
signal HADDR_M2_const_net_0                         : std_logic_vector(31 downto 0);
signal HTRANS_M2_const_net_0                        : std_logic_vector(1 downto 0);
signal HSIZE_M2_const_net_0                         : std_logic_vector(2 downto 0);
signal HBURST_M2_const_net_0                        : std_logic_vector(2 downto 0);
signal HPROT_M2_const_net_0                         : std_logic_vector(3 downto 0);
signal HWDATA_M2_const_net_0                        : std_logic_vector(31 downto 0);
signal HADDR_M3_const_net_0                         : std_logic_vector(31 downto 0);
signal HTRANS_M3_const_net_0                        : std_logic_vector(1 downto 0);
signal HSIZE_M3_const_net_0                         : std_logic_vector(2 downto 0);
signal HBURST_M3_const_net_0                        : std_logic_vector(2 downto 0);
signal HPROT_M3_const_net_0                         : std_logic_vector(3 downto 0);
signal HWDATA_M3_const_net_0                        : std_logic_vector(31 downto 0);
signal HRDATA_S1_const_net_0                        : std_logic_vector(31 downto 0);
signal HRESP_S1_const_net_0                         : std_logic_vector(1 downto 0);
signal HRDATA_S2_const_net_0                        : std_logic_vector(31 downto 0);
signal HRESP_S2_const_net_0                         : std_logic_vector(1 downto 0);
signal HRDATA_S3_const_net_0                        : std_logic_vector(31 downto 0);
signal HRESP_S3_const_net_0                         : std_logic_vector(1 downto 0);
signal HRDATA_S4_const_net_0                        : std_logic_vector(31 downto 0);
signal HRESP_S4_const_net_0                         : std_logic_vector(1 downto 0);
signal HRDATA_S5_const_net_0                        : std_logic_vector(31 downto 0);
signal HRESP_S5_const_net_0                         : std_logic_vector(1 downto 0);
signal HRDATA_S6_const_net_0                        : std_logic_vector(31 downto 0);
signal HRESP_S6_const_net_0                         : std_logic_vector(1 downto 0);
signal HRDATA_S7_const_net_0                        : std_logic_vector(31 downto 0);
signal HRESP_S7_const_net_0                         : std_logic_vector(1 downto 0);
signal HRDATA_S8_const_net_0                        : std_logic_vector(31 downto 0);
signal HRESP_S8_const_net_0                         : std_logic_vector(1 downto 0);
signal HRDATA_S9_const_net_0                        : std_logic_vector(31 downto 0);
signal HRESP_S9_const_net_0                         : std_logic_vector(1 downto 0);
signal HRDATA_S10_const_net_0                       : std_logic_vector(31 downto 0);
signal HRESP_S10_const_net_0                        : std_logic_vector(1 downto 0);
signal HRDATA_S11_const_net_0                       : std_logic_vector(31 downto 0);
signal HRESP_S11_const_net_0                        : std_logic_vector(1 downto 0);
signal HRDATA_S12_const_net_0                       : std_logic_vector(31 downto 0);
signal HRESP_S12_const_net_0                        : std_logic_vector(1 downto 0);
signal HRDATA_S13_const_net_0                       : std_logic_vector(31 downto 0);
signal HRESP_S13_const_net_0                        : std_logic_vector(1 downto 0);
signal HRDATA_S14_const_net_0                       : std_logic_vector(31 downto 0);
signal HRESP_S14_const_net_0                        : std_logic_vector(1 downto 0);
signal HRDATA_S15_const_net_0                       : std_logic_vector(31 downto 0);
signal HRESP_S15_const_net_0                        : std_logic_vector(1 downto 0);
signal HRDATA_S16_const_net_0                       : std_logic_vector(31 downto 0);
signal HRESP_S16_const_net_0                        : std_logic_vector(1 downto 0);
signal HADDR_M1_const_net_1                         : std_logic_vector(31 downto 0);
signal HTRANS_M1_const_net_1                        : std_logic_vector(1 downto 0);
signal HSIZE_M1_const_net_1                         : std_logic_vector(2 downto 0);
signal HBURST_M1_const_net_1                        : std_logic_vector(2 downto 0);
signal HPROT_M1_const_net_1                         : std_logic_vector(3 downto 0);
signal HWDATA_M1_const_net_1                        : std_logic_vector(31 downto 0);
signal HADDR_M2_const_net_1                         : std_logic_vector(31 downto 0);
signal HTRANS_M2_const_net_1                        : std_logic_vector(1 downto 0);
signal HSIZE_M2_const_net_1                         : std_logic_vector(2 downto 0);
signal HBURST_M2_const_net_1                        : std_logic_vector(2 downto 0);
signal HPROT_M2_const_net_1                         : std_logic_vector(3 downto 0);
signal HWDATA_M2_const_net_1                        : std_logic_vector(31 downto 0);
signal HADDR_M3_const_net_1                         : std_logic_vector(31 downto 0);
signal HTRANS_M3_const_net_1                        : std_logic_vector(1 downto 0);
signal HSIZE_M3_const_net_1                         : std_logic_vector(2 downto 0);
signal HBURST_M3_const_net_1                        : std_logic_vector(2 downto 0);
signal HPROT_M3_const_net_1                         : std_logic_vector(3 downto 0);
signal HWDATA_M3_const_net_1                        : std_logic_vector(31 downto 0);
signal HRDATA_S0_const_net_0                        : std_logic_vector(31 downto 0);
signal HRESP_S0_const_net_0                         : std_logic_vector(1 downto 0);
signal HRDATA_S1_const_net_1                        : std_logic_vector(31 downto 0);
signal HRESP_S1_const_net_1                         : std_logic_vector(1 downto 0);
signal HRDATA_S2_const_net_1                        : std_logic_vector(31 downto 0);
signal HRESP_S2_const_net_1                         : std_logic_vector(1 downto 0);
signal HRDATA_S3_const_net_1                        : std_logic_vector(31 downto 0);
signal HRESP_S3_const_net_1                         : std_logic_vector(1 downto 0);
signal HRDATA_S4_const_net_1                        : std_logic_vector(31 downto 0);
signal HRESP_S4_const_net_1                         : std_logic_vector(1 downto 0);
signal HRDATA_S5_const_net_1                        : std_logic_vector(31 downto 0);
signal HRESP_S5_const_net_1                         : std_logic_vector(1 downto 0);
signal HRDATA_S6_const_net_1                        : std_logic_vector(31 downto 0);
signal HRESP_S6_const_net_1                         : std_logic_vector(1 downto 0);
signal HRDATA_S7_const_net_1                        : std_logic_vector(31 downto 0);
signal HRESP_S7_const_net_1                         : std_logic_vector(1 downto 0);
signal HRDATA_S8_const_net_1                        : std_logic_vector(31 downto 0);
signal HRESP_S8_const_net_1                         : std_logic_vector(1 downto 0);
signal HRDATA_S9_const_net_1                        : std_logic_vector(31 downto 0);
signal HRESP_S9_const_net_1                         : std_logic_vector(1 downto 0);
signal HRDATA_S10_const_net_1                       : std_logic_vector(31 downto 0);
signal HRESP_S10_const_net_1                        : std_logic_vector(1 downto 0);
signal HRDATA_S11_const_net_1                       : std_logic_vector(31 downto 0);
signal HRESP_S11_const_net_1                        : std_logic_vector(1 downto 0);
signal HRDATA_S12_const_net_1                       : std_logic_vector(31 downto 0);
signal HRESP_S12_const_net_1                        : std_logic_vector(1 downto 0);
signal HRDATA_S13_const_net_1                       : std_logic_vector(31 downto 0);
signal HRESP_S13_const_net_1                        : std_logic_vector(1 downto 0);
signal HRDATA_S14_const_net_1                       : std_logic_vector(31 downto 0);
signal HRESP_S14_const_net_1                        : std_logic_vector(1 downto 0);
signal HRDATA_S15_const_net_1                       : std_logic_vector(31 downto 0);
signal HRESP_S15_const_net_1                        : std_logic_vector(1 downto 0);
----------------------------------------------------------------------
-- Bus Interface Nets Declarations - Unequal Pin Widths
----------------------------------------------------------------------
signal CoreAHBLite_1_AHBmslave16_HRESP_0_1to1       : std_logic_vector(1 to 1);
signal CoreAHBLite_1_AHBmslave16_HRESP_0_0to0       : std_logic_vector(0 to 0);
signal CoreAHBLite_1_AHBmslave16_HRESP_0            : std_logic_vector(1 downto 0);
signal CoreAHBLite_1_AHBmslave16_HRESP              : std_logic;

signal CoreAHBLite_1_AHBmslave16_HSIZE              : std_logic_vector(2 downto 0);
signal CoreAHBLite_1_AHBmslave16_HSIZE_0_1to0       : std_logic_vector(1 downto 0);
signal CoreAHBLite_1_AHBmslave16_HSIZE_0            : std_logic_vector(1 downto 0);

signal CoreConfigP_0_MDDR_APBmslave_PADDR           : std_logic_vector(15 downto 2);
signal CoreConfigP_0_MDDR_APBmslave_PADDR_0_10to2   : std_logic_vector(10 downto 2);
signal CoreConfigP_0_MDDR_APBmslave_PADDR_0         : std_logic_vector(10 downto 2);

signal CoreConfigP_0_MDDR_APBmslave_PRDATA          : std_logic_vector(15 downto 0);
signal CoreConfigP_0_MDDR_APBmslave_PRDATA_0_31to16 : std_logic_vector(31 downto 16);
signal CoreConfigP_0_MDDR_APBmslave_PRDATA_0_15to0  : std_logic_vector(15 downto 0);
signal CoreConfigP_0_MDDR_APBmslave_PRDATA_0        : std_logic_vector(31 downto 0);

signal CoreConfigP_0_MDDR_APBmslave_PWDATA          : std_logic_vector(31 downto 0);
signal CoreConfigP_0_MDDR_APBmslave_PWDATA_0_15to0  : std_logic_vector(15 downto 0);
signal CoreConfigP_0_MDDR_APBmslave_PWDATA_0        : std_logic_vector(15 downto 0);

signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRESP        : std_logic_vector(1 downto 0);
signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRESP_0_0to0 : std_logic_vector(0 to 0);
signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRESP_0      : std_logic;

signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE_0_2to2 : std_logic_vector(2 to 2);
signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE_0_1to0 : std_logic_vector(1 downto 0);
signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE_0      : std_logic_vector(2 downto 0);
signal Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE        : std_logic_vector(1 downto 0);

signal Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR        : std_logic_vector(15 downto 2);
signal Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR_0_16to16: std_logic_vector(16 to 16);
signal Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR_0_15to2: std_logic_vector(15 downto 2);
signal Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR_0      : std_logic_vector(16 downto 2);


begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 VCC_net                  <= '1';
 GND_net                  <= '0';
 FDDR_PRDATA_const_net_0  <= B"00000000000000000000000000000000";
 SDIF0_PRDATA_const_net_0 <= B"00000000000000000000000000000000";
 SDIF1_PRDATA_const_net_0 <= B"00000000000000000000000000000000";
 SDIF2_PRDATA_const_net_0 <= B"00000000000000000000000000000000";
 SDIF3_PRDATA_const_net_0 <= B"00000000000000000000000000000000";
 SDIF0_PRDATA_const_net_1 <= B"00000000000000000000000000000000";
 SDIF1_PRDATA_const_net_1 <= B"00000000000000000000000000000000";
 SDIF2_PRDATA_const_net_1 <= B"00000000000000000000000000000000";
 SDIF3_PRDATA_const_net_1 <= B"00000000000000000000000000000000";
 HBURST_M0_const_net_0    <= B"000";
 HPROT_M0_const_net_0     <= B"0000";
 HADDR_M1_const_net_0     <= B"00000000000000000000000000000000";
 HTRANS_M1_const_net_0    <= B"00";
 HSIZE_M1_const_net_0     <= B"000";
 HBURST_M1_const_net_0    <= B"000";
 HPROT_M1_const_net_0     <= B"0000";
 HWDATA_M1_const_net_0    <= B"00000000000000000000000000000000";
 HADDR_M2_const_net_0     <= B"00000000000000000000000000000000";
 HTRANS_M2_const_net_0    <= B"00";
 HSIZE_M2_const_net_0     <= B"000";
 HBURST_M2_const_net_0    <= B"000";
 HPROT_M2_const_net_0     <= B"0000";
 HWDATA_M2_const_net_0    <= B"00000000000000000000000000000000";
 HADDR_M3_const_net_0     <= B"00000000000000000000000000000000";
 HTRANS_M3_const_net_0    <= B"00";
 HSIZE_M3_const_net_0     <= B"000";
 HBURST_M3_const_net_0    <= B"000";
 HPROT_M3_const_net_0     <= B"0000";
 HWDATA_M3_const_net_0    <= B"00000000000000000000000000000000";
 HRDATA_S1_const_net_0    <= B"00000000000000000000000000000000";
 HRESP_S1_const_net_0     <= B"00";
 HRDATA_S2_const_net_0    <= B"00000000000000000000000000000000";
 HRESP_S2_const_net_0     <= B"00";
 HRDATA_S3_const_net_0    <= B"00000000000000000000000000000000";
 HRESP_S3_const_net_0     <= B"00";
 HRDATA_S4_const_net_0    <= B"00000000000000000000000000000000";
 HRESP_S4_const_net_0     <= B"00";
 HRDATA_S5_const_net_0    <= B"00000000000000000000000000000000";
 HRESP_S5_const_net_0     <= B"00";
 HRDATA_S6_const_net_0    <= B"00000000000000000000000000000000";
 HRESP_S6_const_net_0     <= B"00";
 HRDATA_S7_const_net_0    <= B"00000000000000000000000000000000";
 HRESP_S7_const_net_0     <= B"00";
 HRDATA_S8_const_net_0    <= B"00000000000000000000000000000000";
 HRESP_S8_const_net_0     <= B"00";
 HRDATA_S9_const_net_0    <= B"00000000000000000000000000000000";
 HRESP_S9_const_net_0     <= B"00";
 HRDATA_S10_const_net_0   <= B"00000000000000000000000000000000";
 HRESP_S10_const_net_0    <= B"00";
 HRDATA_S11_const_net_0   <= B"00000000000000000000000000000000";
 HRESP_S11_const_net_0    <= B"00";
 HRDATA_S12_const_net_0   <= B"00000000000000000000000000000000";
 HRESP_S12_const_net_0    <= B"00";
 HRDATA_S13_const_net_0   <= B"00000000000000000000000000000000";
 HRESP_S13_const_net_0    <= B"00";
 HRDATA_S14_const_net_0   <= B"00000000000000000000000000000000";
 HRESP_S14_const_net_0    <= B"00";
 HRDATA_S15_const_net_0   <= B"00000000000000000000000000000000";
 HRESP_S15_const_net_0    <= B"00";
 HRDATA_S16_const_net_0   <= B"00000000000000000000000000000000";
 HRESP_S16_const_net_0    <= B"00";
 HADDR_M1_const_net_1     <= B"00000000000000000000000000000000";
 HTRANS_M1_const_net_1    <= B"00";
 HSIZE_M1_const_net_1     <= B"000";
 HBURST_M1_const_net_1    <= B"000";
 HPROT_M1_const_net_1     <= B"0000";
 HWDATA_M1_const_net_1    <= B"00000000000000000000000000000000";
 HADDR_M2_const_net_1     <= B"00000000000000000000000000000000";
 HTRANS_M2_const_net_1    <= B"00";
 HSIZE_M2_const_net_1     <= B"000";
 HBURST_M2_const_net_1    <= B"000";
 HPROT_M2_const_net_1     <= B"0000";
 HWDATA_M2_const_net_1    <= B"00000000000000000000000000000000";
 HADDR_M3_const_net_1     <= B"00000000000000000000000000000000";
 HTRANS_M3_const_net_1    <= B"00";
 HSIZE_M3_const_net_1     <= B"000";
 HBURST_M3_const_net_1    <= B"000";
 HPROT_M3_const_net_1     <= B"0000";
 HWDATA_M3_const_net_1    <= B"00000000000000000000000000000000";
 HRDATA_S0_const_net_0    <= B"00000000000000000000000000000000";
 HRESP_S0_const_net_0     <= B"00";
 HRDATA_S1_const_net_1    <= B"00000000000000000000000000000000";
 HRESP_S1_const_net_1     <= B"00";
 HRDATA_S2_const_net_1    <= B"00000000000000000000000000000000";
 HRESP_S2_const_net_1     <= B"00";
 HRDATA_S3_const_net_1    <= B"00000000000000000000000000000000";
 HRESP_S3_const_net_1     <= B"00";
 HRDATA_S4_const_net_1    <= B"00000000000000000000000000000000";
 HRESP_S4_const_net_1     <= B"00";
 HRDATA_S5_const_net_1    <= B"00000000000000000000000000000000";
 HRESP_S5_const_net_1     <= B"00";
 HRDATA_S6_const_net_1    <= B"00000000000000000000000000000000";
 HRESP_S6_const_net_1     <= B"00";
 HRDATA_S7_const_net_1    <= B"00000000000000000000000000000000";
 HRESP_S7_const_net_1     <= B"00";
 HRDATA_S8_const_net_1    <= B"00000000000000000000000000000000";
 HRESP_S8_const_net_1     <= B"00";
 HRDATA_S9_const_net_1    <= B"00000000000000000000000000000000";
 HRESP_S9_const_net_1     <= B"00";
 HRDATA_S10_const_net_1   <= B"00000000000000000000000000000000";
 HRESP_S10_const_net_1    <= B"00";
 HRDATA_S11_const_net_1   <= B"00000000000000000000000000000000";
 HRESP_S11_const_net_1    <= B"00";
 HRDATA_S12_const_net_1   <= B"00000000000000000000000000000000";
 HRESP_S12_const_net_1    <= B"00";
 HRDATA_S13_const_net_1   <= B"00000000000000000000000000000000";
 HRESP_S13_const_net_1    <= B"00";
 HRDATA_S14_const_net_1   <= B"00000000000000000000000000000000";
 HRESP_S14_const_net_1    <= B"00";
 HRDATA_S15_const_net_1   <= B"00000000000000000000000000000000";
 HRESP_S15_const_net_1    <= B"00";
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 MMUART_1_TXD_net_1          <= MMUART_1_TXD_net_0;
 MMUART_1_TXD                <= MMUART_1_TXD_net_1;
 MDDR_DQS_TMATCH_0_OUT_net_1 <= MDDR_DQS_TMATCH_0_OUT_net_0;
 MDDR_DQS_TMATCH_0_OUT       <= MDDR_DQS_TMATCH_0_OUT_net_1;
 MDDR_CAS_N_net_1            <= MDDR_CAS_N_net_0;
 MDDR_CAS_N                  <= MDDR_CAS_N_net_1;
 MDDR_CLK_net_1              <= MDDR_CLK_net_0;
 MDDR_CLK                    <= MDDR_CLK_net_1;
 MDDR_CLK_N_net_1            <= MDDR_CLK_N_net_0;
 MDDR_CLK_N                  <= MDDR_CLK_N_net_1;
 MDDR_CKE_net_1              <= MDDR_CKE_net_0;
 MDDR_CKE                    <= MDDR_CKE_net_1;
 MDDR_CS_N_net_1             <= MDDR_CS_N_net_0;
 MDDR_CS_N                   <= MDDR_CS_N_net_1;
 MDDR_ODT_net_1              <= MDDR_ODT_net_0;
 MDDR_ODT                    <= MDDR_ODT_net_1;
 MDDR_RAS_N_net_1            <= MDDR_RAS_N_net_0;
 MDDR_RAS_N                  <= MDDR_RAS_N_net_1;
 MDDR_RESET_N_net_1          <= MDDR_RESET_N_net_0;
 MDDR_RESET_N                <= MDDR_RESET_N_net_1;
 MDDR_WE_N_net_1             <= MDDR_WE_N_net_0;
 MDDR_WE_N                   <= MDDR_WE_N_net_1;
 USB_ULPI_STP_1_net_0        <= USB_ULPI_STP_1;
 USB_ULPI_STP                <= USB_ULPI_STP_1_net_0;
 GPIO_29_M2F_net_1           <= GPIO_29_M2F_net_0;
 GPIO_29_M2F                 <= GPIO_29_M2F_net_1;
 AHBmmaster0_HREADY_net_0    <= AHBmmaster0_HREADY;
 HREADY_M0                   <= AHBmmaster0_HREADY_net_0;
 AHBmslave0_HWRITE_net_0     <= AHBmslave0_HWRITE;
 HWRITE_S0                   <= AHBmslave0_HWRITE_net_0;
 AHBmslave0_HSELx_net_0      <= AHBmslave0_HSELx;
 HSEL_S0                     <= AHBmslave0_HSELx_net_0;
 AHBmslave0_HREADY_net_0     <= AHBmslave0_HREADY;
 HREADY_S0                   <= AHBmslave0_HREADY_net_0;
 AHBmslave0_HMASTLOCK_net_0  <= AHBmslave0_HMASTLOCK;
 HMASTLOCK_S0                <= AHBmslave0_HMASTLOCK_net_0;
 MSS_HPMS_READY_1_net_0      <= MSS_HPMS_READY_1;
 MSS_HPMS_READY              <= MSS_HPMS_READY_1_net_0;
 MDDR_ADDR_net_1             <= MDDR_ADDR_net_0;
 MDDR_ADDR(15 downto 0)      <= MDDR_ADDR_net_1;
 MDDR_BA_net_1               <= MDDR_BA_net_0;
 MDDR_BA(2 downto 0)         <= MDDR_BA_net_1;
 AHBmmaster0_HRDATA_net_0    <= AHBmmaster0_HRDATA;
 HRDATA_M0(31 downto 0)      <= AHBmmaster0_HRDATA_net_0;
 AHBmmaster0_HRESP_net_0     <= AHBmmaster0_HRESP;
 HRESP_M0(1 downto 0)        <= AHBmmaster0_HRESP_net_0;
 AHBmslave0_HADDR_net_0      <= AHBmslave0_HADDR;
 HADDR_S0(31 downto 0)       <= AHBmslave0_HADDR_net_0;
 AHBmslave0_HTRANS_net_0     <= AHBmslave0_HTRANS;
 HTRANS_S0(1 downto 0)       <= AHBmslave0_HTRANS_net_0;
 AHBmslave0_HSIZE_net_0      <= AHBmslave0_HSIZE;
 HSIZE_S0(2 downto 0)        <= AHBmslave0_HSIZE_net_0;
 AHBmslave0_HWDATA_net_0     <= AHBmslave0_HWDATA;
 HWDATA_S0(31 downto 0)      <= AHBmslave0_HWDATA_net_0;
 AHBmslave0_HBURST_net_0     <= AHBmslave0_HBURST;
 HBURST_S0(2 downto 0)       <= AHBmslave0_HBURST_net_0;
 AHBmslave0_HPROT_net_0      <= AHBmslave0_HPROT;
 HPROT_S0(3 downto 0)        <= AHBmslave0_HPROT_net_0;
----------------------------------------------------------------------
-- Bus Interface Nets Assignments - Unequal Pin Widths
----------------------------------------------------------------------
 CoreAHBLite_1_AHBmslave16_HRESP_0_1to1(1) <= '0';
 CoreAHBLite_1_AHBmslave16_HRESP_0_0to0(0) <= CoreAHBLite_1_AHBmslave16_HRESP;
 CoreAHBLite_1_AHBmslave16_HRESP_0 <= ( CoreAHBLite_1_AHBmslave16_HRESP_0_1to1(1) & CoreAHBLite_1_AHBmslave16_HRESP_0_0to0(0) );

 CoreAHBLite_1_AHBmslave16_HSIZE_0_1to0(1 downto 0) <= CoreAHBLite_1_AHBmslave16_HSIZE(1 downto 0);
 CoreAHBLite_1_AHBmslave16_HSIZE_0 <= ( CoreAHBLite_1_AHBmslave16_HSIZE_0_1to0(1 downto 0) );

 CoreConfigP_0_MDDR_APBmslave_PADDR_0_10to2(10 downto 2) <= CoreConfigP_0_MDDR_APBmslave_PADDR(10 downto 2);
 CoreConfigP_0_MDDR_APBmslave_PADDR_0 <= ( CoreConfigP_0_MDDR_APBmslave_PADDR_0_10to2(10 downto 2) );

 CoreConfigP_0_MDDR_APBmslave_PRDATA_0_31to16(31 downto 16) <= B"0000000000000000";
 CoreConfigP_0_MDDR_APBmslave_PRDATA_0_15to0(15 downto 0) <= CoreConfigP_0_MDDR_APBmslave_PRDATA(15 downto 0);
 CoreConfigP_0_MDDR_APBmslave_PRDATA_0 <= ( CoreConfigP_0_MDDR_APBmslave_PRDATA_0_31to16(31 downto 16) & CoreConfigP_0_MDDR_APBmslave_PRDATA_0_15to0(15 downto 0) );

 CoreConfigP_0_MDDR_APBmslave_PWDATA_0_15to0(15 downto 0) <= CoreConfigP_0_MDDR_APBmslave_PWDATA(15 downto 0);
 CoreConfigP_0_MDDR_APBmslave_PWDATA_0 <= ( CoreConfigP_0_MDDR_APBmslave_PWDATA_0_15to0(15 downto 0) );

 Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRESP_0_0to0(0) <= Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRESP(0);
 Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRESP_0 <= ( Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRESP_0_0to0(0) );

 Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE_0_2to2(2) <= '0';
 Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE_0_1to0(1 downto 0) <= Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE(1 downto 0);
 Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE_0 <= ( Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE_0_2to2(2) & Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE_0_1to0(1 downto 0) );

 Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR_0_16to16(16) <= '0';
 Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR_0_15to2(15 downto 2) <= Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR(15 downto 2);
 Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR_0 <= ( Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR_0_16to16(16) & Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR_0_15to2(15 downto 2) );

----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- CoreAHBLite_0   -   Actel:DirectCore:CoreAHBLite:5.2.100
CoreAHBLite_0 : entity COREAHBLITE_LIB.CoreAHBLite
    generic map( 
        FAMILY             => ( 19 ),
        HADDR_SHG_CFG      => ( 1 ),
        M0_AHBSLOT0ENABLE  => ( 1 ),
        M0_AHBSLOT1ENABLE  => ( 0 ),
        M0_AHBSLOT2ENABLE  => ( 0 ),
        M0_AHBSLOT3ENABLE  => ( 0 ),
        M0_AHBSLOT4ENABLE  => ( 0 ),
        M0_AHBSLOT5ENABLE  => ( 0 ),
        M0_AHBSLOT6ENABLE  => ( 0 ),
        M0_AHBSLOT7ENABLE  => ( 0 ),
        M0_AHBSLOT8ENABLE  => ( 0 ),
        M0_AHBSLOT9ENABLE  => ( 0 ),
        M0_AHBSLOT10ENABLE => ( 0 ),
        M0_AHBSLOT11ENABLE => ( 0 ),
        M0_AHBSLOT12ENABLE => ( 0 ),
        M0_AHBSLOT13ENABLE => ( 0 ),
        M0_AHBSLOT14ENABLE => ( 0 ),
        M0_AHBSLOT15ENABLE => ( 0 ),
        M0_AHBSLOT16ENABLE => ( 0 ),
        M1_AHBSLOT0ENABLE  => ( 0 ),
        M1_AHBSLOT1ENABLE  => ( 0 ),
        M1_AHBSLOT2ENABLE  => ( 0 ),
        M1_AHBSLOT3ENABLE  => ( 0 ),
        M1_AHBSLOT4ENABLE  => ( 0 ),
        M1_AHBSLOT5ENABLE  => ( 0 ),
        M1_AHBSLOT6ENABLE  => ( 0 ),
        M1_AHBSLOT7ENABLE  => ( 0 ),
        M1_AHBSLOT8ENABLE  => ( 0 ),
        M1_AHBSLOT9ENABLE  => ( 0 ),
        M1_AHBSLOT10ENABLE => ( 0 ),
        M1_AHBSLOT11ENABLE => ( 0 ),
        M1_AHBSLOT12ENABLE => ( 0 ),
        M1_AHBSLOT13ENABLE => ( 0 ),
        M1_AHBSLOT14ENABLE => ( 0 ),
        M1_AHBSLOT15ENABLE => ( 0 ),
        M1_AHBSLOT16ENABLE => ( 0 ),
        M2_AHBSLOT0ENABLE  => ( 0 ),
        M2_AHBSLOT1ENABLE  => ( 0 ),
        M2_AHBSLOT2ENABLE  => ( 0 ),
        M2_AHBSLOT3ENABLE  => ( 0 ),
        M2_AHBSLOT4ENABLE  => ( 0 ),
        M2_AHBSLOT5ENABLE  => ( 0 ),
        M2_AHBSLOT6ENABLE  => ( 0 ),
        M2_AHBSLOT7ENABLE  => ( 0 ),
        M2_AHBSLOT8ENABLE  => ( 0 ),
        M2_AHBSLOT9ENABLE  => ( 0 ),
        M2_AHBSLOT10ENABLE => ( 0 ),
        M2_AHBSLOT11ENABLE => ( 0 ),
        M2_AHBSLOT12ENABLE => ( 0 ),
        M2_AHBSLOT13ENABLE => ( 0 ),
        M2_AHBSLOT14ENABLE => ( 0 ),
        M2_AHBSLOT15ENABLE => ( 0 ),
        M2_AHBSLOT16ENABLE => ( 0 ),
        M3_AHBSLOT0ENABLE  => ( 0 ),
        M3_AHBSLOT1ENABLE  => ( 0 ),
        M3_AHBSLOT2ENABLE  => ( 0 ),
        M3_AHBSLOT3ENABLE  => ( 0 ),
        M3_AHBSLOT4ENABLE  => ( 0 ),
        M3_AHBSLOT5ENABLE  => ( 0 ),
        M3_AHBSLOT6ENABLE  => ( 0 ),
        M3_AHBSLOT7ENABLE  => ( 0 ),
        M3_AHBSLOT8ENABLE  => ( 0 ),
        M3_AHBSLOT9ENABLE  => ( 0 ),
        M3_AHBSLOT10ENABLE => ( 0 ),
        M3_AHBSLOT11ENABLE => ( 0 ),
        M3_AHBSLOT12ENABLE => ( 0 ),
        M3_AHBSLOT13ENABLE => ( 0 ),
        M3_AHBSLOT14ENABLE => ( 0 ),
        M3_AHBSLOT15ENABLE => ( 0 ),
        M3_AHBSLOT16ENABLE => ( 0 ),
        MEMSPACE           => ( 5 ),
        SC_0               => ( 0 ),
        SC_1               => ( 0 ),
        SC_2               => ( 0 ),
        SC_3               => ( 0 ),
        SC_4               => ( 0 ),
        SC_5               => ( 0 ),
        SC_6               => ( 0 ),
        SC_7               => ( 0 ),
        SC_8               => ( 0 ),
        SC_9               => ( 0 ),
        SC_10              => ( 0 ),
        SC_11              => ( 0 ),
        SC_12              => ( 0 ),
        SC_13              => ( 0 ),
        SC_14              => ( 0 ),
        SC_15              => ( 0 )
        )
    port map( 
        -- Inputs
        HCLK          => MCCC_CLK_BASE,
        HRESETN       => MSS_HPMS_READY_1,
        REMAP_M0      => GND_net, -- tied to '0' from definition
        HMASTLOCK_M0  => GND_net, -- tied to '0' from definition
        HWRITE_M0     => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HWRITE,
        HMASTLOCK_M1  => GND_net, -- tied to '0' from definition
        HWRITE_M1     => GND_net, -- tied to '0' from definition
        HMASTLOCK_M2  => GND_net, -- tied to '0' from definition
        HWRITE_M2     => GND_net, -- tied to '0' from definition
        HMASTLOCK_M3  => GND_net, -- tied to '0' from definition
        HWRITE_M3     => GND_net, -- tied to '0' from definition
        HREADYOUT_S0  => HREADYOUT_S0,
        HREADYOUT_S1  => VCC_net, -- tied to '1' from definition
        HREADYOUT_S2  => VCC_net, -- tied to '1' from definition
        HREADYOUT_S3  => VCC_net, -- tied to '1' from definition
        HREADYOUT_S4  => VCC_net, -- tied to '1' from definition
        HREADYOUT_S5  => VCC_net, -- tied to '1' from definition
        HREADYOUT_S6  => VCC_net, -- tied to '1' from definition
        HREADYOUT_S7  => VCC_net, -- tied to '1' from definition
        HREADYOUT_S8  => VCC_net, -- tied to '1' from definition
        HREADYOUT_S9  => VCC_net, -- tied to '1' from definition
        HREADYOUT_S10 => VCC_net, -- tied to '1' from definition
        HREADYOUT_S11 => VCC_net, -- tied to '1' from definition
        HREADYOUT_S12 => VCC_net, -- tied to '1' from definition
        HREADYOUT_S13 => VCC_net, -- tied to '1' from definition
        HREADYOUT_S14 => VCC_net, -- tied to '1' from definition
        HREADYOUT_S15 => VCC_net, -- tied to '1' from definition
        HREADYOUT_S16 => VCC_net, -- tied to '1' from definition
        HADDR_M0      => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HADDR,
        HSIZE_M0      => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE_0,
        HTRANS_M0     => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HTRANS,
        HWDATA_M0     => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HWDATA,
        HBURST_M0     => HBURST_M0_const_net_0, -- tied to X"0" from definition
        HPROT_M0      => HPROT_M0_const_net_0, -- tied to X"0" from definition
        HADDR_M1      => HADDR_M1_const_net_0, -- tied to X"0" from definition
        HSIZE_M1      => HSIZE_M1_const_net_0, -- tied to X"0" from definition
        HTRANS_M1     => HTRANS_M1_const_net_0, -- tied to X"0" from definition
        HWDATA_M1     => HWDATA_M1_const_net_0, -- tied to X"0" from definition
        HBURST_M1     => HBURST_M1_const_net_0, -- tied to X"0" from definition
        HPROT_M1      => HPROT_M1_const_net_0, -- tied to X"0" from definition
        HADDR_M2      => HADDR_M2_const_net_0, -- tied to X"0" from definition
        HSIZE_M2      => HSIZE_M2_const_net_0, -- tied to X"0" from definition
        HTRANS_M2     => HTRANS_M2_const_net_0, -- tied to X"0" from definition
        HWDATA_M2     => HWDATA_M2_const_net_0, -- tied to X"0" from definition
        HBURST_M2     => HBURST_M2_const_net_0, -- tied to X"0" from definition
        HPROT_M2      => HPROT_M2_const_net_0, -- tied to X"0" from definition
        HADDR_M3      => HADDR_M3_const_net_0, -- tied to X"0" from definition
        HSIZE_M3      => HSIZE_M3_const_net_0, -- tied to X"0" from definition
        HTRANS_M3     => HTRANS_M3_const_net_0, -- tied to X"0" from definition
        HWDATA_M3     => HWDATA_M3_const_net_0, -- tied to X"0" from definition
        HBURST_M3     => HBURST_M3_const_net_0, -- tied to X"0" from definition
        HPROT_M3      => HPROT_M3_const_net_0, -- tied to X"0" from definition
        HRDATA_S0     => HRDATA_S0,
        HRESP_S0      => HRESP_S0,
        HRDATA_S1     => HRDATA_S1_const_net_0, -- tied to X"0" from definition
        HRESP_S1      => HRESP_S1_const_net_0, -- tied to X"0" from definition
        HRDATA_S2     => HRDATA_S2_const_net_0, -- tied to X"0" from definition
        HRESP_S2      => HRESP_S2_const_net_0, -- tied to X"0" from definition
        HRDATA_S3     => HRDATA_S3_const_net_0, -- tied to X"0" from definition
        HRESP_S3      => HRESP_S3_const_net_0, -- tied to X"0" from definition
        HRDATA_S4     => HRDATA_S4_const_net_0, -- tied to X"0" from definition
        HRESP_S4      => HRESP_S4_const_net_0, -- tied to X"0" from definition
        HRDATA_S5     => HRDATA_S5_const_net_0, -- tied to X"0" from definition
        HRESP_S5      => HRESP_S5_const_net_0, -- tied to X"0" from definition
        HRDATA_S6     => HRDATA_S6_const_net_0, -- tied to X"0" from definition
        HRESP_S6      => HRESP_S6_const_net_0, -- tied to X"0" from definition
        HRDATA_S7     => HRDATA_S7_const_net_0, -- tied to X"0" from definition
        HRESP_S7      => HRESP_S7_const_net_0, -- tied to X"0" from definition
        HRDATA_S8     => HRDATA_S8_const_net_0, -- tied to X"0" from definition
        HRESP_S8      => HRESP_S8_const_net_0, -- tied to X"0" from definition
        HRDATA_S9     => HRDATA_S9_const_net_0, -- tied to X"0" from definition
        HRESP_S9      => HRESP_S9_const_net_0, -- tied to X"0" from definition
        HRDATA_S10    => HRDATA_S10_const_net_0, -- tied to X"0" from definition
        HRESP_S10     => HRESP_S10_const_net_0, -- tied to X"0" from definition
        HRDATA_S11    => HRDATA_S11_const_net_0, -- tied to X"0" from definition
        HRESP_S11     => HRESP_S11_const_net_0, -- tied to X"0" from definition
        HRDATA_S12    => HRDATA_S12_const_net_0, -- tied to X"0" from definition
        HRESP_S12     => HRESP_S12_const_net_0, -- tied to X"0" from definition
        HRDATA_S13    => HRDATA_S13_const_net_0, -- tied to X"0" from definition
        HRESP_S13     => HRESP_S13_const_net_0, -- tied to X"0" from definition
        HRDATA_S14    => HRDATA_S14_const_net_0, -- tied to X"0" from definition
        HRESP_S14     => HRESP_S14_const_net_0, -- tied to X"0" from definition
        HRDATA_S15    => HRDATA_S15_const_net_0, -- tied to X"0" from definition
        HRESP_S15     => HRESP_S15_const_net_0, -- tied to X"0" from definition
        HRDATA_S16    => HRDATA_S16_const_net_0, -- tied to X"0" from definition
        HRESP_S16     => HRESP_S16_const_net_0, -- tied to X"0" from definition
        -- Outputs
        HREADY_M0     => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HREADY,
        HREADY_M1     => OPEN,
        HREADY_M2     => OPEN,
        HREADY_M3     => OPEN,
        HSEL_S0       => AHBmslave0_HSELx,
        HWRITE_S0     => AHBmslave0_HWRITE,
        HREADY_S0     => AHBmslave0_HREADY,
        HMASTLOCK_S0  => AHBmslave0_HMASTLOCK,
        HSEL_S1       => OPEN,
        HWRITE_S1     => OPEN,
        HREADY_S1     => OPEN,
        HMASTLOCK_S1  => OPEN,
        HSEL_S2       => OPEN,
        HWRITE_S2     => OPEN,
        HREADY_S2     => OPEN,
        HMASTLOCK_S2  => OPEN,
        HSEL_S3       => OPEN,
        HWRITE_S3     => OPEN,
        HREADY_S3     => OPEN,
        HMASTLOCK_S3  => OPEN,
        HSEL_S4       => OPEN,
        HWRITE_S4     => OPEN,
        HREADY_S4     => OPEN,
        HMASTLOCK_S4  => OPEN,
        HSEL_S5       => OPEN,
        HWRITE_S5     => OPEN,
        HREADY_S5     => OPEN,
        HMASTLOCK_S5  => OPEN,
        HSEL_S6       => OPEN,
        HWRITE_S6     => OPEN,
        HREADY_S6     => OPEN,
        HMASTLOCK_S6  => OPEN,
        HSEL_S7       => OPEN,
        HWRITE_S7     => OPEN,
        HREADY_S7     => OPEN,
        HMASTLOCK_S7  => OPEN,
        HSEL_S8       => OPEN,
        HWRITE_S8     => OPEN,
        HREADY_S8     => OPEN,
        HMASTLOCK_S8  => OPEN,
        HSEL_S9       => OPEN,
        HWRITE_S9     => OPEN,
        HREADY_S9     => OPEN,
        HMASTLOCK_S9  => OPEN,
        HSEL_S10      => OPEN,
        HWRITE_S10    => OPEN,
        HREADY_S10    => OPEN,
        HMASTLOCK_S10 => OPEN,
        HSEL_S11      => OPEN,
        HWRITE_S11    => OPEN,
        HREADY_S11    => OPEN,
        HMASTLOCK_S11 => OPEN,
        HSEL_S12      => OPEN,
        HWRITE_S12    => OPEN,
        HREADY_S12    => OPEN,
        HMASTLOCK_S12 => OPEN,
        HSEL_S13      => OPEN,
        HWRITE_S13    => OPEN,
        HREADY_S13    => OPEN,
        HMASTLOCK_S13 => OPEN,
        HSEL_S14      => OPEN,
        HWRITE_S14    => OPEN,
        HREADY_S14    => OPEN,
        HMASTLOCK_S14 => OPEN,
        HSEL_S15      => OPEN,
        HWRITE_S15    => OPEN,
        HREADY_S15    => OPEN,
        HMASTLOCK_S15 => OPEN,
        HSEL_S16      => OPEN,
        HWRITE_S16    => OPEN,
        HREADY_S16    => OPEN,
        HMASTLOCK_S16 => OPEN,
        HRESP_M0      => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRESP,
        HRDATA_M0     => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRDATA,
        HRESP_M1      => OPEN,
        HRDATA_M1     => OPEN,
        HRESP_M2      => OPEN,
        HRDATA_M2     => OPEN,
        HRESP_M3      => OPEN,
        HRDATA_M3     => OPEN,
        HADDR_S0      => AHBmslave0_HADDR,
        HSIZE_S0      => AHBmslave0_HSIZE,
        HTRANS_S0     => AHBmslave0_HTRANS,
        HWDATA_S0     => AHBmslave0_HWDATA,
        HBURST_S0     => AHBmslave0_HBURST,
        HPROT_S0      => AHBmslave0_HPROT,
        HADDR_S1      => OPEN,
        HSIZE_S1      => OPEN,
        HTRANS_S1     => OPEN,
        HWDATA_S1     => OPEN,
        HBURST_S1     => OPEN,
        HPROT_S1      => OPEN,
        HADDR_S2      => OPEN,
        HSIZE_S2      => OPEN,
        HTRANS_S2     => OPEN,
        HWDATA_S2     => OPEN,
        HBURST_S2     => OPEN,
        HPROT_S2      => OPEN,
        HADDR_S3      => OPEN,
        HSIZE_S3      => OPEN,
        HTRANS_S3     => OPEN,
        HWDATA_S3     => OPEN,
        HBURST_S3     => OPEN,
        HPROT_S3      => OPEN,
        HADDR_S4      => OPEN,
        HSIZE_S4      => OPEN,
        HTRANS_S4     => OPEN,
        HWDATA_S4     => OPEN,
        HBURST_S4     => OPEN,
        HPROT_S4      => OPEN,
        HADDR_S5      => OPEN,
        HSIZE_S5      => OPEN,
        HTRANS_S5     => OPEN,
        HWDATA_S5     => OPEN,
        HBURST_S5     => OPEN,
        HPROT_S5      => OPEN,
        HADDR_S6      => OPEN,
        HSIZE_S6      => OPEN,
        HTRANS_S6     => OPEN,
        HWDATA_S6     => OPEN,
        HBURST_S6     => OPEN,
        HPROT_S6      => OPEN,
        HADDR_S7      => OPEN,
        HSIZE_S7      => OPEN,
        HTRANS_S7     => OPEN,
        HWDATA_S7     => OPEN,
        HBURST_S7     => OPEN,
        HPROT_S7      => OPEN,
        HADDR_S8      => OPEN,
        HSIZE_S8      => OPEN,
        HTRANS_S8     => OPEN,
        HWDATA_S8     => OPEN,
        HBURST_S8     => OPEN,
        HPROT_S8      => OPEN,
        HADDR_S9      => OPEN,
        HSIZE_S9      => OPEN,
        HTRANS_S9     => OPEN,
        HWDATA_S9     => OPEN,
        HBURST_S9     => OPEN,
        HPROT_S9      => OPEN,
        HADDR_S10     => OPEN,
        HSIZE_S10     => OPEN,
        HTRANS_S10    => OPEN,
        HWDATA_S10    => OPEN,
        HBURST_S10    => OPEN,
        HPROT_S10     => OPEN,
        HADDR_S11     => OPEN,
        HSIZE_S11     => OPEN,
        HTRANS_S11    => OPEN,
        HWDATA_S11    => OPEN,
        HBURST_S11    => OPEN,
        HPROT_S11     => OPEN,
        HADDR_S12     => OPEN,
        HSIZE_S12     => OPEN,
        HTRANS_S12    => OPEN,
        HWDATA_S12    => OPEN,
        HBURST_S12    => OPEN,
        HPROT_S12     => OPEN,
        HADDR_S13     => OPEN,
        HSIZE_S13     => OPEN,
        HTRANS_S13    => OPEN,
        HWDATA_S13    => OPEN,
        HBURST_S13    => OPEN,
        HPROT_S13     => OPEN,
        HADDR_S14     => OPEN,
        HSIZE_S14     => OPEN,
        HTRANS_S14    => OPEN,
        HWDATA_S14    => OPEN,
        HBURST_S14    => OPEN,
        HPROT_S14     => OPEN,
        HADDR_S15     => OPEN,
        HSIZE_S15     => OPEN,
        HTRANS_S15    => OPEN,
        HWDATA_S15    => OPEN,
        HBURST_S15    => OPEN,
        HPROT_S15     => OPEN,
        HADDR_S16     => OPEN,
        HSIZE_S16     => OPEN,
        HTRANS_S16    => OPEN,
        HWDATA_S16    => OPEN,
        HBURST_S16    => OPEN,
        HPROT_S16     => OPEN 
        );
-- CoreAHBLite_1   -   Actel:DirectCore:CoreAHBLite:5.2.100
CoreAHBLite_1 : entity COREAHBLITE_LIB.CoreAHBLite
    generic map( 
        FAMILY             => ( 19 ),
        HADDR_SHG_CFG      => ( 1 ),
        M0_AHBSLOT0ENABLE  => ( 0 ),
        M0_AHBSLOT1ENABLE  => ( 0 ),
        M0_AHBSLOT2ENABLE  => ( 0 ),
        M0_AHBSLOT3ENABLE  => ( 0 ),
        M0_AHBSLOT4ENABLE  => ( 0 ),
        M0_AHBSLOT5ENABLE  => ( 0 ),
        M0_AHBSLOT6ENABLE  => ( 0 ),
        M0_AHBSLOT7ENABLE  => ( 0 ),
        M0_AHBSLOT8ENABLE  => ( 0 ),
        M0_AHBSLOT9ENABLE  => ( 0 ),
        M0_AHBSLOT10ENABLE => ( 0 ),
        M0_AHBSLOT11ENABLE => ( 0 ),
        M0_AHBSLOT12ENABLE => ( 0 ),
        M0_AHBSLOT13ENABLE => ( 0 ),
        M0_AHBSLOT14ENABLE => ( 0 ),
        M0_AHBSLOT15ENABLE => ( 0 ),
        M0_AHBSLOT16ENABLE => ( 1 ),
        M1_AHBSLOT0ENABLE  => ( 0 ),
        M1_AHBSLOT1ENABLE  => ( 0 ),
        M1_AHBSLOT2ENABLE  => ( 0 ),
        M1_AHBSLOT3ENABLE  => ( 0 ),
        M1_AHBSLOT4ENABLE  => ( 0 ),
        M1_AHBSLOT5ENABLE  => ( 0 ),
        M1_AHBSLOT6ENABLE  => ( 0 ),
        M1_AHBSLOT7ENABLE  => ( 0 ),
        M1_AHBSLOT8ENABLE  => ( 0 ),
        M1_AHBSLOT9ENABLE  => ( 0 ),
        M1_AHBSLOT10ENABLE => ( 0 ),
        M1_AHBSLOT11ENABLE => ( 0 ),
        M1_AHBSLOT12ENABLE => ( 0 ),
        M1_AHBSLOT13ENABLE => ( 0 ),
        M1_AHBSLOT14ENABLE => ( 0 ),
        M1_AHBSLOT15ENABLE => ( 0 ),
        M1_AHBSLOT16ENABLE => ( 0 ),
        M2_AHBSLOT0ENABLE  => ( 0 ),
        M2_AHBSLOT1ENABLE  => ( 0 ),
        M2_AHBSLOT2ENABLE  => ( 0 ),
        M2_AHBSLOT3ENABLE  => ( 0 ),
        M2_AHBSLOT4ENABLE  => ( 0 ),
        M2_AHBSLOT5ENABLE  => ( 0 ),
        M2_AHBSLOT6ENABLE  => ( 0 ),
        M2_AHBSLOT7ENABLE  => ( 0 ),
        M2_AHBSLOT8ENABLE  => ( 0 ),
        M2_AHBSLOT9ENABLE  => ( 0 ),
        M2_AHBSLOT10ENABLE => ( 0 ),
        M2_AHBSLOT11ENABLE => ( 0 ),
        M2_AHBSLOT12ENABLE => ( 0 ),
        M2_AHBSLOT13ENABLE => ( 0 ),
        M2_AHBSLOT14ENABLE => ( 0 ),
        M2_AHBSLOT15ENABLE => ( 0 ),
        M2_AHBSLOT16ENABLE => ( 0 ),
        M3_AHBSLOT0ENABLE  => ( 0 ),
        M3_AHBSLOT1ENABLE  => ( 0 ),
        M3_AHBSLOT2ENABLE  => ( 0 ),
        M3_AHBSLOT3ENABLE  => ( 0 ),
        M3_AHBSLOT4ENABLE  => ( 0 ),
        M3_AHBSLOT5ENABLE  => ( 0 ),
        M3_AHBSLOT6ENABLE  => ( 0 ),
        M3_AHBSLOT7ENABLE  => ( 0 ),
        M3_AHBSLOT8ENABLE  => ( 0 ),
        M3_AHBSLOT9ENABLE  => ( 0 ),
        M3_AHBSLOT10ENABLE => ( 0 ),
        M3_AHBSLOT11ENABLE => ( 0 ),
        M3_AHBSLOT12ENABLE => ( 0 ),
        M3_AHBSLOT13ENABLE => ( 0 ),
        M3_AHBSLOT14ENABLE => ( 0 ),
        M3_AHBSLOT15ENABLE => ( 0 ),
        M3_AHBSLOT16ENABLE => ( 0 ),
        MEMSPACE           => ( 0 ),
        SC_0               => ( 0 ),
        SC_1               => ( 0 ),
        SC_2               => ( 0 ),
        SC_3               => ( 0 ),
        SC_4               => ( 0 ),
        SC_5               => ( 0 ),
        SC_6               => ( 0 ),
        SC_7               => ( 0 ),
        SC_8               => ( 0 ),
        SC_9               => ( 0 ),
        SC_10              => ( 0 ),
        SC_11              => ( 0 ),
        SC_12              => ( 0 ),
        SC_13              => ( 0 ),
        SC_14              => ( 0 ),
        SC_15              => ( 0 )
        )
    port map( 
        -- Inputs
        HCLK                  => MCCC_CLK_BASE,
        HRESETN               => MSS_HPMS_READY_1,
        REMAP_M0              => GND_net, -- tied to '0' from definition
        HMASTLOCK_M0          => HMASTLOCK_M0,
        HWRITE_M0             => HWRITE_M0,
        HMASTLOCK_M1          => GND_net, -- tied to '0' from definition
        HWRITE_M1             => GND_net, -- tied to '0' from definition
        HMASTLOCK_M2          => GND_net, -- tied to '0' from definition
        HWRITE_M2             => GND_net, -- tied to '0' from definition
        HMASTLOCK_M3          => GND_net, -- tied to '0' from definition
        HWRITE_M3             => GND_net, -- tied to '0' from definition
        HREADYOUT_S0          => VCC_net, -- tied to '1' from definition
        HREADYOUT_S1          => VCC_net, -- tied to '1' from definition
        HREADYOUT_S2          => VCC_net, -- tied to '1' from definition
        HREADYOUT_S3          => VCC_net, -- tied to '1' from definition
        HREADYOUT_S4          => VCC_net, -- tied to '1' from definition
        HREADYOUT_S5          => VCC_net, -- tied to '1' from definition
        HREADYOUT_S6          => VCC_net, -- tied to '1' from definition
        HREADYOUT_S7          => VCC_net, -- tied to '1' from definition
        HREADYOUT_S8          => VCC_net, -- tied to '1' from definition
        HREADYOUT_S9          => VCC_net, -- tied to '1' from definition
        HREADYOUT_S10         => VCC_net, -- tied to '1' from definition
        HREADYOUT_S11         => VCC_net, -- tied to '1' from definition
        HREADYOUT_S12         => VCC_net, -- tied to '1' from definition
        HREADYOUT_S13         => VCC_net, -- tied to '1' from definition
        HREADYOUT_S14         => VCC_net, -- tied to '1' from definition
        HREADYOUT_S15         => VCC_net, -- tied to '1' from definition
        HREADYOUT_S16         => CoreAHBLite_1_AHBmslave16_HREADYOUT,
        HADDR_M0              => HADDR_M0,
        HSIZE_M0              => HSIZE_M0,
        HTRANS_M0             => HTRANS_M0,
        HWDATA_M0             => HWDATA_M0,
        HBURST_M0             => HBURST_M0,
        HPROT_M0              => HPROT_M0,
        HADDR_M1              => HADDR_M1_const_net_1, -- tied to X"0" from definition
        HSIZE_M1              => HSIZE_M1_const_net_1, -- tied to X"0" from definition
        HTRANS_M1             => HTRANS_M1_const_net_1, -- tied to X"0" from definition
        HWDATA_M1             => HWDATA_M1_const_net_1, -- tied to X"0" from definition
        HBURST_M1             => HBURST_M1_const_net_1, -- tied to X"0" from definition
        HPROT_M1              => HPROT_M1_const_net_1, -- tied to X"0" from definition
        HADDR_M2              => HADDR_M2_const_net_1, -- tied to X"0" from definition
        HSIZE_M2              => HSIZE_M2_const_net_1, -- tied to X"0" from definition
        HTRANS_M2             => HTRANS_M2_const_net_1, -- tied to X"0" from definition
        HWDATA_M2             => HWDATA_M2_const_net_1, -- tied to X"0" from definition
        HBURST_M2             => HBURST_M2_const_net_1, -- tied to X"0" from definition
        HPROT_M2              => HPROT_M2_const_net_1, -- tied to X"0" from definition
        HADDR_M3              => HADDR_M3_const_net_1, -- tied to X"0" from definition
        HSIZE_M3              => HSIZE_M3_const_net_1, -- tied to X"0" from definition
        HTRANS_M3             => HTRANS_M3_const_net_1, -- tied to X"0" from definition
        HWDATA_M3             => HWDATA_M3_const_net_1, -- tied to X"0" from definition
        HBURST_M3             => HBURST_M3_const_net_1, -- tied to X"0" from definition
        HPROT_M3              => HPROT_M3_const_net_1, -- tied to X"0" from definition
        HRDATA_S0             => HRDATA_S0_const_net_0, -- tied to X"0" from definition
        HRESP_S0              => HRESP_S0_const_net_0, -- tied to X"0" from definition
        HRDATA_S1             => HRDATA_S1_const_net_1, -- tied to X"0" from definition
        HRESP_S1              => HRESP_S1_const_net_1, -- tied to X"0" from definition
        HRDATA_S2             => HRDATA_S2_const_net_1, -- tied to X"0" from definition
        HRESP_S2              => HRESP_S2_const_net_1, -- tied to X"0" from definition
        HRDATA_S3             => HRDATA_S3_const_net_1, -- tied to X"0" from definition
        HRESP_S3              => HRESP_S3_const_net_1, -- tied to X"0" from definition
        HRDATA_S4             => HRDATA_S4_const_net_1, -- tied to X"0" from definition
        HRESP_S4              => HRESP_S4_const_net_1, -- tied to X"0" from definition
        HRDATA_S5             => HRDATA_S5_const_net_1, -- tied to X"0" from definition
        HRESP_S5              => HRESP_S5_const_net_1, -- tied to X"0" from definition
        HRDATA_S6             => HRDATA_S6_const_net_1, -- tied to X"0" from definition
        HRESP_S6              => HRESP_S6_const_net_1, -- tied to X"0" from definition
        HRDATA_S7             => HRDATA_S7_const_net_1, -- tied to X"0" from definition
        HRESP_S7              => HRESP_S7_const_net_1, -- tied to X"0" from definition
        HRDATA_S8             => HRDATA_S8_const_net_1, -- tied to X"0" from definition
        HRESP_S8              => HRESP_S8_const_net_1, -- tied to X"0" from definition
        HRDATA_S9             => HRDATA_S9_const_net_1, -- tied to X"0" from definition
        HRESP_S9              => HRESP_S9_const_net_1, -- tied to X"0" from definition
        HRDATA_S10            => HRDATA_S10_const_net_1, -- tied to X"0" from definition
        HRESP_S10             => HRESP_S10_const_net_1, -- tied to X"0" from definition
        HRDATA_S11            => HRDATA_S11_const_net_1, -- tied to X"0" from definition
        HRESP_S11             => HRESP_S11_const_net_1, -- tied to X"0" from definition
        HRDATA_S12            => HRDATA_S12_const_net_1, -- tied to X"0" from definition
        HRESP_S12             => HRESP_S12_const_net_1, -- tied to X"0" from definition
        HRDATA_S13            => HRDATA_S13_const_net_1, -- tied to X"0" from definition
        HRESP_S13             => HRESP_S13_const_net_1, -- tied to X"0" from definition
        HRDATA_S14            => HRDATA_S14_const_net_1, -- tied to X"0" from definition
        HRESP_S14             => HRESP_S14_const_net_1, -- tied to X"0" from definition
        HRDATA_S15            => HRDATA_S15_const_net_1, -- tied to X"0" from definition
        HRESP_S15             => HRESP_S15_const_net_1, -- tied to X"0" from definition
        HRDATA_S16            => CoreAHBLite_1_AHBmslave16_HRDATA,
        HRESP_S16(1 downto 0) => CoreAHBLite_1_AHBmslave16_HRESP_0,
        -- Outputs
        HREADY_M0             => AHBmmaster0_HREADY,
        HREADY_M1             => OPEN,
        HREADY_M2             => OPEN,
        HREADY_M3             => OPEN,
        HSEL_S0               => OPEN,
        HWRITE_S0             => OPEN,
        HREADY_S0             => OPEN,
        HMASTLOCK_S0          => OPEN,
        HSEL_S1               => OPEN,
        HWRITE_S1             => OPEN,
        HREADY_S1             => OPEN,
        HMASTLOCK_S1          => OPEN,
        HSEL_S2               => OPEN,
        HWRITE_S2             => OPEN,
        HREADY_S2             => OPEN,
        HMASTLOCK_S2          => OPEN,
        HSEL_S3               => OPEN,
        HWRITE_S3             => OPEN,
        HREADY_S3             => OPEN,
        HMASTLOCK_S3          => OPEN,
        HSEL_S4               => OPEN,
        HWRITE_S4             => OPEN,
        HREADY_S4             => OPEN,
        HMASTLOCK_S4          => OPEN,
        HSEL_S5               => OPEN,
        HWRITE_S5             => OPEN,
        HREADY_S5             => OPEN,
        HMASTLOCK_S5          => OPEN,
        HSEL_S6               => OPEN,
        HWRITE_S6             => OPEN,
        HREADY_S6             => OPEN,
        HMASTLOCK_S6          => OPEN,
        HSEL_S7               => OPEN,
        HWRITE_S7             => OPEN,
        HREADY_S7             => OPEN,
        HMASTLOCK_S7          => OPEN,
        HSEL_S8               => OPEN,
        HWRITE_S8             => OPEN,
        HREADY_S8             => OPEN,
        HMASTLOCK_S8          => OPEN,
        HSEL_S9               => OPEN,
        HWRITE_S9             => OPEN,
        HREADY_S9             => OPEN,
        HMASTLOCK_S9          => OPEN,
        HSEL_S10              => OPEN,
        HWRITE_S10            => OPEN,
        HREADY_S10            => OPEN,
        HMASTLOCK_S10         => OPEN,
        HSEL_S11              => OPEN,
        HWRITE_S11            => OPEN,
        HREADY_S11            => OPEN,
        HMASTLOCK_S11         => OPEN,
        HSEL_S12              => OPEN,
        HWRITE_S12            => OPEN,
        HREADY_S12            => OPEN,
        HMASTLOCK_S12         => OPEN,
        HSEL_S13              => OPEN,
        HWRITE_S13            => OPEN,
        HREADY_S13            => OPEN,
        HMASTLOCK_S13         => OPEN,
        HSEL_S14              => OPEN,
        HWRITE_S14            => OPEN,
        HREADY_S14            => OPEN,
        HMASTLOCK_S14         => OPEN,
        HSEL_S15              => OPEN,
        HWRITE_S15            => OPEN,
        HREADY_S15            => OPEN,
        HMASTLOCK_S15         => OPEN,
        HSEL_S16              => CoreAHBLite_1_AHBmslave16_HSELx,
        HWRITE_S16            => CoreAHBLite_1_AHBmslave16_HWRITE,
        HREADY_S16            => CoreAHBLite_1_AHBmslave16_HREADY,
        HMASTLOCK_S16         => CoreAHBLite_1_AHBmslave16_HMASTLOCK,
        HRESP_M0              => AHBmmaster0_HRESP,
        HRDATA_M0             => AHBmmaster0_HRDATA,
        HRESP_M1              => OPEN,
        HRDATA_M1             => OPEN,
        HRESP_M2              => OPEN,
        HRDATA_M2             => OPEN,
        HRESP_M3              => OPEN,
        HRDATA_M3             => OPEN,
        HADDR_S0              => OPEN,
        HSIZE_S0              => OPEN,
        HTRANS_S0             => OPEN,
        HWDATA_S0             => OPEN,
        HBURST_S0             => OPEN,
        HPROT_S0              => OPEN,
        HADDR_S1              => OPEN,
        HSIZE_S1              => OPEN,
        HTRANS_S1             => OPEN,
        HWDATA_S1             => OPEN,
        HBURST_S1             => OPEN,
        HPROT_S1              => OPEN,
        HADDR_S2              => OPEN,
        HSIZE_S2              => OPEN,
        HTRANS_S2             => OPEN,
        HWDATA_S2             => OPEN,
        HBURST_S2             => OPEN,
        HPROT_S2              => OPEN,
        HADDR_S3              => OPEN,
        HSIZE_S3              => OPEN,
        HTRANS_S3             => OPEN,
        HWDATA_S3             => OPEN,
        HBURST_S3             => OPEN,
        HPROT_S3              => OPEN,
        HADDR_S4              => OPEN,
        HSIZE_S4              => OPEN,
        HTRANS_S4             => OPEN,
        HWDATA_S4             => OPEN,
        HBURST_S4             => OPEN,
        HPROT_S4              => OPEN,
        HADDR_S5              => OPEN,
        HSIZE_S5              => OPEN,
        HTRANS_S5             => OPEN,
        HWDATA_S5             => OPEN,
        HBURST_S5             => OPEN,
        HPROT_S5              => OPEN,
        HADDR_S6              => OPEN,
        HSIZE_S6              => OPEN,
        HTRANS_S6             => OPEN,
        HWDATA_S6             => OPEN,
        HBURST_S6             => OPEN,
        HPROT_S6              => OPEN,
        HADDR_S7              => OPEN,
        HSIZE_S7              => OPEN,
        HTRANS_S7             => OPEN,
        HWDATA_S7             => OPEN,
        HBURST_S7             => OPEN,
        HPROT_S7              => OPEN,
        HADDR_S8              => OPEN,
        HSIZE_S8              => OPEN,
        HTRANS_S8             => OPEN,
        HWDATA_S8             => OPEN,
        HBURST_S8             => OPEN,
        HPROT_S8              => OPEN,
        HADDR_S9              => OPEN,
        HSIZE_S9              => OPEN,
        HTRANS_S9             => OPEN,
        HWDATA_S9             => OPEN,
        HBURST_S9             => OPEN,
        HPROT_S9              => OPEN,
        HADDR_S10             => OPEN,
        HSIZE_S10             => OPEN,
        HTRANS_S10            => OPEN,
        HWDATA_S10            => OPEN,
        HBURST_S10            => OPEN,
        HPROT_S10             => OPEN,
        HADDR_S11             => OPEN,
        HSIZE_S11             => OPEN,
        HTRANS_S11            => OPEN,
        HWDATA_S11            => OPEN,
        HBURST_S11            => OPEN,
        HPROT_S11             => OPEN,
        HADDR_S12             => OPEN,
        HSIZE_S12             => OPEN,
        HTRANS_S12            => OPEN,
        HWDATA_S12            => OPEN,
        HBURST_S12            => OPEN,
        HPROT_S12             => OPEN,
        HADDR_S13             => OPEN,
        HSIZE_S13             => OPEN,
        HTRANS_S13            => OPEN,
        HWDATA_S13            => OPEN,
        HBURST_S13            => OPEN,
        HPROT_S13             => OPEN,
        HADDR_S14             => OPEN,
        HSIZE_S14             => OPEN,
        HTRANS_S14            => OPEN,
        HWDATA_S14            => OPEN,
        HBURST_S14            => OPEN,
        HPROT_S14             => OPEN,
        HADDR_S15             => OPEN,
        HSIZE_S15             => OPEN,
        HTRANS_S15            => OPEN,
        HWDATA_S15            => OPEN,
        HBURST_S15            => OPEN,
        HPROT_S15             => OPEN,
        HADDR_S16             => CoreAHBLite_1_AHBmslave16_HADDR,
        HSIZE_S16             => CoreAHBLite_1_AHBmslave16_HSIZE,
        HTRANS_S16            => CoreAHBLite_1_AHBmslave16_HTRANS,
        HWDATA_S16            => CoreAHBLite_1_AHBmslave16_HWDATA,
        HBURST_S16            => CoreAHBLite_1_AHBmslave16_HBURST,
        HPROT_S16             => CoreAHBLite_1_AHBmslave16_HPROT 
        );
-- CoreConfigP_0   -   Actel:DirectCore:CoreConfigP:7.0.105
CoreConfigP_0 : CoreConfigP
    generic map( 
        DEVICE_090         => ( 0 ),
        ENABLE_SOFT_RESETS => ( 1 ),
        FDDR_IN_USE        => ( 0 ),
        MDDR_IN_USE        => ( 1 ),
        SDIF0_IN_USE       => ( 0 ),
        SDIF0_PCIE         => ( 0 ),
        SDIF1_IN_USE       => ( 0 ),
        SDIF1_PCIE         => ( 0 ),
        SDIF2_IN_USE       => ( 0 ),
        SDIF2_PCIE         => ( 0 ),
        SDIF3_IN_USE       => ( 0 ),
        SDIF3_PCIE         => ( 0 )
        )
    port map( 
        -- Inputs
        FIC_2_APB_M_PRESET_N           => Eval_pfm_MSS_0_FIC_2_APB_M_PRESET_N,
        FIC_2_APB_M_PCLK               => Eval_pfm_MSS_0_FIC_2_APB_M_PCLK,
        SDIF_RELEASED                  => GND_net, -- tied to '0' from definition
        INIT_DONE                      => CoreResetP_0_INIT_DONE,
        FIC_2_APB_M_PSEL               => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PSELx,
        FIC_2_APB_M_PENABLE            => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PENABLE,
        FIC_2_APB_M_PWRITE             => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PWRITE,
        MDDR_PREADY                    => CoreConfigP_0_MDDR_APBmslave_PREADY,
        MDDR_PSLVERR                   => CoreConfigP_0_MDDR_APBmslave_PSLVERR,
        FDDR_PREADY                    => VCC_net, -- tied to '1' from definition
        FDDR_PSLVERR                   => GND_net, -- tied to '0' from definition
        SDIF0_PREADY                   => VCC_net, -- tied to '1' from definition
        SDIF0_PSLVERR                  => GND_net, -- tied to '0' from definition
        SDIF1_PREADY                   => VCC_net, -- tied to '1' from definition
        SDIF1_PSLVERR                  => GND_net, -- tied to '0' from definition
        SDIF2_PREADY                   => VCC_net, -- tied to '1' from definition
        SDIF2_PSLVERR                  => GND_net, -- tied to '0' from definition
        SDIF3_PREADY                   => VCC_net, -- tied to '1' from definition
        SDIF3_PSLVERR                  => GND_net, -- tied to '0' from definition
        FIC_2_APB_M_PADDR              => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR_0,
        FIC_2_APB_M_PWDATA             => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PWDATA,
        MDDR_PRDATA                    => CoreConfigP_0_MDDR_APBmslave_PRDATA_0,
        FDDR_PRDATA                    => FDDR_PRDATA_const_net_0, -- tied to X"0" from definition
        SDIF0_PRDATA                   => SDIF0_PRDATA_const_net_0, -- tied to X"0" from definition
        SDIF1_PRDATA                   => SDIF1_PRDATA_const_net_0, -- tied to X"0" from definition
        SDIF2_PRDATA                   => SDIF2_PRDATA_const_net_0, -- tied to X"0" from definition
        SDIF3_PRDATA                   => SDIF3_PRDATA_const_net_0, -- tied to X"0" from definition
        -- Outputs
        APB_S_PCLK                     => CoreConfigP_0_APB_S_PCLK,
        APB_S_PRESET_N                 => CoreConfigP_0_APB_S_PRESET_N,
        CONFIG1_DONE                   => CoreConfigP_0_CONFIG1_DONE,
        CONFIG2_DONE                   => CoreConfigP_0_CONFIG2_DONE,
        FIC_2_APB_M_PREADY             => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PREADY,
        FIC_2_APB_M_PSLVERR            => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PSLVERR,
        MDDR_PSEL                      => CoreConfigP_0_MDDR_APBmslave_PSELx,
        MDDR_PENABLE                   => CoreConfigP_0_MDDR_APBmslave_PENABLE,
        MDDR_PWRITE                    => CoreConfigP_0_MDDR_APBmslave_PWRITE,
        FDDR_PSEL                      => OPEN,
        FDDR_PENABLE                   => OPEN,
        FDDR_PWRITE                    => OPEN,
        SDIF0_PSEL                     => OPEN,
        SDIF0_PENABLE                  => OPEN,
        SDIF0_PWRITE                   => OPEN,
        SDIF1_PSEL                     => OPEN,
        SDIF1_PENABLE                  => OPEN,
        SDIF1_PWRITE                   => OPEN,
        SDIF2_PSEL                     => OPEN,
        SDIF2_PENABLE                  => OPEN,
        SDIF2_PWRITE                   => OPEN,
        SDIF3_PSEL                     => OPEN,
        SDIF3_PENABLE                  => OPEN,
        SDIF3_PWRITE                   => OPEN,
        SOFT_EXT_RESET_OUT             => CoreConfigP_0_SOFT_EXT_RESET_OUT,
        SOFT_RESET_F2M                 => CoreConfigP_0_SOFT_RESET_F2M,
        SOFT_M3_RESET                  => net_0,
        SOFT_MDDR_DDR_AXI_S_CORE_RESET => CoreConfigP_0_SOFT_MDDR_DDR_AXI_S_CORE_RESET,
        SOFT_FDDR_CORE_RESET           => OPEN,
        SOFT_SDIF0_PHY_RESET           => OPEN,
        SOFT_SDIF0_CORE_RESET          => OPEN,
        SOFT_SDIF0_0_CORE_RESET        => OPEN,
        SOFT_SDIF0_1_CORE_RESET        => OPEN,
        SOFT_SDIF1_PHY_RESET           => OPEN,
        SOFT_SDIF1_CORE_RESET          => OPEN,
        SOFT_SDIF2_PHY_RESET           => OPEN,
        SOFT_SDIF2_CORE_RESET          => OPEN,
        SOFT_SDIF3_PHY_RESET           => OPEN,
        SOFT_SDIF3_CORE_RESET          => OPEN,
        R_SDIF0_PSEL                   => OPEN,
        R_SDIF0_PWRITE                 => OPEN,
        R_SDIF1_PSEL                   => OPEN,
        R_SDIF1_PWRITE                 => OPEN,
        R_SDIF2_PSEL                   => OPEN,
        R_SDIF2_PWRITE                 => OPEN,
        R_SDIF3_PSEL                   => OPEN,
        R_SDIF3_PWRITE                 => OPEN,
        FIC_2_APB_M_PRDATA             => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PRDATA,
        MDDR_PADDR                     => CoreConfigP_0_MDDR_APBmslave_PADDR,
        MDDR_PWDATA                    => CoreConfigP_0_MDDR_APBmslave_PWDATA,
        FDDR_PADDR                     => OPEN,
        FDDR_PWDATA                    => OPEN,
        SDIF0_PADDR                    => OPEN,
        SDIF0_PWDATA                   => OPEN,
        SDIF1_PADDR                    => OPEN,
        SDIF1_PWDATA                   => OPEN,
        SDIF2_PADDR                    => OPEN,
        SDIF2_PWDATA                   => OPEN,
        SDIF3_PADDR                    => OPEN,
        SDIF3_PWDATA                   => OPEN,
        R_SDIF0_PRDATA                 => OPEN,
        R_SDIF1_PRDATA                 => OPEN,
        R_SDIF2_PRDATA                 => OPEN,
        R_SDIF3_PRDATA                 => OPEN 
        );
-- CoreResetP_0   -   Actel:DirectCore:CoreResetP:7.0.104
CoreResetP_0 : CoreResetP
    generic map( 
        DDR_WAIT            => ( 200 ),
        DEVICE_090          => ( 0 ),
        DEVICE_VOLTAGE      => ( 2 ),
        ENABLE_SOFT_RESETS  => ( 1 ),
        EXT_RESET_CFG       => ( 0 ),
        FDDR_IN_USE         => ( 1 ),
        MDDR_IN_USE         => ( 1 ),
        SDIF0_IN_USE        => ( 0 ),
        SDIF0_PCIE          => ( 0 ),
        SDIF0_PCIE_HOTRESET => ( 1 ),
        SDIF0_PCIE_L2P2     => ( 1 ),
        SDIF1_IN_USE        => ( 0 ),
        SDIF1_PCIE          => ( 0 ),
        SDIF1_PCIE_HOTRESET => ( 1 ),
        SDIF1_PCIE_L2P2     => ( 1 ),
        SDIF2_IN_USE        => ( 0 ),
        SDIF2_PCIE          => ( 0 ),
        SDIF2_PCIE_HOTRESET => ( 1 ),
        SDIF2_PCIE_L2P2     => ( 1 ),
        SDIF3_IN_USE        => ( 0 ),
        SDIF3_PCIE          => ( 0 ),
        SDIF3_PCIE_HOTRESET => ( 1 ),
        SDIF3_PCIE_L2P2     => ( 1 )
        )
    port map( 
        -- Inputs
        RESET_N_M2F                    => Eval_pfm_MSS_0_MSS_RESET_N_M2F,
        FIC_2_APB_M_PRESET_N           => Eval_pfm_MSS_0_FIC_2_APB_M_PRESET_N,
        POWER_ON_RESET_N               => SYSRESET_0_POWER_ON_RESET_N,
        FAB_RESET_N                    => VCC_net,
        RCOSC_25_50MHZ                 => OSC_0_RCOSC_25_50MHZ_O2F,
        CLK_BASE                       => MCCC_CLK_BASE,
        CLK_LTSSM                      => GND_net, -- tied to '0' from definition
        FPLL_LOCK                      => MCCC_CLK_BASE_PLL_LOCK,
        SDIF0_SPLL_LOCK                => VCC_net, -- tied to '1' from definition
        SDIF1_SPLL_LOCK                => VCC_net, -- tied to '1' from definition
        SDIF2_SPLL_LOCK                => VCC_net, -- tied to '1' from definition
        SDIF3_SPLL_LOCK                => VCC_net, -- tied to '1' from definition
        CONFIG1_DONE                   => CoreConfigP_0_CONFIG1_DONE,
        CONFIG2_DONE                   => CoreConfigP_0_CONFIG2_DONE,
        SDIF0_PERST_N                  => VCC_net, -- tied to '1' from definition
        SDIF1_PERST_N                  => VCC_net, -- tied to '1' from definition
        SDIF2_PERST_N                  => VCC_net, -- tied to '1' from definition
        SDIF3_PERST_N                  => VCC_net, -- tied to '1' from definition
        SDIF0_PSEL                     => GND_net, -- tied to '0' from definition
        SDIF0_PWRITE                   => VCC_net, -- tied to '1' from definition
        SDIF1_PSEL                     => GND_net, -- tied to '0' from definition
        SDIF1_PWRITE                   => VCC_net, -- tied to '1' from definition
        SDIF2_PSEL                     => GND_net, -- tied to '0' from definition
        SDIF2_PWRITE                   => VCC_net, -- tied to '1' from definition
        SDIF3_PSEL                     => GND_net, -- tied to '0' from definition
        SDIF3_PWRITE                   => VCC_net, -- tied to '1' from definition
        SOFT_EXT_RESET_OUT             => CoreConfigP_0_SOFT_EXT_RESET_OUT,
        SOFT_RESET_F2M                 => CoreConfigP_0_SOFT_RESET_F2M,
        SOFT_M3_RESET                  => net_0,
        SOFT_MDDR_DDR_AXI_S_CORE_RESET => CoreConfigP_0_SOFT_MDDR_DDR_AXI_S_CORE_RESET,
        SOFT_FDDR_CORE_RESET           => CoreConfigP_0_SOFT_FDDR_CORE_RESET,
        SOFT_SDIF0_PHY_RESET           => GND_net, -- tied to '0' from definition
        SOFT_SDIF0_CORE_RESET          => GND_net, -- tied to '0' from definition
        SOFT_SDIF0_0_CORE_RESET        => GND_net, -- tied to '0' from definition
        SOFT_SDIF0_1_CORE_RESET        => GND_net, -- tied to '0' from definition
        SOFT_SDIF1_PHY_RESET           => GND_net, -- tied to '0' from definition
        SOFT_SDIF1_CORE_RESET          => GND_net, -- tied to '0' from definition
        SOFT_SDIF2_PHY_RESET           => GND_net, -- tied to '0' from definition
        SOFT_SDIF2_CORE_RESET          => GND_net, -- tied to '0' from definition
        SOFT_SDIF3_PHY_RESET           => GND_net, -- tied to '0' from definition
        SOFT_SDIF3_CORE_RESET          => GND_net, -- tied to '0' from definition
        SDIF0_PRDATA                   => SDIF0_PRDATA_const_net_1, -- tied to X"0" from definition
        SDIF1_PRDATA                   => SDIF1_PRDATA_const_net_1, -- tied to X"0" from definition
        SDIF2_PRDATA                   => SDIF2_PRDATA_const_net_1, -- tied to X"0" from definition
        SDIF3_PRDATA                   => SDIF3_PRDATA_const_net_1, -- tied to X"0" from definition
        -- Outputs
        MSS_HPMS_READY                 => MSS_HPMS_READY_1,
        DDR_READY                      => OPEN,
        SDIF_READY                     => OPEN,
        RESET_N_F2M                    => CoreResetP_0_RESET_N_F2M,
        M3_RESET_N                     => OPEN,
        EXT_RESET_OUT                  => OPEN,
        MDDR_DDR_AXI_S_CORE_RESET_N    => OPEN,
        FDDR_CORE_RESET_N              => CoreResetP_0_FDDR_CORE_RESET_N,
        SDIF0_CORE_RESET_N             => OPEN,
        SDIF0_0_CORE_RESET_N           => OPEN,
        SDIF0_1_CORE_RESET_N           => OPEN,
        SDIF0_PHY_RESET_N              => OPEN,
        SDIF1_CORE_RESET_N             => OPEN,
        SDIF1_PHY_RESET_N              => OPEN,
        SDIF2_CORE_RESET_N             => OPEN,
        SDIF2_PHY_RESET_N              => OPEN,
        SDIF3_CORE_RESET_N             => OPEN,
        SDIF3_PHY_RESET_N              => OPEN,
        SDIF_RELEASED                  => OPEN,
        INIT_DONE                      => CoreResetP_0_INIT_DONE 
        );
-- Eval_pfm_MSS_0
Eval_pfm_MSS_0 : Eval_pfm_MSS
    port map( 
        -- Inputs
        MCCC_CLK_BASE             => MCCC_CLK_BASE,
        MMUART_1_RXD              => MMUART_1_RXD,
        MDDR_DQS_TMATCH_0_IN      => MDDR_DQS_TMATCH_0_IN,
        MDDR_DDR_CORE_RESET_N     => CoreResetP_0_FDDR_CORE_RESET_N,
        MDDR_DDR_AHB0_S_HSEL      => CoreAHBLite_1_AHBmslave16_HSELx,
        MDDR_DDR_AHB0_S_HMASTLOCK => CoreAHBLite_1_AHBmslave16_HMASTLOCK,
        MDDR_DDR_AHB0_S_HWRITE    => CoreAHBLite_1_AHBmslave16_HWRITE,
        MDDR_DDR_AHB0_S_HREADY    => CoreAHBLite_1_AHBmslave16_HREADY,
        MCCC_CLK_BASE_PLL_LOCK    => MCCC_CLK_BASE_PLL_LOCK,
        MSS_RESET_N_F2M           => CoreResetP_0_RESET_N_F2M,
        MDDR_APB_S_PRESET_N       => CoreConfigP_0_APB_S_PRESET_N,
        MDDR_APB_S_PCLK           => CoreConfigP_0_APB_S_PCLK,
        FIC_2_APB_M_PREADY        => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PREADY,
        FIC_2_APB_M_PSLVERR       => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PSLVERR,
        MDDR_APB_S_PWRITE         => CoreConfigP_0_MDDR_APBmslave_PWRITE,
        MDDR_APB_S_PENABLE        => CoreConfigP_0_MDDR_APBmslave_PENABLE,
        MDDR_APB_S_PSEL           => CoreConfigP_0_MDDR_APBmslave_PSELx,
        FIC_0_AHB_M_HREADY        => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HREADY,
        FIC_0_AHB_M_HRESP         => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRESP_0,
        USB_ULPI_DIR              => USB_ULPI_DIR,
        USB_ULPI_NXT              => USB_ULPI_NXT,
        USB_ULPI_XCLK             => USB_ULPI_XCLK,
        MDDR_DDR_AHB0_S_HADDR     => CoreAHBLite_1_AHBmslave16_HADDR,
        MDDR_DDR_AHB0_S_HBURST    => CoreAHBLite_1_AHBmslave16_HBURST,
        MDDR_DDR_AHB0_S_HSIZE     => CoreAHBLite_1_AHBmslave16_HSIZE_0,
        MDDR_DDR_AHB0_S_HTRANS    => CoreAHBLite_1_AHBmslave16_HTRANS,
        MDDR_DDR_AHB0_S_HWDATA    => CoreAHBLite_1_AHBmslave16_HWDATA,
        FIC_2_APB_M_PRDATA        => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PRDATA,
        MDDR_APB_S_PWDATA         => CoreConfigP_0_MDDR_APBmslave_PWDATA_0,
        MDDR_APB_S_PADDR          => CoreConfigP_0_MDDR_APBmslave_PADDR_0,
        FIC_0_AHB_M_HRDATA        => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HRDATA,
        -- Outputs
        MMUART_1_TXD              => MMUART_1_TXD_net_0,
        MDDR_DQS_TMATCH_0_OUT     => MDDR_DQS_TMATCH_0_OUT_net_0,
        MDDR_CAS_N                => MDDR_CAS_N_net_0,
        MDDR_CLK                  => MDDR_CLK_net_0,
        MDDR_CLK_N                => MDDR_CLK_N_net_0,
        MDDR_CKE                  => MDDR_CKE_net_0,
        MDDR_CS_N                 => MDDR_CS_N_net_0,
        MDDR_ODT                  => MDDR_ODT_net_0,
        MDDR_RAS_N                => MDDR_RAS_N_net_0,
        MDDR_RESET_N              => MDDR_RESET_N_net_0,
        MDDR_WE_N                 => MDDR_WE_N_net_0,
        MDDR_DDR_AHB0_S_HREADYOUT => CoreAHBLite_1_AHBmslave16_HREADYOUT,
        MDDR_DDR_AHB0_S_HRESP     => CoreAHBLite_1_AHBmslave16_HRESP,
        MSS_RESET_N_M2F           => Eval_pfm_MSS_0_MSS_RESET_N_M2F,
        FIC_2_APB_M_PRESET_N      => Eval_pfm_MSS_0_FIC_2_APB_M_PRESET_N,
        FIC_2_APB_M_PCLK          => Eval_pfm_MSS_0_FIC_2_APB_M_PCLK,
        FIC_2_APB_M_PWRITE        => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PWRITE,
        FIC_2_APB_M_PENABLE       => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PENABLE,
        FIC_2_APB_M_PSEL          => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PSELx,
        MDDR_APB_S_PREADY         => CoreConfigP_0_MDDR_APBmslave_PREADY,
        MDDR_APB_S_PSLVERR        => CoreConfigP_0_MDDR_APBmslave_PSLVERR,
        FIC_0_AHB_M_HWRITE        => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HWRITE,
        USB_ULPI_STP              => USB_ULPI_STP_1,
        GPIO_29_M2F               => GPIO_29_M2F_net_0,
        MDDR_ADDR                 => MDDR_ADDR_net_0,
        MDDR_BA                   => MDDR_BA_net_0,
        MDDR_DDR_AHB0_S_HRDATA    => CoreAHBLite_1_AHBmslave16_HRDATA,
        FIC_2_APB_M_PADDR         => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PADDR,
        FIC_2_APB_M_PWDATA        => Eval_pfm_MSS_0_FIC_2_APB_MASTER_PWDATA,
        MDDR_APB_S_PRDATA         => CoreConfigP_0_MDDR_APBmslave_PRDATA,
        FIC_0_AHB_M_HADDR         => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HADDR,
        FIC_0_AHB_M_HWDATA        => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HWDATA,
        FIC_0_AHB_M_HSIZE         => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HSIZE,
        FIC_0_AHB_M_HTRANS        => Eval_pfm_MSS_0_FIC_0_AHB_MASTER_HTRANS,
        -- Inouts
        MDDR_DM_RDQS              => MDDR_DM_RDQS,
        MDDR_DQ                   => MDDR_DQ,
        MDDR_DQS                  => MDDR_DQS,
        USB_ULPI_DATA             => USB_ULPI_DATA 
        );
-- OSC_0   -   Actel:SgCore:OSC:2.0.101
OSC_0 : Eval_pfm_OSC_0_OSC
    port map( 
        -- Inputs
        XTL                => GND_net, -- tied to '0' from definition
        -- Outputs
        RCOSC_25_50MHZ_CCC => OPEN,
        RCOSC_25_50MHZ_O2F => OSC_0_RCOSC_25_50MHZ_O2F,
        RCOSC_1MHZ_CCC     => OPEN,
        RCOSC_1MHZ_O2F     => OPEN,
        XTLOSC_CCC         => OPEN,
        XTLOSC_O2F         => OPEN 
        );
-- SYSRESET_0
SYSRESET_0 : SYSRESET
    port map( 
        -- Inputs
        DEVRST_N         => DEVRST_N,
        -- Outputs
        POWER_ON_RESET_N => SYSRESET_0_POWER_ON_RESET_N 
        );

end RTL;
