set_component Eval_pfm_top_OSC_0_OSC
# Microsemi Corp.
# Date: 2017-Mar-07 01:17:33
#

create_clock -period 20 [ get_pins { I_RCOSC_25_50MHZ/CLKOUT } ]
create_clock -period 83.3333 [ get_pins { I_XTLOSC/CLKOUT } ]
