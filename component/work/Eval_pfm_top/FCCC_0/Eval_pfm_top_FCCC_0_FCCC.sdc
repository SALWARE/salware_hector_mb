set_component Eval_pfm_top_FCCC_0_FCCC
# Microsemi Corp.
# Date: 2017-Mar-07 01:17:01
#

create_clock -period 83.3333 [ get_pins { CCC_INST/XTLOSC } ]
create_clock -period 10 [ get_pins { CCC_INST/CLK0 } ]
create_generated_clock -multiply_by 83 -divide_by 12 -source [ get_pins { CCC_INST/XTLOSC } ] -phase 0 [ get_pins { CCC_INST/GL0 } ]
create_generated_clock -multiply_by 83 -divide_by 996 -source [ get_pins { CCC_INST/XTLOSC } ] -phase 0 [ get_pins { CCC_INST/GL1 } ]
