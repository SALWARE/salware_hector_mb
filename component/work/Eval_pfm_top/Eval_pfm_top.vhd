----------------------------------------------------------------------
-- Created by SmartDesign Tue Mar 07 01:17:33 2017
-- Version: v11.7 11.7.0.119
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
----------------------------------------------------------------------
-- Eval_pfm_top entity declaration
----------------------------------------------------------------------
entity Eval_pfm_top is
    -- Port list
    port(
        -- Inputs
        D1_DATA3_P            : in    std_logic_vector(0 to 0);
        D2_DATA3_P            : in    std_logic_vector(0 to 0);
        DEVRST_N              : in    std_logic;
        MDDR_DQS_TMATCH_0_IN  : in    std_logic;
        MMUART_1_RXD          : in    std_logic;
        USB_ULPI_DIR          : in    std_logic;
        USB_ULPI_NXT          : in    std_logic;
        USB_ULPI_XCLK         : in    std_logic;
        XTL                   : in    std_logic;
        -- Outputs
        D1_DATA0_N            : out   std_logic_vector(0 to 0);
        D1_DATA0_P            : out   std_logic_vector(0 to 0);
        D1_DATA3_N            : out   std_logic_vector(0 to 0);
        D1_GPIO1              : out   std_logic_vector(0 to 0);
        D1_GPIO2              : out   std_logic_vector(0 to 0);
        D2_DATA0_N            : out   std_logic_vector(0 to 0);
        D2_DATA0_P            : out   std_logic_vector(0 to 0);
        D2_DATA3_N            : out   std_logic_vector(0 to 0);
        D2_GPIO1              : out   std_logic_vector(0 to 0);
        D2_GPIO2              : out   std_logic_vector(0 to 0);
        GPIO_29_M2F           : out   std_logic;
        LED                   : out   std_logic_vector(0 to 0);
        MDDR_ADDR             : out   std_logic_vector(15 downto 0);
        MDDR_BA               : out   std_logic_vector(2 downto 0);
        MDDR_CAS_N            : out   std_logic;
        MDDR_CKE              : out   std_logic;
        MDDR_CLK              : out   std_logic;
        MDDR_CLK_N            : out   std_logic;
        MDDR_CS_N             : out   std_logic;
        MDDR_DQS_TMATCH_0_OUT : out   std_logic;
        MDDR_ODT              : out   std_logic;
        MDDR_RAS_N            : out   std_logic;
        MDDR_RESET_N          : out   std_logic;
        MDDR_WE_N             : out   std_logic;
        MMUART_1_TXD          : out   std_logic;
        USB_ULPI_STP          : out   std_logic;
        -- Inouts
        MDDR_DM_RDQS          : inout std_logic_vector(1 downto 0);
        MDDR_DQ               : inout std_logic_vector(15 downto 0);
        MDDR_DQS              : inout std_logic_vector(1 downto 0);
        USB_ULPI_DATA         : inout std_logic_vector(7 downto 0)
        );
end Eval_pfm_top;
----------------------------------------------------------------------
-- Eval_pfm_top architecture body
----------------------------------------------------------------------
architecture RTL of Eval_pfm_top is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- ctrl
-- using entity instantiation for component ctrl
-- Eval_pfm
component Eval_pfm
    -- Port list
    port(
        -- Inputs
        DEVRST_N               : in    std_logic;
        HADDR_M0               : in    std_logic_vector(31 downto 0);
        HBURST_M0              : in    std_logic_vector(2 downto 0);
        HMASTLOCK_M0           : in    std_logic;
        HPROT_M0               : in    std_logic_vector(3 downto 0);
        HRDATA_S0              : in    std_logic_vector(31 downto 0);
        HREADYOUT_S0           : in    std_logic;
        HRESP_S0               : in    std_logic_vector(1 downto 0);
        HSIZE_M0               : in    std_logic_vector(2 downto 0);
        HTRANS_M0              : in    std_logic_vector(1 downto 0);
        HWDATA_M0              : in    std_logic_vector(31 downto 0);
        HWRITE_M0              : in    std_logic;
        MCCC_CLK_BASE          : in    std_logic;
        MCCC_CLK_BASE_PLL_LOCK : in    std_logic;
        MDDR_DQS_TMATCH_0_IN   : in    std_logic;
        MMUART_1_RXD           : in    std_logic;
        USB_ULPI_DIR           : in    std_logic;
        USB_ULPI_NXT           : in    std_logic;
        USB_ULPI_XCLK          : in    std_logic;
        -- Outputs
        GPIO_29_M2F            : out   std_logic;
        HADDR_S0               : out   std_logic_vector(31 downto 0);
        HBURST_S0              : out   std_logic_vector(2 downto 0);
        HMASTLOCK_S0           : out   std_logic;
        HPROT_S0               : out   std_logic_vector(3 downto 0);
        HRDATA_M0              : out   std_logic_vector(31 downto 0);
        HREADY_M0              : out   std_logic;
        HREADY_S0              : out   std_logic;
        HRESP_M0               : out   std_logic_vector(1 downto 0);
        HSEL_S0                : out   std_logic;
        HSIZE_S0               : out   std_logic_vector(2 downto 0);
        HTRANS_S0              : out   std_logic_vector(1 downto 0);
        HWDATA_S0              : out   std_logic_vector(31 downto 0);
        HWRITE_S0              : out   std_logic;
        MDDR_ADDR              : out   std_logic_vector(15 downto 0);
        MDDR_BA                : out   std_logic_vector(2 downto 0);
        MDDR_CAS_N             : out   std_logic;
        MDDR_CKE               : out   std_logic;
        MDDR_CLK               : out   std_logic;
        MDDR_CLK_N             : out   std_logic;
        MDDR_CS_N              : out   std_logic;
        MDDR_DQS_TMATCH_0_OUT  : out   std_logic;
        MDDR_ODT               : out   std_logic;
        MDDR_RAS_N             : out   std_logic;
        MDDR_RESET_N           : out   std_logic;
        MDDR_WE_N              : out   std_logic;
        MMUART_1_TXD           : out   std_logic;
        MSS_HPMS_READY         : out   std_logic;
        USB_ULPI_STP           : out   std_logic;
        -- Inouts
        MDDR_DM_RDQS           : inout std_logic_vector(1 downto 0);
        MDDR_DQ                : inout std_logic_vector(15 downto 0);
        MDDR_DQS               : inout std_logic_vector(1 downto 0);
        USB_ULPI_DATA          : inout std_logic_vector(7 downto 0)
        );
end component;
-- Eval_pfm_top_FCCC_0_FCCC   -   Actel:SgCore:FCCC:2.0.200
component Eval_pfm_top_FCCC_0_FCCC
    -- Port list
    port(
        -- Inputs
        XTLOSC : in  std_logic;
        -- Outputs
        GL0    : out std_logic;
        GL1    : out std_logic;
        LOCK   : out std_logic
        );
end component;
-- Eval_pfm_top_IO_0_IO   -   Actel:SgCore:IO:1.0.101
component Eval_pfm_top_IO_0_IO
    -- Port list
    port(
        -- Inputs
        D        : in  std_logic_vector(0 to 0);
        -- Outputs
        PADN_OUT : out std_logic_vector(0 to 0);
        PADP_OUT : out std_logic_vector(0 to 0)
        );
end component;
-- Eval_pfm_top_IO_1_IO   -   Actel:SgCore:IO:1.0.101
component Eval_pfm_top_IO_1_IO
    -- Port list
    port(
        -- Inputs
        D        : in  std_logic_vector(0 to 0);
        -- Outputs
        PADN_OUT : out std_logic_vector(0 to 0);
        PADP_OUT : out std_logic_vector(0 to 0)
        );
end component;
-- Eval_pfm_top_IO_2_IO   -   Actel:SgCore:IO:1.0.101
component Eval_pfm_top_IO_2_IO
    -- Port list
    port(
        -- Inputs
        D       : in  std_logic_vector(0 to 0);
        -- Outputs
        PAD_OUT : out std_logic_vector(0 to 0)
        );
end component;
-- Eval_pfm_top_IO_3_IO   -   Actel:SgCore:IO:1.0.101
component Eval_pfm_top_IO_3_IO
    -- Port list
    port(
        -- Inputs
        D       : in  std_logic_vector(0 to 0);
        -- Outputs
        PAD_OUT : out std_logic_vector(0 to 0)
        );
end component;
-- Eval_pfm_top_IO_5_IO   -   Actel:SgCore:IO:1.0.101
component Eval_pfm_top_IO_5_IO
    -- Port list
    port(
        -- Inputs
        D       : in  std_logic_vector(0 to 0);
        -- Outputs
        PAD_OUT : out std_logic_vector(0 to 0)
        );
end component;
-- Eval_pfm_top_IO_6_IO   -   Actel:SgCore:IO:1.0.101
component Eval_pfm_top_IO_6_IO
    -- Port list
    port(
        -- Inputs
        D       : in  std_logic_vector(0 to 0);
        -- Outputs
        PAD_OUT : out std_logic_vector(0 to 0)
        );
end component;
-- Eval_pfm_top_IO_10_IO   -   Actel:SgCore:IO:1.0.101
component Eval_pfm_top_IO_10_IO
    -- Port list
    port(
        -- Inputs
        D       : in  std_logic_vector(0 to 0);
        -- Outputs
        PAD_OUT : out std_logic_vector(0 to 0)
        );
end component;
-- Eval_pfm_top_IO_13_IO   -   Actel:SgCore:IO:1.0.101
component Eval_pfm_top_IO_13_IO
    -- Port list
    port(
        -- Inputs
        PAD_IN : in  std_logic_vector(0 to 0);
        -- Outputs
        Y      : out std_logic_vector(0 to 0)
        );
end component;
-- Eval_pfm_top_IO_14_IO   -   Actel:SgCore:IO:1.0.101
component Eval_pfm_top_IO_14_IO
    -- Port list
    port(
        -- Inputs
        PAD_IN : in  std_logic_vector(0 to 0);
        -- Outputs
        Y      : out std_logic_vector(0 to 0)
        );
end component;
-- Eval_pfm_top_IO_15_IO   -   Actel:SgCore:IO:1.0.101
component Eval_pfm_top_IO_15_IO
    -- Port list
    port(
        -- Inputs
        D       : in  std_logic_vector(0 to 0);
        -- Outputs
        PAD_OUT : out std_logic_vector(0 to 0)
        );
end component;
-- Eval_pfm_top_IO_16_IO   -   Actel:SgCore:IO:1.0.101
component Eval_pfm_top_IO_16_IO
    -- Port list
    port(
        -- Inputs
        D       : in  std_logic_vector(0 to 0);
        -- Outputs
        PAD_OUT : out std_logic_vector(0 to 0)
        );
end component;
-- mux
component mux
    -- Port list
    port(
        -- Inputs
        db_ssi_rx_1 : in  std_logic;
        db_ssi_rx_2 : in  std_logic;
        set_mux     : in  std_logic;
        -- Outputs
        db_ssi_rx   : out std_logic
        );
end component;
-- Eval_pfm_top_OSC_0_OSC   -   Actel:SgCore:OSC:2.0.101
component Eval_pfm_top_OSC_0_OSC
    -- Port list
    port(
        -- Inputs
        XTL                : in  std_logic;
        -- Outputs
        RCOSC_1MHZ_CCC     : out std_logic;
        RCOSC_1MHZ_O2F     : out std_logic;
        RCOSC_25_50MHZ_CCC : out std_logic;
        RCOSC_25_50MHZ_O2F : out std_logic;
        XTLOSC_CCC         : out std_logic;
        XTLOSC_O2F         : out std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal ctrl_0_BIF_2_HADDR              : std_logic_vector(31 downto 0);
signal ctrl_0_BIF_2_HBURST             : std_logic_vector(2 downto 0);
signal ctrl_0_BIF_2_HBUSREQ            : std_logic;
signal ctrl_0_BIF_2_HLOCK              : std_logic;
signal ctrl_0_BIF_2_HRDATA             : std_logic_vector(31 downto 0);
signal ctrl_0_BIF_2_HREADY             : std_logic;
signal ctrl_0_BIF_2_HTRANS             : std_logic_vector(1 downto 0);
signal ctrl_0_BIF_2_HWDATA             : std_logic_vector(31 downto 0);
signal ctrl_0_BIF_2_HWRITE             : std_logic;
signal ctrl_0_db_mux_out               : std_logic;
signal ctrl_0_db_nrst                  : std_logic;
signal ctrl_0_led_out                  : std_logic;
signal ctrl_0_ssi_tx                   : std_logic;
signal D1_DATA0_N_net_0                : std_logic_vector(0 to 0);
signal D1_DATA0_P_net_0                : std_logic_vector(0 to 0);
signal D1_DATA3_N_net_0                : std_logic_vector(0 to 0);
signal D1_GPIO1_net_0                  : std_logic_vector(0 to 0);
signal D1_GPIO2_net_0                  : std_logic_vector(0 to 0);
signal D2_DATA0_N_net_0                : std_logic_vector(0 to 0);
signal D2_DATA0_P_net_0                : std_logic_vector(0 to 0);
signal D2_DATA3_N_net_0                : std_logic_vector(0 to 0);
signal D2_GPIO1_net_0                  : std_logic_vector(0 to 0);
signal D2_GPIO2_net_0                  : std_logic_vector(0 to 0);
signal Eval_pfm_0_AHBmslave0_HADDR     : std_logic_vector(31 downto 0);
signal Eval_pfm_0_AHBmslave0_HBURST    : std_logic_vector(2 downto 0);
signal Eval_pfm_0_AHBmslave0_HMASTLOCK : std_logic;
signal Eval_pfm_0_AHBmslave0_HPROT     : std_logic_vector(3 downto 0);
signal Eval_pfm_0_AHBmslave0_HRDATA    : std_logic_vector(31 downto 0);
signal Eval_pfm_0_AHBmslave0_HREADY    : std_logic;
signal Eval_pfm_0_AHBmslave0_HREADYOUT : std_logic;
signal Eval_pfm_0_AHBmslave0_HSELx     : std_logic;
signal Eval_pfm_0_AHBmslave0_HSIZE     : std_logic_vector(2 downto 0);
signal Eval_pfm_0_AHBmslave0_HTRANS    : std_logic_vector(1 downto 0);
signal Eval_pfm_0_AHBmslave0_HWDATA    : std_logic_vector(31 downto 0);
signal Eval_pfm_0_AHBmslave0_HWRITE    : std_logic;
signal FCCC_0_GL0                      : std_logic;
signal FCCC_0_GL1_0                    : std_logic;
signal FCCC_0_LOCK                     : std_logic;
signal GPIO_29_M2F_net_0               : std_logic;
signal IO_13_Y                         : std_logic_vector(0 to 0);
signal IO_14_Y                         : std_logic_vector(0 to 0);
signal LED_net_0                       : std_logic_vector(0 to 0);
signal MDDR_ADDR_net_0                 : std_logic_vector(15 downto 0);
signal MDDR_BA_net_0                   : std_logic_vector(2 downto 0);
signal MDDR_CAS_N_net_0                : std_logic;
signal MDDR_CKE_net_0                  : std_logic;
signal MDDR_CLK_net_0                  : std_logic;
signal MDDR_CLK_N_net_0                : std_logic;
signal MDDR_CS_N_net_0                 : std_logic;
signal MDDR_DQS_TMATCH_0_OUT_net_0     : std_logic;
signal MDDR_ODT_net_0                  : std_logic;
signal MDDR_RAS_N_net_0                : std_logic;
signal MDDR_RESET_N_net_0              : std_logic;
signal MDDR_WE_N_net_0                 : std_logic;
signal MMUART_1_TXD_net_0              : std_logic;
signal mux_0_db_ssi_rx                 : std_logic;
signal OSC_0_XTLOSC_CCC_OUT_XTLOSC_CCC : std_logic;
signal USB_ULPI_STP_net_0              : std_logic;
signal MMUART_1_TXD_net_1              : std_logic;
signal MDDR_DQS_TMATCH_0_OUT_net_1     : std_logic;
signal MDDR_CAS_N_net_1                : std_logic;
signal MDDR_CLK_net_1                  : std_logic;
signal MDDR_CLK_N_net_1                : std_logic;
signal MDDR_CKE_net_1                  : std_logic;
signal MDDR_CS_N_net_1                 : std_logic;
signal MDDR_ODT_net_1                  : std_logic;
signal MDDR_RAS_N_net_1                : std_logic;
signal MDDR_RESET_N_net_1              : std_logic;
signal MDDR_WE_N_net_1                 : std_logic;
signal USB_ULPI_STP_net_1              : std_logic;
signal GPIO_29_M2F_net_1               : std_logic;
signal MDDR_ADDR_net_1                 : std_logic_vector(15 downto 0);
signal MDDR_BA_net_1                   : std_logic_vector(2 downto 0);
signal D1_GPIO2_net_1                  : std_logic_vector(0 to 0);
signal D1_GPIO1_net_1                  : std_logic_vector(0 to 0);
signal D2_GPIO2_net_1                  : std_logic_vector(0 to 0);
signal D2_GPIO1_net_1                  : std_logic_vector(0 to 0);
signal LED_net_1                       : std_logic_vector(0 to 0);
signal D1_DATA3_N_net_1                : std_logic_vector(0 to 0);
signal D2_DATA3_N_net_1                : std_logic_vector(0 to 0);
signal D2_DATA0_P_net_1                : std_logic_vector(0 to 0);
signal D2_DATA0_N_net_1                : std_logic_vector(0 to 0);
signal D1_DATA0_P_net_1                : std_logic_vector(0 to 0);
signal D1_DATA0_N_net_1                : std_logic_vector(0 to 0);
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal GND_net                         : std_logic;
signal PADDR_const_net_0               : std_logic_vector(7 downto 2);
signal PWDATA_const_net_0              : std_logic_vector(7 downto 0);
signal HPROT_M0_const_net_0            : std_logic_vector(3 downto 0);
signal HRESP_S0_const_net_0            : std_logic_vector(1 downto 0);
----------------------------------------------------------------------
-- Bus Interface Nets Declarations - Unequal Pin Widths
----------------------------------------------------------------------
signal ctrl_0_BIF_2_HRESP              : std_logic_vector(1 downto 0);
signal ctrl_0_BIF_2_HRESP_0_0to0       : std_logic_vector(0 to 0);
signal ctrl_0_BIF_2_HRESP_0            : std_logic;

signal ctrl_0_BIF_2_HSIZE_0_2to2       : std_logic_vector(2 to 2);
signal ctrl_0_BIF_2_HSIZE_0_1to0       : std_logic_vector(1 downto 0);
signal ctrl_0_BIF_2_HSIZE_0            : std_logic_vector(2 downto 0);
signal ctrl_0_BIF_2_HSIZE              : std_logic_vector(1 downto 0);


begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 GND_net              <= '0';
 PADDR_const_net_0    <= B"000000";
 PWDATA_const_net_0   <= B"00000000";
 HPROT_M0_const_net_0 <= B"0000";
 HRESP_S0_const_net_0 <= B"00";
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 MMUART_1_TXD_net_1          <= MMUART_1_TXD_net_0;
 MMUART_1_TXD                <= MMUART_1_TXD_net_1;
 MDDR_DQS_TMATCH_0_OUT_net_1 <= MDDR_DQS_TMATCH_0_OUT_net_0;
 MDDR_DQS_TMATCH_0_OUT       <= MDDR_DQS_TMATCH_0_OUT_net_1;
 MDDR_CAS_N_net_1            <= MDDR_CAS_N_net_0;
 MDDR_CAS_N                  <= MDDR_CAS_N_net_1;
 MDDR_CLK_net_1              <= MDDR_CLK_net_0;
 MDDR_CLK                    <= MDDR_CLK_net_1;
 MDDR_CLK_N_net_1            <= MDDR_CLK_N_net_0;
 MDDR_CLK_N                  <= MDDR_CLK_N_net_1;
 MDDR_CKE_net_1              <= MDDR_CKE_net_0;
 MDDR_CKE                    <= MDDR_CKE_net_1;
 MDDR_CS_N_net_1             <= MDDR_CS_N_net_0;
 MDDR_CS_N                   <= MDDR_CS_N_net_1;
 MDDR_ODT_net_1              <= MDDR_ODT_net_0;
 MDDR_ODT                    <= MDDR_ODT_net_1;
 MDDR_RAS_N_net_1            <= MDDR_RAS_N_net_0;
 MDDR_RAS_N                  <= MDDR_RAS_N_net_1;
 MDDR_RESET_N_net_1          <= MDDR_RESET_N_net_0;
 MDDR_RESET_N                <= MDDR_RESET_N_net_1;
 MDDR_WE_N_net_1             <= MDDR_WE_N_net_0;
 MDDR_WE_N                   <= MDDR_WE_N_net_1;
 USB_ULPI_STP_net_1          <= USB_ULPI_STP_net_0;
 USB_ULPI_STP                <= USB_ULPI_STP_net_1;
 GPIO_29_M2F_net_1           <= GPIO_29_M2F_net_0;
 GPIO_29_M2F                 <= GPIO_29_M2F_net_1;
 MDDR_ADDR_net_1             <= MDDR_ADDR_net_0;
 MDDR_ADDR(15 downto 0)      <= MDDR_ADDR_net_1;
 MDDR_BA_net_1               <= MDDR_BA_net_0;
 MDDR_BA(2 downto 0)         <= MDDR_BA_net_1;
 D1_GPIO2_net_1(0)           <= D1_GPIO2_net_0(0);
 D1_GPIO2(0)                 <= D1_GPIO2_net_1(0);
 D1_GPIO1_net_1(0)           <= D1_GPIO1_net_0(0);
 D1_GPIO1(0)                 <= D1_GPIO1_net_1(0);
 D2_GPIO2_net_1(0)           <= D2_GPIO2_net_0(0);
 D2_GPIO2(0)                 <= D2_GPIO2_net_1(0);
 D2_GPIO1_net_1(0)           <= D2_GPIO1_net_0(0);
 D2_GPIO1(0)                 <= D2_GPIO1_net_1(0);
 LED_net_1(0)                <= LED_net_0(0);
 LED(0)                      <= LED_net_1(0);
 D1_DATA3_N_net_1(0)         <= D1_DATA3_N_net_0(0);
 D1_DATA3_N(0)               <= D1_DATA3_N_net_1(0);
 D2_DATA3_N_net_1(0)         <= D2_DATA3_N_net_0(0);
 D2_DATA3_N(0)               <= D2_DATA3_N_net_1(0);
 D2_DATA0_P_net_1(0)         <= D2_DATA0_P_net_0(0);
 D2_DATA0_P(0)               <= D2_DATA0_P_net_1(0);
 D2_DATA0_N_net_1(0)         <= D2_DATA0_N_net_0(0);
 D2_DATA0_N(0)               <= D2_DATA0_N_net_1(0);
 D1_DATA0_P_net_1(0)         <= D1_DATA0_P_net_0(0);
 D1_DATA0_P(0)               <= D1_DATA0_P_net_1(0);
 D1_DATA0_N_net_1(0)         <= D1_DATA0_N_net_0(0);
 D1_DATA0_N(0)               <= D1_DATA0_N_net_1(0);
----------------------------------------------------------------------
-- Bus Interface Nets Assignments - Unequal Pin Widths
----------------------------------------------------------------------
 ctrl_0_BIF_2_HRESP_0_0to0(0) <= ctrl_0_BIF_2_HRESP(0);
 ctrl_0_BIF_2_HRESP_0 <= ( ctrl_0_BIF_2_HRESP_0_0to0(0) );

 ctrl_0_BIF_2_HSIZE_0_2to2(2) <= '0';
 ctrl_0_BIF_2_HSIZE_0_1to0(1 downto 0) <= ctrl_0_BIF_2_HSIZE(1 downto 0);
 ctrl_0_BIF_2_HSIZE_0 <= ( ctrl_0_BIF_2_HSIZE_0_2to2(2) & ctrl_0_BIF_2_HSIZE_0_1to0(1 downto 0) );

----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- ctrl_0
ctrl_0 : entity work.ctrl
    port map( 
        -- Inputs
        hreadyout  => ctrl_0_BIF_2_HREADY,
        hresp      => ctrl_0_BIF_2_HRESP_0,
        hrdata     => ctrl_0_BIF_2_HRDATA,
        haddr_mss  => Eval_pfm_0_AHBmslave0_HADDR,
        hwdata_mss => Eval_pfm_0_AHBmslave0_HWDATA,
        hsel_mss   => Eval_pfm_0_AHBmslave0_HSELx,
        hwrite_mss => Eval_pfm_0_AHBmslave0_HWRITE,
        hrdy_mss   => Eval_pfm_0_AHBmslave0_HREADY,
        n_rst      => FCCC_0_LOCK,
        clk        => FCCC_0_GL0,
        ssi_clk    => FCCC_0_GL1_0,
        ssi_rx     => mux_0_db_ssi_rx,
        -- Outputs
        htrans     => ctrl_0_BIF_2_HTRANS,
        hsize      => ctrl_0_BIF_2_HSIZE,
        hwrite     => ctrl_0_BIF_2_HWRITE,
        hwdata     => ctrl_0_BIF_2_HWDATA,
        haddr      => ctrl_0_BIF_2_HADDR,
        hmastlock  => ctrl_0_BIF_2_HLOCK,
        hsel       => ctrl_0_BIF_2_HBUSREQ,
        hburst     => ctrl_0_BIF_2_HBURST,
        hrdata_mss => Eval_pfm_0_AHBmslave0_HRDATA,
        hready_mss => Eval_pfm_0_AHBmslave0_HREADYOUT,
        ssi_tx     => ctrl_0_ssi_tx,
        db_mux_out => ctrl_0_db_mux_out,
        db_nrst    => ctrl_0_db_nrst,
        led_out    => ctrl_0_led_out 
        );
-- Eval_pfm_0
Eval_pfm_0 : Eval_pfm
    port map( 
        -- Inputs
        MMUART_1_RXD           => MMUART_1_RXD,
        MDDR_DQS_TMATCH_0_IN   => MDDR_DQS_TMATCH_0_IN,
        USB_ULPI_DIR           => USB_ULPI_DIR,
        USB_ULPI_NXT           => USB_ULPI_NXT,
        USB_ULPI_XCLK          => USB_ULPI_XCLK,
        HWRITE_M0              => ctrl_0_BIF_2_HWRITE,
        HMASTLOCK_M0           => ctrl_0_BIF_2_HLOCK,
        HREADYOUT_S0           => Eval_pfm_0_AHBmslave0_HREADYOUT,
        MCCC_CLK_BASE          => FCCC_0_GL0,
        MCCC_CLK_BASE_PLL_LOCK => FCCC_0_LOCK,
        DEVRST_N               => DEVRST_N,
        HADDR_M0               => ctrl_0_BIF_2_HADDR,
        HTRANS_M0              => ctrl_0_BIF_2_HTRANS,
        HSIZE_M0               => ctrl_0_BIF_2_HSIZE_0,
        HBURST_M0              => ctrl_0_BIF_2_HBURST,
        HPROT_M0               => HPROT_M0_const_net_0, -- tied to X"0" from definition
        HWDATA_M0              => ctrl_0_BIF_2_HWDATA,
        HRDATA_S0              => Eval_pfm_0_AHBmslave0_HRDATA,
        HRESP_S0               => HRESP_S0_const_net_0, -- tied to X"0" from definition
        -- Outputs
        MMUART_1_TXD           => MMUART_1_TXD_net_0,
        MDDR_DQS_TMATCH_0_OUT  => MDDR_DQS_TMATCH_0_OUT_net_0,
        MDDR_CAS_N             => MDDR_CAS_N_net_0,
        MDDR_CLK               => MDDR_CLK_net_0,
        MDDR_CLK_N             => MDDR_CLK_N_net_0,
        MDDR_CKE               => MDDR_CKE_net_0,
        MDDR_CS_N              => MDDR_CS_N_net_0,
        MDDR_ODT               => MDDR_ODT_net_0,
        MDDR_RAS_N             => MDDR_RAS_N_net_0,
        MDDR_RESET_N           => MDDR_RESET_N_net_0,
        MDDR_WE_N              => MDDR_WE_N_net_0,
        USB_ULPI_STP           => USB_ULPI_STP_net_0,
        GPIO_29_M2F            => GPIO_29_M2F_net_0,
        HREADY_M0              => ctrl_0_BIF_2_HREADY,
        HWRITE_S0              => Eval_pfm_0_AHBmslave0_HWRITE,
        HSEL_S0                => Eval_pfm_0_AHBmslave0_HSELx,
        HREADY_S0              => Eval_pfm_0_AHBmslave0_HREADY,
        HMASTLOCK_S0           => Eval_pfm_0_AHBmslave0_HMASTLOCK,
        MSS_HPMS_READY         => OPEN,
        MDDR_ADDR              => MDDR_ADDR_net_0,
        MDDR_BA                => MDDR_BA_net_0,
        HRDATA_M0              => ctrl_0_BIF_2_HRDATA,
        HRESP_M0               => ctrl_0_BIF_2_HRESP,
        HADDR_S0               => Eval_pfm_0_AHBmslave0_HADDR,
        HTRANS_S0              => Eval_pfm_0_AHBmslave0_HTRANS,
        HSIZE_S0               => Eval_pfm_0_AHBmslave0_HSIZE,
        HWDATA_S0              => Eval_pfm_0_AHBmslave0_HWDATA,
        HBURST_S0              => Eval_pfm_0_AHBmslave0_HBURST,
        HPROT_S0               => Eval_pfm_0_AHBmslave0_HPROT,
        -- Inouts
        MDDR_DM_RDQS           => MDDR_DM_RDQS,
        MDDR_DQ                => MDDR_DQ,
        MDDR_DQS               => MDDR_DQS,
        USB_ULPI_DATA          => USB_ULPI_DATA 
        );
-- FCCC_0   -   Actel:SgCore:FCCC:2.0.200
FCCC_0 : Eval_pfm_top_FCCC_0_FCCC
    port map( 
        -- Inputs
        XTLOSC => OSC_0_XTLOSC_CCC_OUT_XTLOSC_CCC,
        -- Outputs
        GL0    => FCCC_0_GL0,
        GL1    => FCCC_0_GL1_0,
        LOCK   => FCCC_0_LOCK 
        );
-- IO_0   -   Actel:SgCore:IO:1.0.101
IO_0 : Eval_pfm_top_IO_0_IO
    port map( 
        -- Inputs
        D(0)     => FCCC_0_GL0,
        -- Outputs
        PADP_OUT => D2_DATA0_P_net_0,
        PADN_OUT => D2_DATA0_N_net_0 
        );
-- IO_1   -   Actel:SgCore:IO:1.0.101
IO_1 : Eval_pfm_top_IO_1_IO
    port map( 
        -- Inputs
        D(0)     => FCCC_0_GL0,
        -- Outputs
        PADP_OUT => D1_DATA0_P_net_0,
        PADN_OUT => D1_DATA0_N_net_0 
        );
-- IO_2   -   Actel:SgCore:IO:1.0.101
IO_2 : Eval_pfm_top_IO_2_IO
    port map( 
        -- Inputs
        D(0)    => ctrl_0_db_nrst,
        -- Outputs
        PAD_OUT => D1_GPIO2_net_0 
        );
-- IO_3   -   Actel:SgCore:IO:1.0.101
IO_3 : Eval_pfm_top_IO_3_IO
    port map( 
        -- Inputs
        D(0)    => FCCC_0_GL1_0,
        -- Outputs
        PAD_OUT => D1_GPIO1_net_0 
        );
-- IO_5   -   Actel:SgCore:IO:1.0.101
IO_5 : Eval_pfm_top_IO_5_IO
    port map( 
        -- Inputs
        D(0)    => ctrl_0_db_nrst,
        -- Outputs
        PAD_OUT => D2_GPIO2_net_0 
        );
-- IO_6   -   Actel:SgCore:IO:1.0.101
IO_6 : Eval_pfm_top_IO_6_IO
    port map( 
        -- Inputs
        D(0)    => FCCC_0_GL1_0,
        -- Outputs
        PAD_OUT => D2_GPIO1_net_0 
        );
-- IO_10   -   Actel:SgCore:IO:1.0.101
IO_10 : Eval_pfm_top_IO_10_IO
    port map( 
        -- Inputs
        D(0)    => ctrl_0_led_out,
        -- Outputs
        PAD_OUT => LED_net_0 
        );
-- IO_13   -   Actel:SgCore:IO:1.0.101
IO_13 : Eval_pfm_top_IO_13_IO
    port map( 
        -- Inputs
        PAD_IN => D1_DATA3_P,
        -- Outputs
        Y      => IO_13_Y 
        );
-- IO_14   -   Actel:SgCore:IO:1.0.101
IO_14 : Eval_pfm_top_IO_14_IO
    port map( 
        -- Inputs
        PAD_IN => D2_DATA3_P,
        -- Outputs
        Y      => IO_14_Y 
        );
-- IO_15   -   Actel:SgCore:IO:1.0.101
IO_15 : Eval_pfm_top_IO_15_IO
    port map( 
        -- Inputs
        D(0)    => ctrl_0_ssi_tx,
        -- Outputs
        PAD_OUT => D1_DATA3_N_net_0 
        );
-- IO_16   -   Actel:SgCore:IO:1.0.101
IO_16 : Eval_pfm_top_IO_16_IO
    port map( 
        -- Inputs
        D(0)    => ctrl_0_ssi_tx,
        -- Outputs
        PAD_OUT => D2_DATA3_N_net_0 
        );
-- mux_0
mux_0 : mux
    port map( 
        -- Inputs
        db_ssi_rx_1 => IO_13_Y(0),
        db_ssi_rx_2 => IO_14_Y(0),
        set_mux     => ctrl_0_db_mux_out,
        -- Outputs
        db_ssi_rx   => mux_0_db_ssi_rx 
        );
-- OSC_0   -   Actel:SgCore:OSC:2.0.101
OSC_0 : Eval_pfm_top_OSC_0_OSC
    port map( 
        -- Inputs
        XTL                => XTL,
        -- Outputs
        RCOSC_25_50MHZ_CCC => OPEN,
        RCOSC_25_50MHZ_O2F => OPEN,
        RCOSC_1MHZ_CCC     => OPEN,
        RCOSC_1MHZ_O2F     => OPEN,
        XTLOSC_CCC         => OSC_0_XTLOSC_CCC_OUT_XTLOSC_CCC,
        XTLOSC_O2F         => OPEN 
        );

end RTL;
