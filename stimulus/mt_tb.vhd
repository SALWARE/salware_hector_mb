--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: mt_tb.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::SmartFusion2> <Die::M2S025> <Package::484 FBGA>
-- Author: <Name>
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity mt_tb is
end mt_tb;

architecture behavioral of mt_tb is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';

    component Eval_pfm
        -- ports
        port( 
            -- Inputs
            MMUART_1_RXD : in std_logic;
            MDDR_DQS_TMATCH_0_IN : in std_logic;
            XTL : in std_logic;
            DEVRST_N : in std_logic;
            GPIO_2_F2M : in std_logic;
            RESET_N : in std_logic;

            -- Outputs
            MMUART_1_TXD : out std_logic;
            MDDR_DQS_TMATCH_0_OUT : out std_logic;
            MDDR_CAS_N : out std_logic;
            MDDR_CLK : out std_logic;
            MDDR_CLK_N : out std_logic;
            MDDR_CKE : out std_logic;
            MDDR_CS_N : out std_logic;
            MDDR_ODT : out std_logic;
            MDDR_RAS_N : out std_logic;
            MDDR_RESET_N : out std_logic;
            MDDR_WE_N : out std_logic;
            INT1 : out std_logic;
            MDDR_ADDR : out std_logic_vector(15 downto 0);
            MDDR_BA : out std_logic_vector(2 downto 0);

            -- Inouts
            MDDR_DM_RDQS : inout std_logic_vector(1 downto 0);
            MDDR_DQ : inout std_logic_vector(15 downto 0);
            MDDR_DQS : inout std_logic_vector(1 downto 0)

        );
    end component;

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '0';
            wait for ( SYSCLK_PERIOD * 10 );
            
            NSYSRESET <= '1';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  Eval_pfm
    Eval_pfm_0 : Eval_pfm
        -- port map
        port map( 
            -- Inputs
            MMUART_1_RXD => '0',
            MDDR_DQS_TMATCH_0_IN => NSYSRESET,
            XTL => SYSCLK,
            DEVRST_N => NSYSRESET,
            GPIO_2_F2M => '0',
            RESET_N => NSYSRESET,

            -- Outputs
            MMUART_1_TXD =>  open,
            MDDR_DQS_TMATCH_0_OUT =>  open,
            MDDR_CAS_N =>  open,
            MDDR_CLK =>  open,
            MDDR_CLK_N =>  open,
            MDDR_CKE =>  open,
            MDDR_CS_N =>  open,
            MDDR_ODT =>  open,
            MDDR_RAS_N =>  open,
            MDDR_RESET_N =>  open,
            MDDR_WE_N =>  open,
            INT1 =>  open,
            MDDR_ADDR => open,
            MDDR_BA => open,

            -- Inouts
            MDDR_DM_RDQS => open,
            MDDR_DQ => open,
            MDDR_DQS => open

        );

end behavioral;

