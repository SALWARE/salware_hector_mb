--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: my_tb.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::SmartFusion2> <Die::M2S025> <Package::484 FBGA>
-- Author: <Name>
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity my_tb is
end my_tb;

architecture behavioral of my_tb is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';

    component Eval_pfm_top
        -- ports
        port( 
            -- Inputs
            MMUART_1_RXD : in std_logic;
            MDDR_DQS_TMATCH_0_IN : in std_logic;
            USB_ULPI_DIR : in std_logic;
            USB_ULPI_NXT : in std_logic;
            USB_ULPI_XCLK : in std_logic;
            DEVRST_N : in std_logic;
            XTL : in std_logic;
            CLKP_IN1 : in std_logic_vector(0 to 0);
            CLKN_IN1 : in std_logic_vector(0 to 0);
            DATP_IN1 : in std_logic_vector(0 to 0);
            DATN_IN1 : in std_logic_vector(0 to 0);
            DATP_IN2 : in std_logic_vector(0 to 0);
            DATN_IN2 : in std_logic_vector(0 to 0);
            CLKP_IN2 : in std_logic_vector(0 to 0);
            CLKN_IN2 : in std_logic_vector(0 to 0);
            SYNP_IN1 : in std_logic_vector(0 to 0);
            SYNN_IN1 : in std_logic_vector(0 to 0);
            SYNP_IN2 : in std_logic_vector(0 to 0);
            SYNN_IN2 : in std_logic_vector(0 to 0);

            -- Outputs
            MMUART_1_TXD : out std_logic;
            MDDR_DQS_TMATCH_0_OUT : out std_logic;
            MDDR_CAS_N : out std_logic;
            MDDR_CLK : out std_logic;
            MDDR_CLK_N : out std_logic;
            MDDR_CKE : out std_logic;
            MDDR_CS_N : out std_logic;
            MDDR_ODT : out std_logic;
            MDDR_RAS_N : out std_logic;
            MDDR_RESET_N : out std_logic;
            MDDR_WE_N : out std_logic;
            USB_ULPI_STP : out std_logic;
            GPIO_29_M2F : out std_logic;
            MDDR_ADDR : out std_logic_vector(15 downto 0);
            MDDR_BA : out std_logic_vector(2 downto 0);
            GPIO2_1 : out std_logic_vector(0 to 0);
            GPIO1_1 : out std_logic_vector(0 to 0);
            GPIO0_1 : out std_logic_vector(0 to 0);
            GPIO2_2 : out std_logic_vector(0 to 0);
            GPIO1_2 : out std_logic_vector(0 to 0);
            GPIO0_2 : out std_logic_vector(0 to 0);
            LED : out std_logic_vector(0 to 0);

            -- Inouts
            MDDR_DM_RDQS : inout std_logic_vector(1 downto 0);
            MDDR_DQ : inout std_logic_vector(15 downto 0);
            MDDR_DQS : inout std_logic_vector(1 downto 0);
            USB_ULPI_DATA : inout std_logic_vector(7 downto 0)

        );
    end component;

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '0';
            wait for ( SYSCLK_PERIOD * 10 );
            
            NSYSRESET <= '1';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  Eval_pfm_top
    Eval_pfm_top_0 : Eval_pfm_top
        -- port map
        port map( 
            -- Inputs
            MMUART_1_RXD => '0',
            MDDR_DQS_TMATCH_0_IN => NSYSRESET,
            USB_ULPI_DIR => '0',
            USB_ULPI_NXT => '0',
            USB_ULPI_XCLK => SYSCLK,
            DEVRST_N => NSYSRESET,
            XTL => SYSCLK,
            CLKP_IN1 => (others=> '0'),
            CLKN_IN1 => (others=> '0'),
            DATP_IN1 => (others=> '0'),
            DATN_IN1 => (others=> '0'),
            DATP_IN2 => (others=> '0'),
            DATN_IN2 => (others=> '0'),
            CLKP_IN2 => (others=> '0'),
            CLKN_IN2 => (others=> '0'),
            SYNP_IN1 => (others=> '0'),
            SYNN_IN1 => (others=> '0'),
            SYNP_IN2 => (others=> '0'),
            SYNN_IN2 => (others=> '0'),

            -- Outputs
            MMUART_1_TXD =>  open,
            MDDR_DQS_TMATCH_0_OUT =>  open,
            MDDR_CAS_N =>  open,
            MDDR_CLK =>  open,
            MDDR_CLK_N =>  open,
            MDDR_CKE =>  open,
            MDDR_CS_N =>  open,
            MDDR_ODT =>  open,
            MDDR_RAS_N =>  open,
            MDDR_RESET_N =>  open,
            MDDR_WE_N =>  open,
            USB_ULPI_STP =>  open,
            GPIO_29_M2F =>  open,
            MDDR_ADDR => open,
            MDDR_BA => open,
            GPIO2_1 => open,
            GPIO1_1 => open,
            GPIO0_1 => open,
            GPIO2_2 => open,
            GPIO1_2 => open,
            GPIO0_2 => open,
            LED => open,

            -- Inouts
            MDDR_DM_RDQS => open,
            MDDR_DQ => open,
            MDDR_DQS => open,
            USB_ULPI_DATA => open

        );

end behavioral;

