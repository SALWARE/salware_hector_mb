--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: mss_tb.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::SmartFusion2> <Die::M2S025> <Package::484 FBGA>
-- Author: <Name>
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity mss_tb is
end mss_tb;

architecture behavioral of mss_tb is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';
    signal my_signal : std_logic := '0';
    component Eval_pfm_MSS
        -- ports
        port( 
            -- Inputs
            MCCC_CLK_BASE : in std_logic;
            MMUART_1_RXD : in std_logic;
            MDDR_DQS_TMATCH_0_IN : in std_logic;
            MDDR_DDR_CORE_RESET_N : in std_logic;
            MDDR_DDR_AHB0_S_HSEL : in std_logic;
            MDDR_DDR_AHB0_S_HMASTLOCK : in std_logic;
            MDDR_DDR_AHB0_S_HWRITE : in std_logic;
            MDDR_DDR_AHB0_S_HREADY : in std_logic;
            MCCC_CLK_BASE_PLL_LOCK : in std_logic;
            MSS_RESET_N_F2M : in std_logic;
            MDDR_APB_S_PRESET_N : in std_logic;
            MDDR_APB_S_PCLK : in std_logic;
            FIC_2_APB_M_PREADY : in std_logic;
            FIC_2_APB_M_PSLVERR : in std_logic;
            MDDR_APB_S_PWRITE : in std_logic;
            MDDR_APB_S_PENABLE : in std_logic;
            MDDR_APB_S_PSEL : in std_logic;
            GPIO_2_F2M : in std_logic;
            MDDR_DDR_AHB0_S_HADDR : in std_logic_vector(31 downto 0);
            MDDR_DDR_AHB0_S_HBURST : in std_logic_vector(2 downto 0);
            MDDR_DDR_AHB0_S_HSIZE : in std_logic_vector(1 downto 0);
            MDDR_DDR_AHB0_S_HTRANS : in std_logic_vector(1 downto 0);
            MDDR_DDR_AHB0_S_HWDATA : in std_logic_vector(31 downto 0);
            FIC_2_APB_M_PRDATA : in std_logic_vector(31 downto 0);
            MDDR_APB_S_PWDATA : in std_logic_vector(15 downto 0);
            MDDR_APB_S_PADDR : in std_logic_vector(10 downto 2);

            -- Outputs
            MMUART_1_TXD : out std_logic;
            MDDR_DQS_TMATCH_0_OUT : out std_logic;
            MDDR_CAS_N : out std_logic;
            MDDR_CLK : out std_logic;
            MDDR_CLK_N : out std_logic;
            MDDR_CKE : out std_logic;
            MDDR_CS_N : out std_logic;
            MDDR_ODT : out std_logic;
            MDDR_RAS_N : out std_logic;
            MDDR_RESET_N : out std_logic;
            MDDR_WE_N : out std_logic;
            MDDR_DDR_AHB0_S_HREADYOUT : out std_logic;
            MDDR_DDR_AHB0_S_HRESP : out std_logic;
            MSS_RESET_N_M2F : out std_logic;
            FIC_2_APB_M_PRESET_N : out std_logic;
            FIC_2_APB_M_PCLK : out std_logic;
            FIC_2_APB_M_PWRITE : out std_logic;
            FIC_2_APB_M_PENABLE : out std_logic;
            FIC_2_APB_M_PSEL : out std_logic;
            MDDR_APB_S_PREADY : out std_logic;
            MDDR_APB_S_PSLVERR : out std_logic;
            GPIO_0_M2F : out std_logic;
            GPIO_1_M2F : out std_logic;
            MDDR_ADDR : out std_logic_vector(15 downto 0);
            MDDR_BA : out std_logic_vector(2 downto 0);
            MDDR_DDR_AHB0_S_HRDATA : out std_logic_vector(31 downto 0);
            FIC_2_APB_M_PADDR : out std_logic_vector(15 downto 2);
            FIC_2_APB_M_PWDATA : out std_logic_vector(31 downto 0);
            MDDR_APB_S_PRDATA : out std_logic_vector(15 downto 0);

            -- Inouts
            MDDR_DM_RDQS : inout std_logic_vector(1 downto 0);
            MDDR_DQ : inout std_logic_vector(15 downto 0);
            MDDR_DQS : inout std_logic_vector(1 downto 0)

        );
    end component;

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '0';
            my_signal <= '0';
            wait for ( SYSCLK_PERIOD * 10 );
            
            NSYSRESET <= '1';
            my_signal <= '0';    
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  Eval_pfm_MSS
    Eval_pfm_MSS_0 : Eval_pfm_MSS
        -- port map
        port map( 
            -- Inputs
            MCCC_CLK_BASE => SYSCLK,
            MMUART_1_RXD => '0',
            MDDR_DQS_TMATCH_0_IN => NSYSRESET,
            MDDR_DDR_CORE_RESET_N => NSYSRESET,
            MDDR_DDR_AHB0_S_HSEL => '0',
            MDDR_DDR_AHB0_S_HMASTLOCK => NSYSRESET,
            MDDR_DDR_AHB0_S_HWRITE => NSYSRESET,
            MDDR_DDR_AHB0_S_HREADY => '0',
            MCCC_CLK_BASE_PLL_LOCK => SYSCLK,
            MSS_RESET_N_F2M => NSYSRESET,
            MDDR_APB_S_PRESET_N => NSYSRESET,
            MDDR_APB_S_PCLK => SYSCLK,
            FIC_2_APB_M_PREADY => '0',
            FIC_2_APB_M_PSLVERR => '0',
            MDDR_APB_S_PWRITE => NSYSRESET,
            MDDR_APB_S_PENABLE => '0',
            MDDR_APB_S_PSEL => '0',
            GPIO_2_F2M => '0',
            MDDR_DDR_AHB0_S_HADDR => (others=> '0'),
            MDDR_DDR_AHB0_S_HBURST => (others=> '0'),
            MDDR_DDR_AHB0_S_HSIZE => (others=> '0'),
            MDDR_DDR_AHB0_S_HTRANS => (others=> '0'),
            MDDR_DDR_AHB0_S_HWDATA => (others=> '0'),
            FIC_2_APB_M_PRDATA => (others=> '0'),
            MDDR_APB_S_PWDATA => (others=> '0'),
            MDDR_APB_S_PADDR => (others=> '0'),

            -- Outputs
            MMUART_1_TXD =>  open,
            MDDR_DQS_TMATCH_0_OUT =>  open,
            MDDR_CAS_N =>  open,
            MDDR_CLK =>  open,
            MDDR_CLK_N =>  open,
            MDDR_CKE =>  open,
            MDDR_CS_N =>  open,
            MDDR_ODT =>  open,
            MDDR_RAS_N =>  open,
            MDDR_RESET_N =>  open,
            MDDR_WE_N =>  open,
            MDDR_DDR_AHB0_S_HREADYOUT => open,
            MDDR_DDR_AHB0_S_HRESP =>  open,
            MSS_RESET_N_M2F =>  open,
            FIC_2_APB_M_PRESET_N =>  open,
            FIC_2_APB_M_PCLK =>  open,
            FIC_2_APB_M_PWRITE =>  open,
            FIC_2_APB_M_PENABLE =>  open,
            FIC_2_APB_M_PSEL =>  open,
            MDDR_APB_S_PREADY =>  open,
            MDDR_APB_S_PSLVERR =>  open,
            GPIO_0_M2F =>  my_signal,
            GPIO_1_M2F =>  open,
            MDDR_ADDR => open,
            MDDR_BA => open,
            MDDR_DDR_AHB0_S_HRDATA => open,
            FIC_2_APB_M_PADDR => open,
            FIC_2_APB_M_PWDATA => open,
            MDDR_APB_S_PRDATA => open,

            -- Inouts
            MDDR_DM_RDQS => open,
            MDDR_DQ => open,
            MDDR_DQS => open

        );

end behavioral;

