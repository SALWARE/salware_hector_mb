#ifndef Eval_pfm_HW_PLATFORM_H_
#define Eval_pfm_HW_PLATFORM_H_
/*****************************************************************************
*
*Created by Microsemi SmartDesign  Fri Mar 11 17:01:16 2016
*
*Memory map specification for peripherals in Eval_pfm
*/

/*-----------------------------------------------------------------------------
* CM3 subsystem memory map
* Master(s) for this subsystem: CM3 FABRIC2MSSFIC2 
*---------------------------------------------------------------------------*/
#define EVAL_PFM_MSS_0                  0x40020800U


#endif /* Eval_pfm_HW_PLATFORM_H_*/
