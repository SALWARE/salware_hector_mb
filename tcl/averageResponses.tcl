set numBits 16
set numIter 5

puts "Averaging files"
flush stdout

set outFile [open "average.csv" "w"]

for { set i 0 } {$i < $numIter} {incr i} {
  set inFile$i [open "iteration_$i.csv" "r"]
}

for { set i 0 } {$i < $numBits} {incr i} {
  set avgBit 0
  set avgF1 0
  set avgF2 0
  set maxF1 0
  set minF1 10000
  set maxF2 0
  set minF2 10000
  for { set j 0 } {$j < $numIter} {incr j} {
    set instr [gets [set inFile$j]]
    set invalues [string map {"," " "} $instr]
    set avgBit [expr $avgBit + [lindex $invalues 1]]
    set avgF1 [expr $avgF1 + [lindex $invalues 2]]
    set avgF2 [expr $avgF2 + [lindex $invalues 3]]
    if { [lindex $invalues 2] < $minF1 } {
      set minF1 [lindex $invalues 2]
    } elseif { [lindex $invalues 2] > $maxF1 } {
      set maxF1 [lindex $invalues 2]
    }

    if { [lindex $invalues 3] < $minF2 } {
      set minF2 [lindex $invalues 3]
    } elseif { [lindex $invalues 3] > $maxF2 } {
      set maxF2 [lindex $invalues 3]
    }
  }
  set avgBit [expr $avgBit / $numIter]
  set avgF1 [expr $avgF1 / $numIter]
  set avgF2 [expr $avgF2 / $numIter]
  puts $outFile "$i,$avgBit,$avgF1,$minF1,$maxF1,$avgF2,$minF2,$maxF2"
}

for { set i 0 } {$i < $numIter} {incr i} {
  close [set inFile$i]
}

close $outFile

puts {}
flush stdout
