source HECTOR_data_acq.tcl
source puf_if.tcl

puts -nonewline "Number of bits to get: "
flush stdout
set numBits [gets stdin]
puts -nonewline "Number of averages: "
flush stdout
set numIter [gets stdin]
puts -nonewline "Number of cards: "
flush stdout
set numCards [gets stdin]

for { set card 0 } { $card < $numCards } { incr card } {

  puts "Install card #[expr $card+1]... Press enter to continue"
  flush stdout
  gets stdin

  set dev [openDevice /dev/ttyUSB0]

  softReset $dev

  selectDaughterBoard $dev 2

  sendDaughterReset $dev 

  puts "Calibrating frequency measurement..."
  flush stdout

  set calib1 ""
  set calib2 ""

  puts -nonewline "0 %\r"
  flush stdout
  for {set i 0} {$i < 4} {incr i} {
    puf_if $dev "calibrate $i $i"
    set ret [puf_if $dev "freq"]
    set calib1 "$calib1 [lindex $ret 0]"
    set calib2 "$calib2 [lindex $ret 1]"
    puts -nonewline "[expr 100*($i+1)/4] %\r"
    flush stdout
  }
  puts "Calibration results:"
  puts "  $calib1"
  puts "  $calib2"
  puts {}

  puts -nonewline "Do you want to continue ? (y/n) "
  flush stdout
  set ret [gets stdin]
  if { $ret != "y" } {
    return
  }

  set resp 0

  for {set i 0} {$i < $numIter} {incr i} {

    set outFile [open "iteration_$i\_card_$card.csv" "w"]

    for {set j 0} {$j < $numBits} {incr j} {

      puts -nonewline "Getting the response bit #[expr $j+1] of $numBits, Iteration [expr $i+1] of $numIter\r"
      flush stdout

      puf_if $dev "setmux $j $j"


      set ret [puf_if $dev "status"]
      while { [getBitValue [fromHEX $ret] 2] == 0 } {
        set ret [puf_if $dev "status"]
      }
      set resp [getBitValue [fromHEX $ret] 0]

      set ret [puf_if $dev "freq"]
      
      puts $outFile "$j,$resp,[lindex $ret 0],[lindex $ret 1]"

    }

    for {set j 0} {$j < 100} {incr j} {
      puts -nonewline " "
    }
    puts -nonewline "\r"

    close $outFile
  }

  puts "Averaging files"
  flush stdout

  set outFile [open "average_$card.csv" "w"]

  for { set i 0 } {$i < $numIter} {incr i} {
    set inFile$i [open "iteration_$i\_card_$card.csv" "r"]
  }

  for { set i 0 } {$i < $numBits} {incr i} {
    set avgBit 0
    set avgF1 0
    set avgF2 0
    set maxF1 0
    set minF1 10000
    set maxF2 0
    set minF2 10000
    for { set j 0 } {$j < $numIter} {incr j} {
      set instr [gets [set inFile$j]]
      set invalues [string map {"," " "} $instr]
      set avgBit [expr $avgBit + [lindex $invalues 1]]
      set avgF1 [expr $avgF1 + [lindex $invalues 2]]
      set avgF2 [expr $avgF2 + [lindex $invalues 3]]

      if { [lindex $invalues 2] < $minF1 } {
        set minF1 [lindex $invalues 2]
      } elseif { [lindex $invalues 2] > $maxF1 } {
        set maxF1 [lindex $invalues 2]
      }

      if { [lindex $invalues 3] < $minF2 } {
        set minF2 [lindex $invalues 3]
      } elseif { [lindex $invalues 3] > $maxF2 } {
        set maxF2 [lindex $invalues 3]
      }
    }
    set avgBit [expr $avgBit / $numIter]
    set avgF1 [expr $avgF1 / $numIter]
    set avgF2 [expr $avgF2 / $numIter]
    puts $outFile "$i,$avgBit,$avgF1,$avgF2"
  }

  for { set i 0 } {$i < $numIter} {incr i} {
    close [set inFile$i]
  }

  close $outFile

  puts {}
  flush stdout

  disconnect $dev
}

set outFile [open "average.csv" "w"]

for { set card 0 } { $card < $numCards } { incr card } {
  set inFile$card [open "average_$card.csv" "r"]
}

for { set i 0 } { $i < $numBits } { incr i } {
  puts -nonewline $outFile "$i"
  for { set j 0 } { $j < $numCards } { incr j } {
    set instr [gets [set inFile$j]]
    puts -nonewline $outFile ",[string range $instr [expr [string first "," $instr]+1] end]"
  }
  puts $outFile {}
}

