# Load the HECTOR tcl extension
source HECTOR_data_acq.tcl

# Size of data to retrieve
set filesize 125000

# Set the variables for commands
set load_uart_high_32 8
set send_uart_word 6
set read_uart_high_32 9
set read_uart_low_32 10
set read_acquisition_time 7

# Open the serial device
set dev [openDevice /dev/ttyUSB0]

for {set i 0} {$i < 1} {incr i} {
  
# Set timestamp for file name
  set filenameTimestamp [clock format [clock seconds] -format "%Y%m%d%H%M%S"]
  puts "Measurement # $i (timestamp: $filenameTimestamp)"

# Reset the mohterboard
  softReset $dev
  sendFabricReset $dev
  sendDaughterReset $dev

# Select the daughter board connection (1 = HDMI, 2 = SATA)
  selectDaughterBoard $dev 2

# Configure the GPIOs as outputs
#     second parameter is the GPIO configuration
#     which is an 8-bit word where every bit corresponds
#     to one GPIO. 1 is output and 0 input
  configureGPIO $dev 255

# Set the GPIOs to configure the TRNG output multiplexer
#   0 = XOR - TRNG output after decimator
#   1 = DFF - raw random data before decimator
  setGPIO $dev 0

# Create filesystem for data
  createFileSystem $dev

# Reset daughter board
  sendDaughterReset $dev

# Begin data acquisition
#     We must provide the filename that will be assigned
#     to acquired data file. Last parameter sets the size
#     of the file in bytes.
  beginAcquisition $dev ACQ.DAT $filesize

# Wait until the acquisition is finished
  set err [getStatus $dev]
  while {[lindex $err 2] != 0} {
    set err [getStatus $dev]

# Print progress
    puts -nonewline "[lindex $err 1] %\r"
    flush stdout
  }
  puts {}
  flush stdout
  
# Get TRNG status
  sendFabricCommand $dev $load_uart_high_32 0
  sendFabricCommand $dev $send_uart_word 0
  set ret [getStatus $dev]
  while { [getBitValue [fromHEX [lindex $ret 4]] 0] == 0 } {
    set ret [getStatus $dev]
  }
  puts {}

# Load the daughter response
set high_32 [lindex [sendFabricCommand $dev $read_uart_high_32 0] 1]
set low_32 [lindex [sendFabricCommand $dev $read_uart_low_32 0] 1]

# Compute P1 and P2 values
set P2 [string range $low_32 5 7]
set P2 [expr $P2*256/1000]
set P1 [string range $low_32 2 4]

# Open the log file
  set logFile [open "log_$filenameTimestamp\_$i.txt" "w"]
# Output data to console and to the file
  puts "\tStatus register        $high_32$low_32"
  puts ""
  puts $logFile "Status register        $high_32$low_32"
  puts $logFile ""
  puts "\tP1 value               $P1"
  puts "\tP2 value               $P2"
  puts $logFile "\tP1 value               $P1"
  puts $logFile "\tP2 value               $P2"
  set time [lindex [sendFabricCommand $dev $read_acquisition_time 0] 1]
  puts "\tFilesize               [expr ($filesize)] Bytes"
  puts $logFile "Filesize               [expr ($filesize)] Bytes"
  puts "\tAcquisition time       [fromHEX $time] ms"
  puts $logFile "Acquisition time       [fromHEX $time] ms"
  puts "\tBitrate                [expr ($filesize * 8.0) / ([fromHEX $time] / 1.0)] kbits/s"
  puts $logFile "Bitrate                [expr ($filesize * 8.0) / ([fromHEX $time] / 1.0)] kbits/s"
  puts {}
  if { $P2 < 35.5 } {
    puts "!!! Not enough entropy !!!"
    puts $logFile "!!! Not enough entropy !!!"
    puts {}
  }

# Mount the disk to make it available for the PC
  mountDisk $dev

# Automatically detect the HECTOR disk
  set hectorDrive [findHECTOR]

  if { $hectorDrive == 0 } {
    puts "Hector disk drive not found. You will have to search for it manually."
    return 1
  }

# Check for the mountpoint
  if { [string length [lindex $hectorDrive 2]] == 0 } {
    puts "Hector disk drive is not mounted. Please mount the device [lindex $hectorDrive 0]."
    return 1
  }

# Copy the data from HECTOR disk
  file copy -force "[lindex $hectorDrive 2]/ACQ.DAT" "./acq_$filenameTimestamp\_$i.bin"
}

# Close the connection to the board
disconnect $dev
