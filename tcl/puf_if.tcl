source HECTOR_data_acq.tcl

proc puf_if {dev command} {
  switch [lindex $command 0] {
    "freq" {
      sendFabricCommand $dev 8 [fromHEX "03000000"]
      sendFabricCommand $dev 6 0

      set ret [sendFabricCommand $dev 0 0]
      while { [getBitValue [fromHEX [lindex $ret 0]] 0] == 0 } {
        set ret [sendFabricCommand $dev 0 0]
      }

      set f1 [expr [fromHEX [lindex [sendFabricCommand $dev 9 0] 1]] * 2075.0 / 1000000.0]
      set f2 [expr [fromHEX [lindex [sendFabricCommand $dev 10 0] 1]] * 2075.0 / 1000000.0]
      set retValue "$f1 $f2"
    }
    "calibrate" {
      set f1mux [lindex $command 1]
      set f2mux [lindex $command 2]
      if { $f1mux <= 3 && $f1mux >= 0 && $f2mux <= 3 && $f1mux >= 0 } {
        sendFabricCommand $dev 8 [fromHEX "02000000"]
        set f1mux [toHEX $f1mux]
        set f2mux [toHEX $f2mux]
        while { [string length $f1mux] < 2 } {
          set f1mux "0$f1mux"
        }
        while { [string length $f2mux] < 2 } {
          set f2mux "0$f2mux"
        }
        sendFabricCommand $dev 6 [fromHEX "000000$f1mux$f2mux"]
        set retValue "$f1mux $f2mux"
      } else {
        #puts "f1mux and f2mux must be integers between 0 and 3!"
        set retValue -1
      }
    }
    "setmux" {
      set mux1 [lindex $command 1]
      set mux2 [lindex $command 2]
      if { $mux1 <= 255 && $mux1 >= 0 && $mux2 <= 255 && $mux2 >= 0 } {
        sendFabricCommand $dev 8 [fromHEX "01000000"]
        set mux1 [toHEX $mux1]
        set mux2 [toHEX $mux2]
        while { [string length $mux1] < 2 } {
          set mux1 "0$mux1"
        }
        while { [string length $mux2] < 2 } {
          set mux2 "0$mux2"
        }
        sendFabricCommand $dev 6 [fromHEX "0000$mux1$mux2"]
        set retValue "$mux1 $mux2"
      } else {
        #puts "mux1 and mux2 must be integers between 0 and 255!"
        set retValue -1
      }
    }
    "status" {
      sendFabricCommand $dev 8 [fromHEX "00000000"]
      sendFabricCommand $dev 6 0

      set ret [sendFabricCommand $dev 0 0]
      while { [getBitValue [fromHEX [lindex $ret 0]] 0] == 0 } {
        set ret [sendFabricCommand $dev 0 0]
      }

      set status [lindex [sendFabricCommand $dev 9 0] 1][lindex [sendFabricCommand $dev 10 0] 1]
      set retValue $status
    }
    "reset" {
      sendDaughterReset $dev
      set retValue 0
    }
    default {
      #puts "Command not found (type ? or 'help' for help)"
      set retValue -1
    }
  }
  return $retValue
}
