source HECTOR_data_acq.tcl
source puf_if.tcl

puts -nonewline "HECTOR board COM port: "
flush stdout
set comport [gets stdin]

set dev [openDevice $comport]

softReset $dev

selectDaughterBoard $dev 2

sendDaughterReset $dev

set quit 0
while { $quit == 0 } {
  puts -nonewline "> "
  flush stdout
  set userInput [gets stdin]
  switch [lindex $userInput 0] {
    "?" -
    "help" {
      puts "List of commands:"
      puts "  ?, help                     : Shows this help"
      puts "  freq                        : Gets the frequency measurement values"
      puts "  calibrate <f1 mux> <f2 mux> : Calibrates the frequency measurement"
      puts "  setmux    <mux1> <mux2>     : Sets the PUF MUX"
      puts "  status                      : Gets the PUF status"
      puts "  reset                       : Resets the Daughter module"
      puts "  quit                        : Quits this interface"
    }
    "exit" -
    "quit" {
      set quit 1
    }
    default {
      set ret [puf_if $dev $userInput]
      if { $ret == -1 } {
        puts "Command not found. Type ? for help."
      } elseif { $ret == 0 } {
      } else {
        for { set i 0 } { [string length [lindex $ret $i]] > 0 } { incr i } {
          puts [lindex $ret $i]
        }
      }
    }
  }
}

disconnect $dev
