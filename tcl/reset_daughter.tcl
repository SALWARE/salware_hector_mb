# Load the HECTOR tcl extension
source HECTOR_data_acq.tcl

# Set the variables for commands
set load_uart_high_32 8
set send_uart_word 6
set read_uart_high_32 9
set read_uart_low_32 10
set read_acquisition_time 7

# Open the serial device
set dev [openDevice COM3]

# Reset the mohterboard
  softReset $dev
  sendFabricReset $dev
  sendDaughterReset $dev

# Select the daughter board connection (1 = HDMI, 2 = SATA)
  selectDaughterBoard $dev 2

# Configure the GPIOs as outputs
#     second parameter is the GPIO configuration
#     which is an 8-bit word where every bit corresponds
#     to one GPIO. 1 is output and 0 input
  configureGPIO $dev 255

# Set the GPIOs to configure the TRNG output multiplexer
#   0 = XOR - TRNG output after decimator
#   1 = DFF - raw random data before decimator
  setGPIO $dev 0


# Get TRNG status
  sendFabricCommand $dev $load_uart_high_32 0
  sendFabricCommand $dev $send_uart_word 0
  set ret [getStatus $dev]
  while { [getBitValue [fromHEX [lindex $ret 4]] 0] == 0 } {
    set ret [getStatus $dev]
  }
  puts {}

# Load the daughter response
set high_32 [lindex [sendFabricCommand $dev $read_uart_high_32 0] 1]
set low_32 [lindex [sendFabricCommand $dev $read_uart_low_32 0] 1]

# Compute P1 and P2 values
set P2 [string range $low_32 5 7]
set P2 [expr $P2*256/1000]
set P1 [string range $low_32 2 4]

# Open the log file
 set logFile [open "log_Reset.txt" "w"]
# Output data to console and to the file
 puts "\tStatus register      $high_32$low_32  "
 puts ""
 puts $logFile "Status register        $high_32$low_32"
 puts $logFile ""
 puts "\tP1 value               $P1"
 puts "\tP2 value               $P2"
 puts $logFile "\tP1 value               $P1"
 puts $logFile "\tP2 value               $P2"
 puts {}
 if { $P2 < 35.5 } {
   puts "!!! Not enough entropy !!!"
   puts $logFile "!!! Not enough entropy !!!"
   puts {}
 }

# Close the connection to the board
disconnect $dev
